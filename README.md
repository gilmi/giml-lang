# <a href="https://giml-lang.org"><img src="https://gitlab.com/uploads/-/system/project/avatar/25082385/lambda-round-small.png?width=16" alt="Giml logo" style="vertical-align:middle;"> Giml</a>

![Build status](https://gitlab.com/gilmi/giml-lang/badges/main/pipeline.svg?job=pipeline)

Giml is a strict, statically typed, purely functional language with emphasis on structural typing.

The compiler currently targets JavaScript, but more backends are planned in the future.

This project was streamed on [twitch.tv](), and past sessions are available on [Youtube](https://www.youtube.com/watch?list=PLhMOOgDOKD4IkQM75GkAnXI-fpIrDAnsu&v=khAKcFgziWU).

For more information, visit the [website](https://giml-lang.org).

## Build and run

### With [Cabal + GHC 9.6](https://www.haskell.org/ghcup):

```sh
> cabal update
> cabal build all
> cabal run giml -- compile --input examples/factorial.giml --output /tmp/output.js
> node /tmp/output.js # use node to run the program
120
```


## Logo

Logos adapted from “Rad Pack – 80’s Theme” Wallpapers by Nate Wren (CC BY-NC 4.0)
