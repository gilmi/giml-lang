.PHONY: build
build:
	stack build --fast

.PHONY: format
format:
	find ./src -type f -name "*.hs" -exec sh -c 'fourmolu -i {}' \;

.PHONY: i
i:
	stack build --fast && stack run interactive

.PHONY: ghci
ghci:
	cabal repl --repl-options "-interactive-print=Text.Pretty.Simple.pPrint" giml-language

.PHONY: test
test:
	cabal test all --test-show-details=always --test-option=--color

.PHONY: test-parser
test-parser:
	stack test --fast --test-arguments="--match parser --color"

.PHONY: test-infer
test-infer:
	stack test --fast --test-arguments="--match infer --color"

.PHONY: test-negative
test-negative:
	stack test --fast --test-arguments="--match negative --color"

.PHONY: clean
clean:
	stack clean && rm -r .stack-work && rm -r src/frontend/.stack-work && rm -r src/backend/.stack-work && rm -r src/compiler/.stack-work; rm -rf dist-newstyle

