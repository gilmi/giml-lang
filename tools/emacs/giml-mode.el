;;; giml-mode.el --- Giml major mode
;;; Adapted from elm-mode (GPL3) and futhark-mode (ISC)

;; Copyright (C) 2021 Gil Mizrahi

;; Author: Gil Mizrahi
;; URL: https://gitlab.com/gilmi/giml-lang
;; Keywords: extensions

;; This file isnot part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'font-lock)
(require 'rx)

(defgroup giml nil
  "Support for the Giml programming language."
  :link '(url-link :tag "Gitlab" "https://gitlab.com/gilmi/giml-mode")
  :group 'languages)

;;###autoload

(defvar giml-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "<tab>") 'tab-to-tab-stop)
    (define-key map [tab] 'tab-to-tab-stop)
	map)
  "Keymap for Giml major mode")

;;;;;;;;;;;;;

(defgroup giml-font-lock nil
  "Font locking for Giml code."
  :group 'faces)


(defface giml-font-lock-multiline-list-delimiters
  '((t :inherit font-lock-keyword-face))
  "The default face used to highlight brackets and commas in multiline lists."
  :group 'giml-font-lock)


(defcustom giml-font-lock-multiline-list-delimiters-face 'giml-font-lock-multiline-list-delimiters
  "The face used to highlight brackets and commas in multilist lists.
To disable this highlighting, set this to nil."
  :type '(choice (const nil)
                 face)
  :group 'giml-font-lock)

(defconst giml--keywords
  '("if" "then" "else"
    "case" "of"
    "let" "in"
    "do" "end"
    "alias"
    "module"
    "import" "as" "hiding" "exposing"
    "_"
    "ffi")
  "Reserved keywords.")

(defconst giml--regexp-keywords
  (regexp-opt giml--keywords 'symbols)
  "A regular expression representing the reserved keywords.")

(defconst giml--font-lock-keywords
  (cons giml--regexp-keywords font-lock-keyword-face)
  "Highlighting for keywords.")

(defvar giml--syntax-table
  (let ((st (make-syntax-table)))
    ;;; Syntax entry for {- -} type comments.
    (modify-syntax-entry ?\{  "(}1nb" st)
    (modify-syntax-entry ?\}  "){4nb" st)
    (modify-syntax-entry ?-  ". 123" st)
    (modify-syntax-entry ?\n ">" st)
    (modify-syntax-entry ?. "_" st)

    (modify-syntax-entry ?\" "\"\"" st)
    (modify-syntax-entry ?\\ "\\" st)
    st))

(defconst giml--regexp-top-level
  "^\\([a-z][0-9A-Za-z_]*\\)"
  "A regular expression representing top level identifiers.")


(defconst giml--font-lock-top-level
  (cons giml--regexp-top-level font-lock-variable-name-face)
  "Highlighting for top level identifiers.")

;; we want a type declaration to also light up
(font-lock-add-keywords 'giml-mode '(("^\\(type\\)\\s-+[A-Z]" 1 font-lock-keyword-face t)))

(defconst giml--regexp-type
  "\\<[A-Z][0-9A-Za-z_']*"
  "A regular expression representing modules, types and variants.")


(defconst giml--font-lock-types
  (cons giml--regexp-type font-lock-type-face)
  "Highlighting for module names, types and variants.")

;; numbers

(defconst giml-highlight-number
  (concat "-?"
          "\\<\\(?:"
          (concat "\\(?:"
                  "\\(?:0[xb]\\)"
                  "[0-9a-fA-F]+"
                  "\\(?:\\.[0-9a-fA-F]+\\)?"
                  "\\(?:[pP][+-]?[0-9]+\\)?"
                  "\\|"
                  "[0-9]+"
                  "\\(?:\\.[0-9]+\\)?"
                  "\\(?:e-?[0-9]+\\)?"
                  "\\)"
                  )
          "\\)\\>")
  "All numeric constants, including hex float literals.")

(defconst giml--font-lock-numbers
  (cons giml-highlight-number font-lock-constant-face)
  "Highlighting for module names, types and variants.")

;; operators

(defface giml-font-lock-operators
  '((t :inherit font-lock-builtin-face))
  "The default face used to highlight operators inside expressions."
  :group 'giml-font-lock)

(defconst giml--regexp-operators
  (concat "\\(" "`[^`]+`"
          "\\|" "\\B\\\\"
          "\\|" "[-+*/\\\\|<>=:!@#$%^&,.]+"
          "\\)")
  "A regular expression representing operators inside expressions.")

(defconst giml--font-lock-operators
  (cons giml--regexp-operators 'font-lock-function-name-face)
  "Highlighting for operators inside expressions.")

;;;;;;;;;;;

(defun giml--syntax-stringify ()
  "Syntax propertize triple quoted strings."
  (let* ((ppss (save-excursion
                 (backward-char 3)
                 (syntax-ppss)))
         (string-started (and (not (nth 4 ppss)) (nth 8 ppss)))
         (quote-starting-pos (- (point) 3))
         (quote-ending-pos (point)))
    (if (not string-started)
        (put-text-property quote-starting-pos (1+ quote-starting-pos)
                           'syntax-table (string-to-syntax "|"))
      (put-text-property (1- quote-ending-pos) quote-ending-pos
                           'syntax-table (string-to-syntax "|")))))

(defconst giml--syntax-propertize
  (syntax-propertize-rules
   ;;; Syntax rule for char literals
   ((rx (and (1+ " ")
             (group "'")
             (optional "\\") any
             (group "'")))
    (1 "\"")
    (2 "\""))

   ((rx (and (or point
                 (not (any ?\\ ?\"))
                 (and (or (not (any ?\\)) point) ?\\ (* ?\\ ?\\) (any ?\")))
             (* ?\\ ?\\)
             "\"\"\""))
    (0 (ignore (giml--syntax-stringify))))))

(defun giml--syntax-propertize-function (begin end)
  "Mark special lexemes between BEGIN and END."
  (funcall giml--syntax-propertize begin end)
  (save-excursion
    (goto-char begin)
    (while (re-search-forward "\\\\[({]" end t)
      (let ((open (match-beginning 0)))
        (add-text-properties open (1+ open) '(syntax-table (1 . nil)))))))

;; setup

(defconst giml--font-lock-highlighting
  (list (list giml--font-lock-keywords
              giml--font-lock-top-level
              giml--font-lock-types
			  giml--font-lock-numbers
              giml--font-lock-operators)
        nil nil))


(defun giml--font-lock-enable ()
  "Turn on Giml font lock."
  (setq font-lock-multiline t)
  (set-syntax-table giml--syntax-table)
  (set (make-local-variable 'syntax-propertize-function) #'giml--syntax-propertize-function)
  (set (make-local-variable 'font-lock-defaults) giml--font-lock-highlighting))

;;;###autoload

(add-to-list 'auto-mode-alist '("\\.giml\\'" . giml-mode))

;; indentation

(defun giml-indent-mode ()
  "Indent mode for Giml files"
  (setq-default indent-tabs-mode 'only)
  (setq tab-width 4) ; or any other preferred value
  (defvaralias 'c-basic-offset 'tab-width)
  (defvaralias 'cperl-indent-level 'tab-width)
  )

;; imenu

(require 'imenu)

(defcustom giml-imenu-use-categories t
  "Group imenu entries by their type, e.g. functions, type aliases."
  :group 'giml
  :type 'boolean
  :safe 'booleanp)

(defun giml-imenu-create-index ()
  "Create an imenu index for the current buffer."
  (save-excursion
    (imenu--generic-function
     `((,(and giml-imenu-use-categories "Definitions")
		"^\\([a-z][0-9A-Za-z_]*\\)\\s-+\\(=\\|[a-z]\\)" 1)
	   (,(and giml-imenu-use-categories "Types")
		"^type\\s-+\\([A-Z][0-9A-Za-z_]*\\)" 1)))))

;; giml-mode

(defun giml-mode ()
  "Major mode for editing Giml files"
  (interactive)
  (kill-all-local-variables)
  (use-local-map giml-mode-map)
  (giml--font-lock-enable)
  (setq-local imenu-create-index-function #'giml-imenu-create-index)
  (setq major-mode 'giml-mode)
  (setq mode-name "Giml")
  (run-hooks 'giml-mode-hook))

(provide 'giml-mode)

;;; giml.el ends here

