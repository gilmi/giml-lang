#!/bin/bash

for i in examples/*.giml; do
    echo $i && stack run -- compile --input $i --output /tmp/output.js && node /tmp/output.js;
done

