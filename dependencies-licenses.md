# Dependency License Report

Bold-faced **`package-name`**s denote standard libraries bundled with `ghc-9.2.1`.

## Direct dependencies of `giml-compiler:exe:giml`

| Name | Version | [SPDX](https://spdx.org/licenses/) License Id | Description | Also depended upon by |
| --- | --- | --- | --- | --- |
| **`base`** | [`4.16.0.0`](http://hackage.haskell.org/package/base-4.16.0.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/base-4.16.0.0/src/LICENSE) | Basic libraries | *(core library)* |
| **`containers`** | [`0.6.5.1`](http://hackage.haskell.org/package/containers-0.6.5.1) | [`BSD-3-Clause`](http://hackage.haskell.org/package/containers-0.6.5.1/src/LICENSE) | Assorted concrete container types | `binary`, `giml-backend-js`, `giml-language`, `hashable`, `haskeline`, `megaparsec`, `pretty-simple`, `scientific`, `uniplate` |
| **`directory`** | [`1.3.6.2`](http://hackage.haskell.org/package/directory-1.3.6.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/directory-1.3.6.2/src/LICENSE) | Platform-agnostic library for filesystem operations | `haskeline`, `process` |
| **`exceptions`** | [`0.10.4`](http://hackage.haskell.org/package/exceptions-0.10.4) | [`BSD-3-Clause`](http://hackage.haskell.org/package/exceptions-0.10.4/src/LICENSE) | Extensible optionally-pure exceptions | `haskeline` |
| **`haskeline`** | [`0.8.2`](http://hackage.haskell.org/package/haskeline-0.8.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/haskeline-0.8.2/src/LICENSE) | A command-line interface for user input, written in Haskell. |  |
| **`mtl`** | [`2.2.2`](http://hackage.haskell.org/package/mtl-2.2.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/mtl-2.2.2/src/LICENSE) | Monad classes, using functional dependencies | `exceptions`, `giml-language`, `megaparsec`, `pretty-simple` |
| `optparse-generic` | [`1.4.7`](http://hackage.haskell.org/package/optparse-generic-1.4.7) | [`BSD-3-Clause`](licenses/optparse-generic-1.4.7/LICENSE) | Auto-generate a command-line parser for your datatype | `giml-language` |
| `pretty-simple` | [`4.0.0.0`](http://hackage.haskell.org/package/pretty-simple-4.0.0.0) | [`BSD-3-Clause`](licenses/pretty-simple-4.0.0.0/LICENSE) | pretty printer for data types with a 'Show' instance. | `giml-language` |
| **`process`** | [`1.6.13.2`](http://hackage.haskell.org/package/process-1.6.13.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/process-1.6.13.2/src/LICENSE) | Process libraries | `haskeline`, `optparse-applicative` |
| **`text`** | [`1.2.5.0`](http://hackage.haskell.org/package/text-1.2.5.0) | [`BSD-2-Clause`](http://hackage.haskell.org/package/text-1.2.5.0/src/LICENSE) | An efficient packed Unicode text type. | `case-insensitive`, `giml-backend-js`, `giml-language`, `hashable`, `megaparsec`, `optparse-generic`, `pretty-simple`, `prettyprinter`, `prettyprinter-ansi-terminal`, `scientific`, `system-filepath` |
| `uniplate` | [`1.6.13`](http://hackage.haskell.org/package/uniplate-1.6.13) | [`BSD-3-Clause`](licenses/uniplate-1.6.13/LICENSE) | Help writing simple, concise and fast generic operations. | `giml-language` |

## Indirect transitive dependencies

| Name | Version | [SPDX](https://spdx.org/licenses/) License Id | Description | Depended upon by |
| --- | --- | --- | --- | --- |
| `Only` | [`0.1`](http://hackage.haskell.org/package/Only-0.1) | [`BSD-3-Clause`](licenses/Only-0.1/LICENSE) | The 1-tuple type or single-value "collection" | `optparse-generic` |
| `ansi-terminal` | [`0.11.1`](http://hackage.haskell.org/package/ansi-terminal-0.11.1) | [`BSD-3-Clause`](licenses/ansi-terminal-0.11.1/LICENSE) | Simple ANSI terminal support, with Windows compatibility | `ansi-wl-pprint`, `prettyprinter-ansi-terminal` |
| `ansi-wl-pprint` | [`0.6.9`](http://hackage.haskell.org/package/ansi-wl-pprint-0.6.9) | [`BSD-3-Clause`](licenses/ansi-wl-pprint-0.6.9/LICENSE) | The Wadler/Leijen Pretty Printer for colored ANSI terminal output | `optparse-applicative` |
| **`array`** | [`0.5.4.0`](http://hackage.haskell.org/package/array-0.5.4.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/array-0.5.4.0/src/LICENSE) | Mutable and immutable arrays | `binary`, `containers`, `deepseq`, `integer-logarithms`, `stm`, `text` |
| `base-orphans` | [`0.8.6`](http://hackage.haskell.org/package/base-orphans-0.8.6) | [`MIT`](licenses/base-orphans-0.8.6/LICENSE) | Backwards-compatible orphan instances for base | `transformers-base` |
| **`binary`** | [`0.8.9.0`](http://hackage.haskell.org/package/binary-0.8.9.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/binary-0.8.9.0/src/LICENSE) | Binary serialisation for Haskell values using lazy ByteStrings | `scientific`, `text` |
| **`bytestring`** | [`0.11.1.0`](http://hackage.haskell.org/package/bytestring-0.11.1.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/bytestring-0.11.1.0/src/LICENSE) | Fast, compact, strict and lazy byte strings with a list interface | `binary`, `case-insensitive`, `hashable`, `haskeline`, `megaparsec`, `optparse-generic`, `scientific`, `system-filepath`, `text`, `unix` |
| `case-insensitive` | [`1.2.1.0`](http://hackage.haskell.org/package/case-insensitive-1.2.1.0) | [`BSD-3-Clause`](licenses/case-insensitive-1.2.1.0/LICENSE) | Case insensitive string comparison | `megaparsec` |
| `co-log-core` | [`0.1.0`](http://hackage.haskell.org/package/co-log-core-0.1.0) | [`MPL-2.0`](licenses/co-log-core-0.1.0/LICENSE) | Logging library | `giml-language` |
| `colour` | [`2.3.6`](http://hackage.haskell.org/package/colour-2.3.6) | [`MIT`](licenses/colour-2.3.6/LICENSE) | A model for human colour/color perception | `ansi-terminal` |
| **`deepseq`** | [`1.4.6.0`](http://hackage.haskell.org/package/deepseq-1.4.6.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/deepseq-1.4.6.0/src/LICENSE) | Deep evaluation of data structures | `Only`, `bytestring`, `case-insensitive`, `containers`, `hashable`, `megaparsec`, `pretty`, `primitive`, `process`, `scientific`, `system-filepath`, `text`, `time`, `unordered-containers` |
| **`filepath`** | [`1.4.2.1`](http://hackage.haskell.org/package/filepath-1.4.2.1) | [`BSD-3-Clause`](http://hackage.haskell.org/package/filepath-1.4.2.1/src/LICENSE) | Library for manipulating FilePaths in a cross platform way. | `directory`, `haskeline`, `process` |
| `generic-records` | [`0.2.0.0`](http://hackage.haskell.org/package/generic-records-0.2.0.0) | [`BSD-3-Clause`](licenses/generic-records-0.2.0.0/LICENSE) | Magic record operations using generics | `giml-language` |
| **`ghc-bignum`** | [`1.2`](http://hackage.haskell.org/package/ghc-bignum-1.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/ghc-bignum-1.2/src/LICENSE) | GHC BigNum library | `base`, `bytestring`, `hashable`, `integer-logarithms` |
| **`ghc-boot-th`** | [`9.2.1`](http://hackage.haskell.org/package/ghc-boot-th-9.2.1) | [`BSD-3-Clause`](http://hackage.haskell.org/package/ghc-boot-th-9.2.1/src/LICENSE) | Shared functionality between GHC and the @template-haskell@ library | `template-haskell` |
| **`ghc-prim`** | [`0.8.0`](http://hackage.haskell.org/package/ghc-prim-0.8.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/ghc-prim-0.8.0/src/LICENSE) | GHC primitives | *(core library)* |
| `hashable` | [`1.4.0.1`](http://hackage.haskell.org/package/hashable-1.4.0.1) | [`BSD-3-Clause`](licenses/hashable-1.4.0.1/LICENSE) | A class for types that can be converted to a hash value | `case-insensitive`, `scientific`, `uniplate`, `unordered-containers` |
| `integer-logarithms` | [`1.0.3.1`](http://hackage.haskell.org/package/integer-logarithms-1.0.3.1) | [`MIT`](licenses/integer-logarithms-1.0.3.1/LICENSE) | Integer logarithms. | `scientific` |
| `megaparsec` | [`9.2.0`](http://hackage.haskell.org/package/megaparsec-9.2.0) | [`BSD-2-Clause`](licenses/megaparsec-9.2.0/LICENSE.md) | Monadic parser combinators | `giml-language` |
| `optparse-applicative` | [`0.16.1.0`](http://hackage.haskell.org/package/optparse-applicative-0.16.1.0) | [`BSD-3-Clause`](licenses/optparse-applicative-0.16.1.0/LICENSE) | Utilities and combinators for parsing command line options | `optparse-generic` |
| `parser-combinators` | [`1.3.0`](http://hackage.haskell.org/package/parser-combinators-1.3.0) | [`BSD-3-Clause`](licenses/parser-combinators-1.3.0/LICENSE.md) | Lightweight package providing commonly useful parser combinators | `megaparsec` |
| **`pretty`** | [`1.1.3.6`](http://hackage.haskell.org/package/pretty-1.1.3.6) | [`BSD-3-Clause`](http://hackage.haskell.org/package/pretty-1.1.3.6/src/LICENSE) | Pretty-printing library | `template-haskell` |
| `prettyprinter` | [`1.7.1`](http://hackage.haskell.org/package/prettyprinter-1.7.1) | [`BSD-2-Clause`](licenses/prettyprinter-1.7.1/LICENSE.md) | A modern, easy to use, well-documented, extensible pretty-printer. | `giml-backend-js`, `giml-language`, `pretty-simple`, `prettyprinter-ansi-terminal` |
| `prettyprinter-ansi-terminal` | [`1.1.3`](http://hackage.haskell.org/package/prettyprinter-ansi-terminal-1.1.3) | [`BSD-2-Clause`](licenses/prettyprinter-ansi-terminal-1.1.3/LICENSE.md) | ANSI terminal backend for the »prettyprinter« package. | `pretty-simple` |
| `primitive` | [`0.7.3.0`](http://hackage.haskell.org/package/primitive-0.7.3.0) | [`BSD-3-Clause`](licenses/primitive-0.7.3.0/LICENSE) | Primitive memory-related operations | `scientific` |
| `scientific` | [`0.3.7.0`](http://hackage.haskell.org/package/scientific-0.3.7.0) | [`BSD-3-Clause`](licenses/scientific-0.3.7.0/LICENSE) | Numbers represented using scientific notation | `megaparsec` |
| **`stm`** | [`2.5.0.0`](http://hackage.haskell.org/package/stm-2.5.0.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/stm-2.5.0.0/src/LICENSE) | Software Transactional Memory | `exceptions`, `haskeline`, `transformers-base` |
| `syb` | [`0.7.2.1`](http://hackage.haskell.org/package/syb-0.7.2.1) | [`BSD-3-Clause`](licenses/syb-0.7.2.1/LICENSE) | Scrap Your Boilerplate | `uniplate` |
| `system-filepath` | [`0.4.14`](http://hackage.haskell.org/package/system-filepath-0.4.14) | [`MIT`](licenses/system-filepath-0.4.14/license.txt) | High-level, byte-based file and directory path manipulations (deprecated) | `optparse-generic` |
| **`template-haskell`** | [`2.18.0.0`](http://hackage.haskell.org/package/template-haskell-2.18.0.0) | [`BSD-3-Clause`](http://hackage.haskell.org/package/template-haskell-2.18.0.0/src/LICENSE) | Support library for Template Haskell | `exceptions`, `scientific`, `text` |
| **`terminfo`** | [`0.4.1.5`](http://hackage.haskell.org/package/terminfo-0.4.1.5) | [`BSD-3-Clause`](http://hackage.haskell.org/package/terminfo-0.4.1.5/src/LICENSE) | Haskell bindings to the terminfo library. | `haskeline` |
| **`time`** | [`1.11.1.1`](http://hackage.haskell.org/package/time-1.11.1.1) | [`BSD-3-Clause`](http://hackage.haskell.org/package/time-1.11.1.1/src/LICENSE) | A time library | `directory`, `optparse-generic`, `unix` |
| **`transformers`** | [`0.5.6.2`](http://hackage.haskell.org/package/transformers-0.5.6.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/transformers-0.5.6.2/src/LICENSE) | Concrete functor and monad transformers | `exceptions`, `haskeline`, `megaparsec`, `mtl`, `optparse-applicative`, `optparse-generic`, `pretty-simple`, `primitive`, `transformers-base`, `transformers-compat` |
| `transformers-base` | [`0.4.6`](http://hackage.haskell.org/package/transformers-base-0.4.6) | [`BSD-3-Clause`](licenses/transformers-base-0.4.6/LICENSE) | Lift computations from the bottom of a transformer stack | `giml-language` |
| `transformers-compat` | [`0.7.1`](http://hackage.haskell.org/package/transformers-compat-0.7.1) | [`BSD-3-Clause`](licenses/transformers-compat-0.7.1/LICENSE) | A small compatibility shim for the transformers library | `optparse-applicative`, `optparse-generic`, `transformers-base` |
| **`unix`** | [`2.7.2.2`](http://hackage.haskell.org/package/unix-2.7.2.2) | [`BSD-3-Clause`](http://hackage.haskell.org/package/unix-2.7.2.2/src/LICENSE) | POSIX functionality | `directory`, `haskeline`, `process` |
| `unordered-containers` | [`0.2.15.0`](http://hackage.haskell.org/package/unordered-containers-0.2.15.0) | [`BSD-3-Clause`](licenses/unordered-containers-0.2.15.0/LICENSE) | Efficient hashing-based container types | `uniplate` |
| `void` | [`0.7.3`](http://hackage.haskell.org/package/void-0.7.3) | [`BSD-3-Clause`](licenses/void-0.7.3/LICENSE) | A Haskell 98 logically uninhabited data type | `optparse-generic` |

