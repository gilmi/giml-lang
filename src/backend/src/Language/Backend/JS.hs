-- | JavaScript backend for Giml
--
-- This module re-exports data types describing an AST of a subset of JavaScript,
-- as well as a prettyprinter for that subset and a set of reserved words.
module Language.Backend.JS (
  module Language.Backend.JS.Ast,
  module Language.Backend.JS.Pretty,
  module Language.Backend.JS.ReservedWords,
) where

import Language.Backend.JS.Ast
import Language.Backend.JS.Pretty
import Language.Backend.JS.ReservedWords
