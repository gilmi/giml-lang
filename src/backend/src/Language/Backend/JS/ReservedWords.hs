{-# LANGUAGE OverloadedStrings #-}

-- | JavaScript reserved words
--
-- See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#keywords
module Language.Backend.JS.ReservedWords where

import Data.Set qualified as S
import Data.Text qualified as T

-- | JavaScript reserved words
reservedWords :: S.Set T.Text
reservedWords =
  S.fromList $
    [ "break"
    , "case"
    , "catch"
    , "class"
    , "const"
    , "continue"
    , "debugger"
    , "default"
    , "delete"
    , "do"
    , "else"
    , "export"
    , "extends"
    , "finally"
    , "for"
    , "function"
    , "if"
    , "import"
    , "in"
    , "instanceof"
    , "new"
    , "return"
    , "super"
    , "switch"
    , "this"
    , "throw"
    , "try"
    , "typeof"
    , "var"
    , "void"
    , "while"
    , "with"
    , "yield"
    , "abstract"
    , "boolean"
    , "byte"
    , "char"
    , "double"
    , "final"
    , "float"
    , "goto"
    , "int"
    , "long"
    , "native"
    , "short"
    , "synchronized"
    , "throws"
    , "transient"
    , "volatile"
    ]
