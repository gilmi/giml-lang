-- | An AST of a subset of JavaScript
module Language.Backend.JS.Ast where

import Data.Int (Int32)
import Data.Map qualified as M
import Data.Text qualified as T

-- | A JavaScript file
data File
  = File [Statement]
  deriving (Show)

-- | A JavaScript definition
data Definition
  = Variable Var Expr
  | Function Var [Var] Block
  deriving (Show)

-- | A JavaScript scoped block
type Block = [Statement]

-- | A JavaScript statement
data Statement
  = -- | An expression
    SExpr Expr
  | -- | A return statement
    SRet Expr
  | -- | A definition
    SDef Definition
  | -- | An if statement (we don't need @else@ for Giml)
    SIf Expr Block
  | -- | A complex statement that shallowly clones an expression
    SRecordClone Var Expr
  | -- | Record label assignment
    SRecordAssign Var Label Expr
  deriving (Show)

-- | A JavaScript expression
data Expr
  = -- | A literal
    ELit Lit
  | -- | A variable
    EVar Var
  | -- | An anonymous function
    EFun [Var] Block
  | -- | A function call
    EFunCall Expr [Expr]
  | -- | A record
    ERecord (Record Expr)
  | -- | expressions separated by &&
    EAnd [Expr]
  | -- | Equality of two expressions
    EEquals Expr Expr
  | -- | a binary operator application
    EBinOp T.Text Expr Expr
  | -- | A negation of an expression
    ENot Expr
  | -- | Record label access
    ERecordAccess Expr Label
  | -- | A raw JavaScript string
    ERaw T.Text
  deriving (Show)

-- | A record label
type Label = T.Text

-- | A record
type Record a =
  M.Map Var a

-- | A JavaScript literal
data Lit
  = LInt Int32
  | LBool Bool
  | LFloat Float
  | LString T.Text
  deriving (Show)

-- | A JavaScript variable
type Var = T.Text
