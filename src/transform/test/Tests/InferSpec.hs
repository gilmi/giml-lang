{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}

module Tests.InferSpec where

import Control.Monad.Except
import Data.List.NonEmpty qualified as NEL
import Data.Text qualified as T
import Language.Giml
import Test.Hspec
import Text.RawString.QQ

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "infer" $ do
    lits
    lambdas
    funcalls
    functions
    variants
    let_polymorphism
    records
    complex

lits :: Spec
lits = do
  describe "literals" $ do
    it "assign lit" $
      shouldBe
        (testinfer "x = 1")
        (pure $ boilerplate "x" tInt $ ELit $ LInt 1)

    it "assign string" $
      shouldBe
        (testinfer [r|x = "hello"|])
        (pure $ boilerplate "x" tString $ ELit $ LString "hello")

    it "assign bool" $
      shouldBe
        (testinfer "x = True")
        (pure $ boilerplate "x" tBool $ ELit $ LBool True)

lambdas :: Spec
lambdas = do
  describe "lambdas" $ do
    it "assign id" $
      shouldBe
        (testinfer "id = \\x -> x")
        ( pure $
            boilerplate
              "id"
              (TypeScheme ["a"] $ typeFun [TypeVar "a"] (TypeVar "a"))
              (EFun [Just "x"] (EAnnotated (TypeVar "a") $ EVar "x"))
        )

funcalls :: Spec
funcalls = do
  describe "funcalls" $ do
    it "id 1" $
      shouldBe
        (testinfer [r|one = (\x -> x)(1)|])
        ( pure $
            boilerplate "one" tInt $
              EFunCall
                ( EAnnotated (typeFun [tInt] tInt) $
                    EFun [Just "x"] (EAnnotated tInt $ EVar "x")
                )
                [ EAnnotated tInt $
                    ELit $
                      LInt 1
                ]
        )

functions :: Spec
functions = do
  describe "functions" $ do
    it "builtin add" $
      shouldBe
        (testinfer "increment = add 1")
        ( pure $
            File [] $
              pure $
                pure $
                  Variable (typeFun [tInt] tInt) "increment" (Just $ typeFun [tInt] tInt) $
                    EAnnotated (typeFun [tInt] tInt) $
                      EFunCall
                        (EAnnotated (typeFun [tInt] $ typeFun [tInt] tInt) (EVar "add"))
                        [EAnnotated tInt $ ELit $ LInt 1]
        )

    it "builtin bool" $
      shouldBe
        (testinfer "nand x y = not (and x y)")
        ( pure $
            boilerplateFun
              "nand"
              (typeFun [tBool, tBool] tBool)
              ("x" NEL.:| ["y"])
              ( EAnnotated tBool $
                  EFunCall
                    (EAnnotated (typeFun [tBool] tBool) (EVar "not"))
                    [ EAnnotated tBool $
                        EFunCall
                          ( EAnnotated (typeFun [tBool] tBool) $
                              EFunCall
                                (EAnnotated (typeFun [tBool] (typeFun [tBool] tBool)) (EVar "and"))
                                [EAnnotated tBool $ EVar "x"]
                          )
                          [EAnnotated tBool $ EVar "y"]
                    ]
              )
        )

    it "id" $
      shouldBe
        (testinfer "id x = x")
        ( let
            typ = TypeScheme ["a"] $ typeFun [TypeVar "a"] (TypeVar "a")
          in
            pure $
              File [] $
                pure $
                  pure $
                    Function typ "id" (Just typ) [Just "x"] $
                      EAnnotated (TypeVar "a") (EVar "x")
        )

    it "const" $ do
      let
        tinner = typeFun [TypeVar "b"] (TypeVar "a")
        t = TypeScheme ["a", "b"] $ typeFun [TypeVar "a"] tinner
      shouldBe
        (testinfer "const x _ = x")
        ( pure $
            File [] $
              pure $
                pure $
                  Function t "const" (Just t) [Just "x"] $
                    EAnnotated tinner $
                      EFun [Nothing] $
                        EAnnotated (TypeVar "a") $
                          EVar "x"
        )

variants :: Spec
variants = do
  describe "variants" $ do
    it "Id" $
      shouldBe
        ( testinfer
            [r|
Id a =
	| Id a

oneId = Id 1
|]
        )
        ( pure $
            File
              [ Datatype
                  tUnit
                  "Id"
                  Nothing
                  ["a"]
                  [Variant "Id" (Just $ TypeVar "a")]
              ]
              [ let
                  typ = TypeApp (TypeCon "Id") tInt
                in
                  pure $
                    Variable typ "oneId" (Just typ) $
                      EAnnotated typ $
                        EFunCall
                          (EAnnotated (typeFun [tInt] $ TypeApp (TypeCon "Id") tInt) $ EVariant "Id")
                          [EAnnotated tInt $ ELit $ LInt 1]
              ]
        )

let_polymorphism :: Spec
let_polymorphism = do
  describe "let polymorphism" $ do
    it "id" $
      shouldBe
        ( testinfer
            [r|
id x = x
intId = id 1
strId = id "hello"
|]
        )
        ( let
            typ = TypeScheme ["a"] $ typeFun [TypeVar "a"] (TypeVar "a")
          in
            pure $
              File [] $
                [
                  [ Function typ "id" (Just typ) [Just "x"] $
                      EAnnotated (TypeVar "a") (EVar "x")
                  ]
                ,
                  [ Variable tInt "intId" (Just tInt) $
                      EAnnotated tInt $
                        EFunCall
                          (EAnnotated (typeFun [tInt] tInt) (EVar "id"))
                          [EAnnotated tInt $ ELit $ LInt 1]
                  ]
                ,
                  [ Variable tString "strId" (Just tString) $
                      EAnnotated tString $
                        EFunCall
                          (EAnnotated (typeFun [tString] tString) (EVar "id"))
                          [EAnnotated tString $ ELit $ LString "hello"]
                  ]
                ]
        )

    it "cyclic" $
      shouldBe
        ( testinfer
            [r|
x = y
y = x
|]
        )
        ( let
            typ = TypeScheme ["a"] $ TypeVar "a"
          in
            pure $
              File [] $
                pure $
                  [ Variable typ "x" (Just typ) $ EAnnotated (TypeVar "a") (EVar "y")
                  , Variable typ "y" (Just typ) $ EAnnotated (TypeVar "a") (EVar "x")
                  ]
        )

    it "out of order" $
      shouldBe
        ( testinfer
            [r|
x = id "hello"
id x = x
y = x
|]
        )
        ( let
            typ = TypeScheme ["a"] $ typeFun [TypeVar "a"] (TypeVar "a")
          in
            pure $
              File [] $
                [
                  [ Function typ "id" (Just typ) [Just "x"] $
                      EAnnotated (TypeVar "a") (EVar "x")
                  ]
                ,
                  [ Variable tString "x" (Just tString) $
                      EAnnotated tString $
                        EFunCall
                          (EAnnotated (typeFun [tString] tString) $ EVar "id")
                          [EAnnotated tString $ ELit $ LString "hello"]
                  ]
                , [Variable tString "y" (Just tString) $ EAnnotated tString (EVar "x")]
                ]
        )

records :: Spec
records = do
  describe "records" $ do
    it "record get" $
      shouldBe
        ( testinfer
            [r|
getX record = record.x

test = getX { x : 1 }
        |]
        )
        ( pure $
            File [] $
              [
                [ let
                    fieldType = TypeVar "a"
                    recType = TypeRecExt [("x", fieldType)] "b"
                    typ = TypeScheme ["a", "b"] $ typeFun [recType] fieldType
                  in
                    Function typ "getX" (Just typ) [Just "record"] $
                      EAnnotated fieldType $
                        ERecordAccess (EAnnotated recType (EVar "record")) "x"
                ]
              ,
                [ let
                    fieldType = tInt
                    recType = TypeRec [("x", tInt)]
                  in
                    Variable fieldType "test" (Just fieldType) $
                      EAnnotated fieldType $
                        EFunCall
                          (EAnnotated (typeFun [recType] fieldType) $ EVar "getX")
                          [ EAnnotated recType $ mkERec [("x", EAnnotated fieldType $ ELit $ LInt 1)] Nothing
                          ]
                ]
              ]
        )

    it "record get not a record" $
      shouldSatisfy
        ( testinfer
            [r|
getX record = record.x

test = getX 1
        |]
        )
        ( \case
            Left (TypeError (_, TypeMismatch (TypeCon "Int") (TypeRecExt [("x", TypeVar _)] _))) ->
              True
            _ -> False
        )

    it "record get no such label" $
      shouldSatisfy
        ( testinfer
            [r|
getX record = record.x

test = getX { y : 1 }
        |]
        )
        ( \case
            Left (TypeError (_, RecordDiff _ _ [] ["x"])) ->
              True
            _ -> False
        )

    it "record extend" $
      shouldBe
        ( testinfer
            [r|
setX record = { x : 1 | record }

test = setX { x : 2 }
        |]
        )
        ( pure $
            File [] $
              [
                [ let
                    fieldType = tInt
                    inrecType = TypeRecExt [] "a"
                    outrecType = TypeRecExt [("x", fieldType)] "a"
                    funType = TypeScheme ["a"] $ typeFun [inrecType] outrecType
                  in
                    Function funType "setX" (Just funType) [Just "record"] $
                      EAnnotated outrecType $
                        mkERec
                          [("x", EAnnotated fieldType $ ELit $ LInt 1)]
                          (Just $ EAnnotated inrecType (EVar "record"))
                ]
              ,
                [ let
                    fieldType = tInt
                    recType = TypeRec [("x", tInt)]
                  in
                    Variable recType "test" (Just recType) $
                      EAnnotated recType $
                        EFunCall
                          (EAnnotated (typeFun [recType] recType) $ EVar "setX")
                          [ EAnnotated recType $ mkERec [("x", EAnnotated fieldType $ ELit $ LInt 2)] Nothing
                          ]
                ]
              ]
        )

complex :: Spec
complex = do
  describe "complex" $ do
    it "foldable" $
      shouldSatisfy
        ( testinfer
            [r|
List a =
	 | Nil {}
	 | Cons { head : a, tail : List a }

lists =
	{ foldr : \f zero list ->
		case list of
			| Nil {} -> zero
			| Cons l ->
				f l.head (lists.foldr f zero l.tail)
	}

foldMap monoid class f =
	class.foldr (\x y -> monoid.append (f x) y) monoid.empty

addition =
	{ empty : 0
	, append : add
	}

sum class = foldMap addition class id

mylist = Cons { head : 1, tail : Nil {} }

id x = x

main = do
	ffi("console.log", sum lists mylist)
|]
        )
        ( \case
            Right {} -> True
            _ -> False
        )

-------------------------

boilerplate :: Var -> Type -> Expr Type -> File Type
boilerplate name typ expr =
  File [] [[Variable typ name (Just typ) $ EAnnotated (removeTypeScheme typ) expr]]

boilerplateFun :: Var -> Type -> NEL.NonEmpty Var -> Expr Type -> File Type
boilerplateFun name typ (first NEL.:| rest) body =
  File [] $ pure [Function typ name (Just typ) [Just first] $ EAnnotated (removeTypeApp typ) $ efun rest body]

removeTypeScheme :: Type -> Type
removeTypeScheme = \case
  TypeScheme _ t -> t
  t -> t

removeTypeApp :: Type -> Type
removeTypeApp = \case
  TypeApp _ ret -> ret
  t -> t

testinfer :: T.Text -> Either (Error T.Text) (File Type)
testinfer = runExcept . fmap (fmap annType) . parseInferPipeline' noLogging "test"
