{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}

module Tests.NegativeSpec where

import Control.Monad.Except
import Data.Foldable
import Data.List (sort)
import Data.Text qualified as T
import Language.Giml
import Language.Giml.Syntax.Lexer qualified as Lex (keywords)
import Test.Hspec
import Text.RawString.QQ

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "negative" $ do
    duplicates
    illTyped
    keywords

duplicates :: Spec
duplicates = do
  describe "duplicates" $ do
    it "duplicate function arguments" $
      shouldSatisfy
        (testinfer "f x y x = x")
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["x", "x"])])) -> True
            _ -> False
        )

    it "duplicate lambda arguments" $
      shouldSatisfy
        (testinfer [r|f = \x y x -> x|])
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["x", "x"])])) -> True
            _ -> False
        )

    it "duplicate term defs" $
      shouldSatisfy
        ( testinfer
            [r|
id x = x
id = \x -> x
        |]
        )
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["id", "id"])])) -> True
            _ -> False
        )

    it "duplicate type defs" $
      shouldSatisfy
        ( testinfer
            [r|
A = | A {}
A = | B {}
        |]
        )
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["A", "A"])])) -> True
            _ -> False
        )

    it "duplicate typedef args" $
      shouldSatisfy
        ( testinfer
            [r|
A a a = | A a
        |]
        )
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["a", "a"])])) -> True
            _ -> False
        )

    it "duplicate typedef variants" $
      shouldSatisfy
        ( testinfer
            [r|
A = | B {} | B {}
        |]
        )
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["B", "B"])])) -> True
            _ -> False
        )

    it "duplicate pattern captures" $
      shouldSatisfy
        ( testinfer
            [r|
test = case { x : 1, y : 2 } of
	| { x : z, y : z } -> z
|]
        )
        ( \case
            Left (PreInferError (DuplicateNames [(_, ["z", "z"])])) -> True
            _ -> False
        )

illTyped :: Spec
illTyped = do
  describe "Ill typed" $ do
    io
    ffi
    openVariants

    it "add mismatch" $
      shouldSatisfy
        (testinfer [r|x = add 1 "hi"|])
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt, t2 == tString -> True
            _ -> False
        )

    it "case arms mismatch" $
      shouldSatisfy
        (testinfer [r|x = case True of | True -> 1 | False -> "0"|])
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt, t2 == tString -> True
            _ -> False
        )

    it "pattern mismatch" $
      shouldSatisfy
        ( testinfer
            [r|
x = case 1 of
	| "hello" -> 2
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t2 == tInt, t1 == tString -> True
            _ -> False
        )

    it "pattern result mismatch" $
      shouldSatisfy
        ( testinfer
            [r|
x = case 1 of
	| 1 -> 1
	| n -> "hello"
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt, t2 == tString -> True
            _ -> False
        )

    it "sum list int" $
      shouldSatisfy
        ( testinfer
            [r|
List a =
	 | Nil {}
	 | Cons { head : a, tail : List a }

lists =
	{ foldr : \f zero list ->
		case list of
			| Nil {} -> zero
			| Cons l ->
				f l.head (lists.foldr f zero l.tail)
	}

foldMap monoid class f =
	class.foldr (\x y -> monoid.append (f x) y) monoid.empty

addition =
	{ empty : 0
	, append : add
	}

sum class = foldMap addition class id

mylist = Cons { head : "hi", tail : Nil {} }

id x = x

main = do
	ffi("console.log", sum lists mylist)

|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | sort [tInt, tString] == sort [t1, t2] -> True
            _ -> False
        )

    it "record list subset labels" $
      shouldSatisfy
        ( testinfer
            [r|
List a =
	| Nil
	| Cons { head : a, tail : List a }

list = Cons { head : { x : 1, y : 1 }, tail : Cons { head : { x : 1 }, tail : Nil } }
|]
        )
        ( \case
            Left (TypeError (_, RecordDiff _ _ t1 t2))
              | sort [["y"], []] == sort [t1, t2] -> True
            _ -> False
        )

io :: Spec
io = do
  describe "IO" $ do
    it "pure" $
      shouldSatisfy
        ( testinfer
            [r|
x = do
	y <- pure 1
	pure y

main = do
	ffi("console.log", add 1 x)
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt, t2 == tIO tInt -> True
            _ -> False
        )

    it "newIORef" $
      shouldSatisfy
        ( testinfer
            [r|
mkZero = newIORef 0

main = do
	ref <- mkZero
	ffi("console.log" : Int -> IO {}, ref)
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tIO tInt, t2 == tInt -> True
              | t1 == tInt, t2 == tIORef tInt -> True
            _ -> False
        )

    it "writeIORef" $
      shouldSatisfy
        ( testinfer
            [r|
main = do
	ref <- writeIORef 0 0
  |]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | sort [tIORef (TypeVar "ti5"), tInt] == sort [t1, t2] -> True
            _ -> False
        )

ffi :: Spec
ffi = do
  describe "FFI" $ do
    it "console.log input type mismatch" $
      shouldSatisfy
        ( testinfer
            [r|
main = do
	ffi("console.log" : Int -> IO {}, "Hello")
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt, t2 == tString -> True
            _ -> False
        )

    it "console.log input arity mismatch 1" $
      shouldSatisfy
        ( testinfer
            [r|
main = do
	ffi("console.log" : Int -> Int -> IO {}, 1)
|]
        )
        ( \case
            Left (TypeError (_, ArityMismatch t1 t2))
              | t1 == typeFun [tInt, tInt] (tIO tUnit)
              , t2 == typeFun [tInt] (TypeVar "t1") ->
                  True
            _ -> False
        )

    it "console.log input arity mismatch 2" $
      shouldSatisfy
        ( testinfer
            [r|
main = do
	ffi("console.log" : Int -> IO {}, 1, 2)
|]
        )
        ( \case
            Left (TypeError (_, ArityMismatch t1 t2))
              | t1 == typeFun [tInt] (tIO tUnit)
              , t2 == typeFun [tInt, tInt] (TypeVar "t1") ->
                  True
            _ -> False
        )

openVariants :: Spec
openVariants = do
  describe "open variants" $ do
    it "wrong poly -> concrete" $
      shouldSatisfy
        ( testinfer
            [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

main = do
	ffi("console.log", withDefault 0 (#Rum 1))
|]
        )
        ( \case
            Left (TypeError (_, VariantDiff _ _ _ ["Rum"])) ->
              True
            _ -> False
        )

    it "wrong type" $
      shouldSatisfy
        ( testinfer
            [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

main = do
	ffi("console.log", withDefault 0 (#Some True))
|]
        )
        ( \case
            Left (TypeError (_, TypeMismatch t1 t2))
              | t1 == tInt
              , t2 == tBool ->
                  True
            _ -> False
        )

    it "wrong variant output->input" $
      shouldSatisfy
        ( testinfer
            [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

incrementMaybe x =
	case x of
		| #Some n -> #Some (add n 1)
		| #Nil {} -> #Mil {}

main = do
	ffi("console.log", withDefault 0 (incrementMaybe (#Some 1)))
|]
        )
        ( \case
            Left (TypeError (_, VariantDiff _ _ _ ["Mil"])) ->
              True
            _ -> False
        )

    it "wrong variant output UB->input" $
      shouldSatisfy
        ( testinfer
            [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

incrementMaybe x =
	case x of
		| #Some n -> #Some (add n 1)
		| a -> a

main = do
	ffi("console.log", withDefault 0 (incrementMaybe (#Mil 1)))
|]
        )
        ( \case
            Left (TypeError (_, VariantDiff _ _ _ ["Mil"])) ->
              True
            _ -> False
        )

keywords :: Spec
keywords = do
  describe "keywords" $ do
    for_ ((,) <$> keywordScenarios <*> Lex.keywords) $ \(scenarioFun, keyword) -> do
      let
        scenario = scenarioFun keyword
      it (T.unpack scenario) $
        shouldSatisfy
          (testinfer scenario)
          ( \case
              Left (ParseError txt) -> True
              --              | "keyword" `T.isInfixOf` txt -> True
              _ -> False
          )

keywordScenarios :: [T.Text -> T.Text]
keywordScenarios =
  [ \w -> w <> " = 1"
  , \w -> "x = let " <> w <> " = 1 in 2"
  , \w -> "A " <> w <> " = |"
  , \w -> "x = { " <> w <> " : 1 }"
  ]

-------------------------

testinfer :: T.Text -> Either (Error T.Text) (File Type)
testinfer = runExcept . fmap (fmap annType) . parseInferPipeline' noLogging "test"
