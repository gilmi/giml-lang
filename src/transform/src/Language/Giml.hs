{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Exporting the important stuff from the giml frontend
module Language.Giml (
  -- * Giml frontend
  module Language.Giml,

  -- * Giml language definition
  module Language.Giml.Syntax.Ast,

  -- * Giml parser
  module Language.Giml.Syntax.Parser,

  -- * Giml Type Inference
  module Language.Giml.Types.Infer,

  -- * Giml builtin functions and types
  module Language.Giml.Builtins,

  -- * Giml Rewrites
  module Language.Giml.Rewrites,
  module Language.Giml.Rewrites.PostInfer.RemoveAnn,

  -- * Giml pretty printing
  module Language.Giml.Pretty,

  -- * Giml logging
  module Language.Giml.Logging,
) where

import Control.Monad.Except
import Language.Giml.Builtins
import Language.Giml.Logging
import Language.Giml.Pretty (printSourcePos, printType)
import Language.Giml.Rewrites
import Language.Giml.Rewrites.PostInfer.RemoveAnn (removeAnn)
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser (SourcePos (..), dummyAnn, parseFile, runParser)
import Language.Giml.Types.Infer (Ann (..), InputAnn, TypeError (..), TypeErrorA, getType, infer, ppTypeError, printAnn)
import Language.Giml.Utils

-- * Giml Parser

-- | Parse a Giml source file from text
parse :: (CompilePhase Text env b m) => FilePath -> Text -> m (ParsedFile SourcePos)
parse path txt =
  setStage Parsing $ do
    logGeneral "Parsing stage"
    runParser parseFile path txt

-- * Giml Type Inference

-- | Parse and infer a Giml source file from text
parseInferPipeline :: (MonadBase b b) => LogAction b LogMsg -> FilePath -> Text -> ExceptT Text b (File Ann)
parseInferPipeline logact file input =
  withExceptT errorToText $ parseInferPipeline' logact file input

-- | Parse and infer a Giml source file from text, keeping errors structure
parseInferPipeline' :: (MonadBase b b) => LogAction b LogMsg -> FilePath -> Text -> ExceptT (Error Text) b (File Ann)
parseInferPipeline' logact path src = do
  parsed <- withExceptT ParseError $ runLog logact $ parse path src
  inferPipeline' logact parsed

-- | Infer a Giml File, keeping errors structure
inferPipeline'
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> ParsedFile InputAnn
  -> ExceptT (Error Text) b (File Ann)
inferPipeline' logact parsed = do
  rewritten <- withExceptT PreInferError $ runLog logact $ preInferRewrites parsed
  inferred <-
    withExceptT TypeError $ do
      infer logact rewritten
  runLog logact $ postInferRewrites inferred

-- * Errors

data Error parseError
  = ParseError parseError
  | TypeError TypeErrorA
  | PreInferError PreInferError
  deriving (Show, Eq, Ord)

errorToText :: Error Text -> Text
errorToText = \case
  ParseError txt -> txt
  TypeError typeErr -> ppTypeError typeErr
  PreInferError err -> pShow err
