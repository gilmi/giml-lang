{-# LANGUAGE OverloadedStrings #-}

-- | Builtin stuff
--
-- Includes:
--
-- - builtin types
-- - values
-- - datatypes
-- - function implementations
module Language.Giml.Builtins (
  -- * Types
  tUnit,
  tInt,
  tFloat,
  tString,
  tBool,
  tIO,
  tIORef,

  -- * Values
  true,
  false,
  unit,

  -- * Data types
  builtinDatatypes,
  dataBool,
  dataOption,

  -- * Functions
  Builtins,
  Builtin (..),
  Impl (..),
  builtins,
) where

import Data.Map qualified as M
import Data.Text qualified as T
import Language.Giml.Syntax.Ast

-- * Types

tUnit :: Type
tUnit = TypeRec mempty

tInt :: Type
tInt = TypeCon "Int"

tFloat :: Type
tFloat = TypeCon "Float"

tString :: Type
tString = TypeCon "String"

tBool :: Type
tBool = TypeCon "Bool"

-- | A type representing IO actions
tIO :: Type -> Type
tIO = TypeApp (TypeCon "IO")

-- | A type representing IO actions
tIORef :: Type -> Type
tIORef = TypeApp (TypeCon "IORef")

-- * Values

true :: Expr ()
true = EVariant "True"

false :: Expr ()
false = EVariant "False"

unit :: Expr ()
unit = ERecord mempty

-- * Data types

builtinDatatypes :: [Datatype ()]
builtinDatatypes =
  [ dataBool
  , dataOption
  ]

dataBool :: Datatype ()
dataBool =
  Datatype
    ()
    "Bool"
    (Just Type)
    []
    [ Variant "True" Nothing
    , Variant "False" Nothing
    ]

dataOption :: Datatype ()
dataOption =
  Datatype
    ()
    "Option"
    (Just $ KindFun Type Type)
    ["a"]
    [ Variant "Some" (Just $ TypeVar "a")
    , Variant "None" Nothing
    ]

-- * Functions

-- | A Built-in function
data Builtin = Builtin
  { bName :: Var
  -- ^ the name
  , bType :: Type
  -- ^ the type
  , bImpl :: Impl
  -- ^ the implementation
  }

-- | The type of the function and it's
--   implementation in the target source code
data Impl
  = Func T.Text
  | BinOp T.Text

type Builtins = M.Map Var Builtin

-- | All of the builtin functions
builtins :: Builtins
builtins =
  M.unions
    [ ints
    , bools
    , strings
    , io
    ]

-- | Integer builtins
ints :: Builtins
ints =
  M.fromList
    [ binop "add" binInt "+"
    , binop "sub" binInt "-"
    , binop "mul" binInt "*"
    , binop "div" binInt "/"
    , func
        "negate"
        (typeFun [tInt] tInt)
        "function (x) { return 0 - x; }"
    , binop
        "int_equals"
        (typeFun [tInt, tInt] tBool)
        "==="
    , binop
        "int_lesser"
        (typeFun [tInt, tInt] tBool)
        "<"
    , binop
        "int_lesser_eq"
        (typeFun [tInt, tInt] tBool)
        "<="
    , binop
        "int_greater"
        (typeFun [tInt, tInt] tBool)
        ">"
    , binop
        "int_greater_eq"
        (typeFun [tInt, tInt] tBool)
        ">="
    , func
        "parseInt"
        (typeFun [tString] tInt)
        "function (n) { return parseInt(n); }"
    , func
        "intToString"
        (typeFun [tInt] tString)
        "function (n) { return n.toString(); }"
    ]
  where
    binInt = typeFun [tInt, tInt] tInt

-- | Boolean builtins
bools :: Builtins
bools =
  M.fromList
    [ binop
        "and"
        (typeFun [tBool, tBool] tBool)
        "&&"
    , binop
        "or"
        (typeFun [tBool, tBool] tBool)
        "||"
    , func
        "not"
        (typeFun [tBool] tBool)
        "function (x) { return !x; }"
    ]

-- | String builtins
strings :: Builtins
strings =
  M.fromList
    [ binop
        "concat"
        (typeFun [tString, tString] tString)
        "+"
    ]

-- | Builtin IO operations
--   We represent IO actions as functions without arguments
io :: Builtins
io =
  M.fromList
    [ func
        "pure"
        (foral ["a"] $ typeFun [TypeVar "a"] (tIO (TypeVar "a")))
        "function(x) { return function() { return x; }; }"
    ]
    `M.union` ioRef

-- | Builtin IORef operations
--
--   IORefs are mutable variables that can be manipulated from IO code.
--
--   They are represented as objects with a @value@ field which holds the value.
ioRef :: Builtins
ioRef =
  M.fromList
    [ func
        "newIORef"
        (foral ["a"] $ typeFun [TypeVar "a"] (tIO (tIORef (TypeVar "a"))))
        "function(x) { return function() { return { value : x }; }; }"
    , func
        "readIORef"
        (foral ["a"] $ typeFun [tIORef (TypeVar "a")] (tIO (TypeVar "a")))
        "function(x) { return function() { return x.value; }; }"
    , func
        "writeIORef"
        (foral ["a"] $ typeFun [tIORef (TypeVar "a"), TypeVar "a"] (tIO tUnit))
        "function(x) { return function(v) { return function() { x.value = v; return {}; }; }; }"
    , func
        "modifyIORef"
        ( foral ["a"] $
            typeFun
              [ tIORef (TypeVar "a")
              , typeFun [TypeVar "a"] (TypeVar "a")
              ]
              (tIO tUnit)
        )
        "function(x) { return function(f) { return function() { x.value = f(x.value); return {}; }; }; }"
    ]

-- | Binary operator helper constructor
binop :: Var -> Type -> T.Text -> (Var, Builtin)
binop name typ impl =
  (name, Builtin name typ (BinOp impl))

-- | Function helper constructor
func :: Var -> Type -> T.Text -> (Var, Builtin)
func name typ impl =
  (name, Builtin name typ (Func impl))

foral :: [TypeVar] -> Type -> Type
foral = TypeScheme
