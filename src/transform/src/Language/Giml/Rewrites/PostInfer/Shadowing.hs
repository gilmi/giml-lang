{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

-- | Warn about variable shadowing
module Language.Giml.Rewrites.PostInfer.Shadowing where

import Control.Monad.Reader
import Data.Generics.Uniplate.Data qualified as U
import Data.Map qualified as M
import Data.Text qualified as T
import Language.Giml.Logging
import Language.Giml.Pretty
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer (Ann, annInput)
import Language.Giml.Utils

data Env b = Env
  { logAction :: LogAction b LogMsg
  , varEnv :: Map Var Ann
  }
  deriving (Generic)

-- | Check for variable shadowing in a File.
check
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> File Ann
  -> b (File Ann)
check logact file = do
  checkFile logact file
  pure file

checkFile
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> File Ann
  -> b ()
checkFile logact (File _ (concat -> termdefs)) =
  let
    toplevel =
      M.fromList $
        map (\t -> (getTermName t, getTermAnn t)) termdefs
  in
    flip runReaderT (Env logact toplevel) $
      mapM_ checkTermDef termdefs

checkTermDef
  :: (HasLog' LogMsg (Env b) b m)
  => TermDef Ann
  -> m ()
checkTermDef = \case
  Variable ann _ _ e ->
    checkExpr ann e
  Function ann _ _ args e ->
    withVars
      (M.fromList $ map (flip (,) ann) (catMaybes args))
      (checkExpr ann e)

checkExpr
  :: (HasLog' LogMsg (Env b) b m)
  => Ann
  -> Expr Ann
  -> m ()
checkExpr ann = \case
  EAnnotated ann' e ->
    checkExpr ann' e
  ELet def e -> do
    checkTermDef def
    let
      name = getTermName def
    withVars (M.singleton name ann) (checkExpr ann e)
  EFun args e -> do
    withVars
      (M.fromList $ map (flip (,) ann) (catMaybes args))
      (checkExpr ann e)
  ECase e pats -> do
    checkExpr ann e
    mapM_ (uncurry $ checkPattern ann) pats
  EBlock block ->
    checkBlock block
  e ->
    mapM_ (checkExpr ann) (U.children e)

checkBlock
  :: (HasLog' LogMsg (Env b) b m)
  => Block Ann
  -> m ()
checkBlock = \case
  [] -> pure ()
  SExpr ann e : rest -> do
    checkExpr ann e
    checkBlock rest
  SBind ann v e : rest -> do
    checkExpr ann e
    withVars (M.singleton v ann) (checkBlock rest)
  SDef ann def : rest -> do
    checkTermDef def
    let
      name = getTermName def
    withVars (M.singleton name ann) (checkBlock rest)

checkPattern
  :: (HasLog' LogMsg (Env b) b m)
  => Ann
  -> Pattern
  -> Expr Ann
  -> m ()
checkPattern ann pat body = do
  let
    captures = getCaptures ann pat
  withVars captures (checkExpr ann body)

getCaptures :: Ann -> Pattern -> Map Var Ann
getCaptures ann pat =
  M.fromList [(v, ann) | PVar v <- U.universeBi pat]

withVars
  :: (HasLog' LogMsg (Env b) b m)
  => Map Var Ann
  -> m ()
  -> m ()
withVars vars m = do
  env <- asks varEnv
  let
    shadowings = M.intersectionWith (,) vars env
  mapM_ (uncurry warnShadowing) (M.toList shadowings)

  flip local m $ \(Env l e) ->
    Env l (M.union vars e)

warnShadowing
  :: (HasLog' LogMsg (Env b) b m)
  => Var
  -> (Ann, Ann)
  -> m ()
warnShadowing name (new, old) =
  warn $
    T.unwords
      [ "The variable '" <> name <> "'"
      , "defined in location " <> printSourcePos (annInput new)
      , "shadows a variable previously defined in location: "
          <> printSourcePos (annInput old)
          <> "."
      ]
