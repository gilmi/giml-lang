{-# LANGUAGE FlexibleContexts #-}

-- | AST rewrites that run before type inference
module Language.Giml.Rewrites.PreInfer (
  module Language.Giml.Rewrites.PreInfer,
  module Export,
) where

import Language.Giml.Logging
import Language.Giml.Rewrites.PreInfer.CheckDuplicates qualified as Dups
import Language.Giml.Rewrites.PreInfer.Currying qualified as Currying
import Language.Giml.Rewrites.PreInfer.Errors as Export
import Language.Giml.Rewrites.PreInfer.GroupDefsByDeps qualified as Group
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser qualified as Parser
import Language.Giml.Utils

-- | Rewrites that should run before type inference
rewrites
  :: (CompilePhase PreInferError env b m)
  => (MonadBase b b)
  => ParsedFile Parser.Ann
  -> m (File Parser.Ann)
rewrites =
  ( Dups.rewrite
      >=> pure . Group.rewrite
      >=> pure . Currying.rewrite
  )
