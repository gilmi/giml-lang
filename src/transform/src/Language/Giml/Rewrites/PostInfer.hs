{-# LANGUAGE FlexibleContexts #-}

-- | AST rewrites that run before type inference
module Language.Giml.Rewrites.PostInfer where

-- import Language.Giml.Utils
import Language.Giml.Logging
import Language.Giml.Rewrites.PostInfer.Shadowing qualified as Shadowing
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer qualified as Infer

-- | Rewrites that should run after type inference
rewrites
  :: (HasLog' LogMsg env b m)
  => (MonadBase b b)
  => File Infer.Ann
  -> m (File Infer.Ann)
rewrites =
  ( \file -> do
      logact <- getLogAction
      liftBase $ Shadowing.check logact file
  )

--  ( RemoveAnn.rewrite
--  . ltrace "after-inference"
--  )
