{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

-- | Convert functions of multiple arguments to a chain of functions of a single argument
--
-- @
-- \a b c -> add (add a b) c
-- @
--
-- should become:
--
-- @
-- \a -> \b -> \c -> add (add a b) c
-- @
--
-- And function application should change too:
--
-- @
-- EFunCall "add" [EVar "a", EVar "b"]
-- @
--
-- should become:
--
-- @
-- EFunCall (EFunCall "add" [EVar "a"]) [EVar "b"]
-- @
--
-- Maybe EFunCall should be converted to a different constructor?
-- Such as @EApp (Expr a) (Expr a)@, but we might do that later.
module Language.Giml.Rewrites.PreInfer.Currying where

import Data.Generics.Uniplate.Data qualified as U
import Language.Giml.Syntax.Ast
import Language.Giml.Utils

rewrite :: (Data a) => File a -> File a
rewrite = rewrite'

rewrite' :: forall f a. (Data a) => (Data (f a)) => f a -> f a
rewrite' =
  ( U.transformBi @(f a) @(Expr a) impl
      . U.transformBi @(f a) @(TermDef a) desugarFun
  )

-- | Convert functions of multiple arguments to a chain of functions of a single argument
--   And application of functions of multiple arguments to a chain of applications
impl :: (Data a) => Expr a -> Expr a
impl = U.rewrite $ \case
  EAnnotated ann (EFun (a1 : a2 : args) body) ->
    Just
      $ EAnnotated
        ann
      $ EFun [a1]
      $ EAnnotated
        ann
        (EFun (a2 : args) body)
  EAnnotated ann (EFunCall (EAnnotated fann f) (a1 : a2 : args)) ->
    Just $
      EAnnotated ann $
        EFunCall
          (EAnnotated fann $ EFunCall f [a1])
          (a2 : args)
  _ -> Nothing

-- | Desugar a Function term definition to a single parameter functions with a lambda
desugarFun :: TermDef a -> TermDef a
desugarFun = \case
  Function ann typ name [arg] body ->
    Function ann typ name [arg] (EAnnotated ann body)
  Function ann typ name (arg : args) body ->
    Function ann typ name [arg] (EAnnotated ann $ EFun args body)
  d -> d
