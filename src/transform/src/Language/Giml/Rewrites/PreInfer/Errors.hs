module Language.Giml.Rewrites.PreInfer.Errors where

import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser

data PreInferError
  = DuplicateNames [([Ann], [Var])]
  deriving (Show, Eq, Ord)
