{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

-- | Group definitions by their dependencies
--
-- In order to do type inference properly, we need to solve constraints in groups and in a certain order:
--
-- 1. The constraints expressions that depend on definitions should be solved __after__ the constraints
--    of said definitions are solved
-- 2. The constraints of expressions that depend on one another should be solved __at the same time__
--
-- This module groups and orders definitions in order of their dependencies.
module Language.Giml.Rewrites.PreInfer.GroupDefsByDeps where

import Data.Generics.Uniplate.Data qualified as U
import Data.Graph qualified as G (SCC (..), flattenSCC, stronglyConnComp)
import Data.Map qualified as M
import Data.Set qualified as S
import Language.Giml.Syntax.Ast
import Language.Giml.Utils

-- | Reorder and group the definitions in a file in order of their dependencies.
rewrite :: forall a. ParsedFile a -> File a
rewrite (ParsedFile defs) =
  let
    reach :: [((TermDef a), Var, [Var])]
    reach =
      mapMaybe
        ( fmap ((\def -> (def, getTermName def, S.toList $ freeVars def)))
            . getTermDef
        )
        defs
    grouped :: [G.SCC (TermDef a)]
    grouped = G.stronglyConnComp reach
  in
    File (mapMaybe getTypeDef defs) (map G.flattenSCC grouped)

-- ** Find dependencies

-- | The types that are bound in an expression.
type Scope = Set Var

-- | Calculate the free variables in a definition - these are the
--   top level definitions that this definition depends on.
freeVars :: TermDef a -> Set Var
freeVars = \case
  Variable _ _ _ e ->
    runReader (freeVarsExpr e) mempty
  Function _ name _ args e ->
    runReader (freeVarsExpr e) (S.insert name $ S.fromList $ catMaybes args)

-- | The free variables in an expression.
freeVarsExpr :: (MonadReader Scope m) => Expr a -> m (Set Var)
freeVarsExpr = \case
  EAnnotated _ e ->
    freeVarsExpr e
  ELit {} ->
    pure mempty
  EVar var -> do
    ismember <- asks (S.member var)
    pure $ if ismember then mempty else S.singleton var
  EOp OpDef {opName} -> do
    ismember <- asks (S.member opName)
    pure $ if ismember then mempty else S.singleton opName
  EFun args expr ->
    local (S.union (S.fromList $ catMaybes args)) $ freeVarsExpr expr
  EFunCall e1 e2 ->
    fmap S.unions . (:) <$> freeVarsExpr e1 <*> traverse freeVarsExpr e2
  EBlock block ->
    freeVarsBlock block
  ELet (Variable _ name _ e) e' ->
    S.union <$> freeVarsExpr e <*> local (S.union $ S.singleton name) (freeVarsExpr e')
  ELet (Function _ name _ args e) e' ->
    S.union
      <$> local (S.union (S.insert name $ S.fromList $ catMaybes args)) (freeVarsExpr e)
      <*> local (S.union $ S.singleton name) (freeVarsExpr e')
  EVariant {} ->
    pure mempty
  EOpenVariant {} ->
    pure mempty
  ERecord record ->
    S.unions <$> traverse freeVarsExpr (M.elems record)
  ERecordAccess e _ ->
    freeVarsExpr e
  ERecordExtension record ext ->
    fmap S.unions . (:) <$> freeVarsExpr ext <*> traverse freeVarsExpr (M.elems record)
  ECase e pats ->
    fmap S.unions . (:)
      <$> freeVarsExpr e
      <*> traverse freeVarsPat pats
  EIf e1 e2 e3 ->
    S.unions <$> traverse freeVarsExpr [e1, e2, e3]
  EFfi _ _ exprs ->
    S.unions <$> traverse freeVarsExpr exprs

-- | The free variables on a single @pattern -> body@ pair.
freeVarsPat :: (MonadReader Scope m) => (Pattern, Expr a) -> m (Set Var)
freeVarsPat (pat, expr) =
  let
    captures = [var | PVar var <- U.universe pat]
  in
    local (S.union $ S.fromList captures) (freeVarsExpr expr)

-- | The free variables in a block.
freeVarsBlock :: (MonadReader Scope m) => Block a -> m (Set Var)
freeVarsBlock = \case
  [] -> pure mempty
  SExpr _ e : block ->
    S.union <$> freeVarsExpr e <*> freeVarsBlock block
  SDef _ (Variable _ name _ e) : block ->
    S.union <$> freeVarsExpr e <*> local (S.union $ S.singleton name) (freeVarsBlock block)
  SDef _ (Function _ name _ args e) : block ->
    S.union
      <$> local (S.union (S.insert name $ S.fromList $ catMaybes args)) (freeVarsExpr e)
      <*> local (S.union $ S.singleton name) (freeVarsBlock block)
  SBind _ name e : block ->
    S.union <$> freeVarsExpr e <*> local (S.union $ S.singleton name) (freeVarsBlock block)
