{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

-- | Check for duplicate names
--
-- We want to make sure the user does not write the same argument name twice
-- in a give anonymous function, function definition, type definition, and variants names.
--
-- We also do not want the same name in pattern captures. (This is not datalog/prolog yet!)
module Language.Giml.Rewrites.PreInfer.CheckDuplicates where

import Control.Monad.Except
import Data.Generics.Uniplate.Data qualified as U
import Data.List
import Language.Giml.Rewrites.PreInfer.Errors
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser
import Language.Giml.Utils

-- | Check for duplicate names in a File.
rewrite :: (MonadError PreInferError m) => ParsedFile Ann -> m (ParsedFile Ann)
rewrite = rewrite'

rewrite' :: forall f m. (MonadError PreInferError m) => (Data (f Ann)) => f Ann -> m (f Ann)
rewrite' =
  ( U.transformBiM @m @(f Ann) @(Expr Ann) implExpr
      <=< U.transformBiM @m @(f Ann) @(Definition Ann) implDef
      <=< U.transformBiM @m @(f Ann) @(ParsedFile Ann) implDefs
  )

implExpr :: (MonadError PreInferError m) => Expr Ann -> m (Expr Ann)
implExpr = \case
  EAnnotated ann (EFun args _)
    | dups@(_ : _) <- duplicates id (catMaybes args) ->
        throwError $ DuplicateNames (map ((,) [ann]) dups)
  EAnnotated ann (ECase _ (map fst -> patterns))
    | dups@(_ : _) <- filter (not . null) $ concatMap dupPattern patterns ->
        throwError $ DuplicateNames (map ((,) [ann]) dups)
  e -> pure e

dupPattern :: Pattern -> [[Var]]
dupPattern pat = duplicates id [pvar | PVar pvar <- U.universe pat]

implDef :: (MonadError PreInferError m) => Definition Ann -> m (Definition Ann)
implDef = \case
  TermDef (Function ann _ _ args _)
    | dups@(_ : _) <- duplicates id (catMaybes args) ->
        throwError $ DuplicateNames (map ((,) [ann]) dups)
  TypeDef (Datatype ann _ _ args (map (\(Variant name _) -> name) -> variants))
    | dups@(_ : _) <- map unTypeVar <$> duplicates id args ->
        throwError $ DuplicateNames (map ((,) [ann]) dups)
    | dups@(_ : _) <- map unConstr <$> duplicates id variants ->
        throwError $ DuplicateNames (map ((,) [ann]) dups)
  e -> pure e

implDefs :: (MonadError PreInferError m) => ParsedFile Ann -> m (ParsedFile Ann)
implDefs (ParsedFile defs) =
  let
    termNames :: [(Ann, Text)]
    termNames =
      mapMaybe (fmap (\def -> (getTermAnn def, getTermName def)) . getTermDef) defs
    typeNames :: [(Ann, Text)]
    typeNames =
      mapMaybe (fmap (\def -> (getTypeAnn def, unTypeCon $ getTypeName def)) . getTypeDef) defs
  in
    case (duplicates snd termNames, duplicates snd typeNames) of
      ([], []) -> pure $ ParsedFile defs
      (dups1, dups2) ->
        throwError $ DuplicateNames $ map unzip dups1 <> map unzip dups2

duplicates :: (Ord b) => (a -> b) -> [a] -> [[a]]
duplicates f = filter ((> 1) . length) . groupOn f . sortOn f

groupOn :: (Eq b) => (a -> b) -> [a] -> [[a]]
groupOn f list =
  case list of
    [] -> []
    _ : _ ->
      uncurry (:) $
        foldr
          ( \y (y' : curr, rest) ->
              if f y == f y'
                then (y : y' : curr, rest)
                else ([y], (y' : curr) : rest)
          )
          ([last list], [])
          (init list)
