{-# LANGUAGE FlexibleContexts #-}

-- | AST rewrites
module Language.Giml.Rewrites (
  module Language.Giml.Rewrites,
  PreInfer.PreInferError (..),
) where

-- import Language.Giml.Utils
import Language.Giml.Logging
import Language.Giml.Rewrites.PostInfer qualified as PostInfer
import Language.Giml.Rewrites.PreInfer qualified as PreInfer
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser qualified as Parser
import Language.Giml.Types.Infer qualified as Infer

-- | Rewrites that should run before type inference
preInferRewrites
  :: (CompilePhase PreInfer.PreInferError env b m)
  => (MonadBase b b)
  => ParsedFile Parser.Ann
  -> m (File Parser.Ann)
preInferRewrites =
  setStage PreInfer . PreInfer.rewrites

-- | Rewrites that should run after type inference
postInferRewrites
  :: (HasLog' LogMsg env b m)
  => (MonadBase b b)
  => File Infer.Ann
  -> m (File Infer.Ann)
postInferRewrites =
  setStage PostInfer . PostInfer.rewrites
