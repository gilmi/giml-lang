{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}

-- | Solve type inference constraints
--
-- In this phase we go over the constraints one by one and try to __unify__ them.
--
-- For example, if we see @Equality (TypeVar "t1") (TypeCon "Int")@, we create
-- a mapping from @t1@ to @Int@ (called a __substitution__) and
-- we go over the rest of the constraints and replace @t1@ with @Int@ (__apply the substitution__).
--
-- We also keep all the substitutions we created from the constraints (and merge them to one substitution
-- but applying new substitution to the accumulated substitution).
--
-- If we see @Equality (TypeCon "Int") (TypeCon "String")@, we throw a type error,
-- because the two types do not match.
--
-- We keep going until there are no more constraints or until we encountered an error.
--
-- The result of the algorithm is the accumulated substitution.
module Language.Giml.Types.Infer.Solve where

import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Data.List (sort)
import Data.Map qualified as M
import Data.Map.Merge.Strict qualified as M
import Data.Set qualified as S
import Language.Giml.Logging
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer.Elaborate (ElabEnv (..), ElabState (..), genTypeVar, instantiate)
import Language.Giml.Types.Infer.Substitute
import Language.Giml.Types.Infer.Types
import Language.Giml.Utils

-- * Solve constraints

-- | Run constraint solving algorithm
solve
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> Int
  -> Constraints
  -> ExceptT TypeErrorA b (Substitution, Int)
solve = runSolve

-- ** Types

-- | Monadic capabilities for the Solve algorithm
type Solve b m =
  ( MonadBase b m
  , MonadError TypeErrorA m
  , MonadState ElabState m -- We highjack the ElabState type for the genTypeVar function.
  , MonadReader (ElabEnv b) m
  )

-- ** Algorithm

runSolve
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> Int
  -> Constraints
  -> ExceptT TypeErrorA b (Substitution, Int)
runSolve logact seed =
  ( fmap (second esTypeVarCounter)
      . flip runStateT (ElabState seed mempty)
      . flip runReaderT (ElabEnv mempty mempty mempty logact)
      . ( \cs -> do
            logDetailed ("Solving constraints: " <> ppShowCAs cs)
            solveConstraints mempty cs
        )
      . S.toList
  )

-- | Recursively the constraints in order, passing the accumulated substitution.
solveConstraints :: (Solve b m) => Substitution -> [ConstraintA] -> m Substitution
solveConstraints sub = \case
  [] -> pure sub
  c : cs -> do
    (newCons, newSub) <- solveConstraint c
    -- apply the new substitution on the rest of the constraints
    cs' <- substituteConstraints newSub (newCons <> cs)
    -- apply the new substitution to the accumulative substitution
    sub' <- substituteSubs newSub sub
    solveConstraints (M.union newSub sub') cs'

-- | Solve a constraint.
--   Returns new constraints that may arise from this constraint and a substitution
solveConstraint :: (Solve b m) => ConstraintA -> m ([ConstraintA], Substitution)
solveConstraint (constraint, ann) = do
  logDetailed ("constraint: " <> ppShowC constraint)
  case constraint of
    -- For let polymorphism. Instantiate a type.
    -- See the comment on @instantiate@ for more information.
    Equality t1 (TypeScheme vars t2) -> do
      t2' <- instantiate "ti" vars t2
      pure ([(Equality t1 t2', ann)], mempty)

    -- When the two types are equals, there's nothing to do.
    Equality t1 t2
      | t1 == t2 ->
          pure (mempty, mempty)
    Equality (TypeApp f1 a1) (TypeApp f2 a2) ->
      pure
        ( map (flip (,) ann) $ [Equality f1 f2, Equality a1 a2]
        , mempty
        )
    -- all record labels and type should match
    Equality t1@(TypeRec (M.fromList -> rec1)) t2@(TypeRec (M.fromList -> rec2)) -> do
      let
        (lefts, rights, matches) =
          unzipMatches $
            M.elems $
              M.merge
                (M.mapMissing $ const . OnlyLeft)
                (M.mapMissing $ const . OnlyRight)
                (M.zipWithMatched $ \_ l r -> BothSides (Equality l r))
                rec1
                rec2
      unless (null lefts && null rights) $
        throwErr [ann] $
          RecordDiff t1 t2 lefts rights
      pure
        ( map (flip (,) ann) matches
        , mempty
        )

    -- we want to make sure rec1 and rec2 do not contain matching labels with non matching types
    -- we want to match the labels from rec1 that do not exist in rec2 to match "ext"
    Equality t1@(TypeRec (M.fromList -> rec1)) t2@(TypeRecExt (M.fromList -> rec2) ext) -> do
      let
        (lefts, rights, matches) =
          unzipMatches $
            M.elems $
              M.merge
                (M.mapMissing $ fmap OnlyLeft . (,))
                (M.mapMissing $ fmap OnlyRight . (,))
                (M.zipWithMatched $ \_ l r -> BothSides (Equality l r))
                rec1
                rec2
      unless (null rights) $
        throwErr [ann] $
          RecordDiff t1 t2 [] (map fst rights)
      pure
        ( map (flip (,) ann) $ Equality (TypeRec lefts) (TypeVar ext) : matches
        , mempty
        )
    Equality t1@TypeRecExt {} t2@TypeRec {} -> do
      solveConstraint (Equality t2 t1, ann)
    Equality (TypeRecExt (M.fromList -> rec1) ext1) (TypeRecExt (M.fromList -> rec2) ext2) -> do
      let
        matches = M.elems $ M.intersectionWith Equality rec1 rec2
        onlyLeft = M.difference rec1 rec2
        onlyRight = M.difference rec2 rec1
      ext' <- genTypeVar "ext"
      pure
        ( map (flip (,) ann) $
            Equality (TypeRecExt (M.toList onlyLeft) ext') (TypeVar ext2)
              : Equality (TypeRecExt (M.toList onlyRight) ext') (TypeVar ext1)
              : matches
        , mempty
        )

    -- Polymorphic Variants --

    -- all variants constructors and types should match
    Equality t1@(TypeVariant (M.fromList -> var1)) t2@(TypeVariant (M.fromList -> var2)) -> do
      let
        (lefts, rights, matches) =
          unzipMatches $
            M.elems $
              M.merge
                (M.mapMissing $ const . OnlyLeft)
                (M.mapMissing $ const . OnlyRight)
                (M.zipWithMatched $ \_ l r -> BothSides (Equality l r))
                var1
                var2
      unless (null lefts && null rights) $
        throwErr [ann] $
          VariantDiff t1 t2 lefts rights
      pure
        ( map (flip (,) ann) matches
        , mempty
        )

    -- all relevant variants should match.
    -- When we have matching constructors from both sides, we match their type.
    Equality (TypePolyVariantLB (M.fromList -> var1) ext1) (TypePolyVariantLB (M.fromList -> var2) ext2) -> do
      let
        (lefts, rights, matches) =
          unzipMatches $
            M.elems $
              M.merge
                (M.mapMissing $ fmap OnlyLeft . (,))
                (M.mapMissing $ fmap OnlyRight . (,))
                (M.zipWithMatched $ \_ l r -> BothSides (Equality l r))
                var1
                var2
      ext' <- genTypeVar "tv"
      pure
        ( map (flip (,) ann) $
            Equality (TypePolyVariantLB (sort $ lefts <> rights) ext') (TypeVar ext1)
              : Equality (TypePolyVariantLB (sort $ lefts <> rights) ext') (TypeVar ext2)
              : matches
        , mempty
        )

    -- to unify two upper bounded polymorphic variants with the same hidden type variant, we:
    -- 1. match the types of the known fields, and
    -- 2. merge all of the fields together into a UB that represents the merge, and
    -- 3. match it with the hidden type variable.
    -- This process keeps track of the variants we ran into for a specific type variable during constraint solving.
    Equality (TypePolyVariantUB tv1 (M.fromList -> var1)) (TypePolyVariantUB tv2 (M.fromList -> var2))
      | tv1 == tv2 -> do
          let
            (lefts, rights, matches) =
              unzipMatches $
                M.elems $
                  M.merge
                    (M.mapMissing $ fmap OnlyLeft . (,))
                    (M.mapMissing $ fmap OnlyRight . (,))
                    (M.zipWithMatched $ \k l r -> BothSides ((k, l), Equality l r))
                    var1
                    var2
          pure
            ( map (flip (,) ann) $
                Equality (TypePolyVariantUB tv1 (sort $ lefts <> rights <> map fst matches)) (TypeVar tv1)
                  : map snd matches
            , mempty
            )
    -- but if the type variables don't match, we treat them as regular variants that should match.
    Equality (TypePolyVariantUB _ vars1) (TypePolyVariantUB _ vars2) ->
      solveConstraint (Equality (TypeVariant vars1) (TypeVariant vars2), ann)
    -- similar to TypeRecordExt, there shouldn't be variants in LB that we don't know of in the regular variant.
    Equality t1@(TypeVariant (M.fromList -> var1)) t2@(TypePolyVariantLB (M.fromList -> var2) ext) -> do
      let
        (lefts, rights, matches) =
          unzipMatches $
            M.elems $
              M.merge
                (M.mapMissing $ fmap OnlyLeft . (,))
                (M.mapMissing $ fmap OnlyRight . (,))
                (M.zipWithMatched $ \_ l r -> BothSides (Equality l r))
                var1
                var2
      unless (null rights) $
        throwErr [ann] $
          VariantDiff t1 t2 [] (map fst rights)
      pure
        ( map (flip (,) ann) $ Equality (TypeVariant lefts) (TypeVar ext) : matches
        , mempty
        )

    -- to unify LB and UB, we treat the UB as a normal variant.

    Equality t1@TypePolyVariantLB {} (TypePolyVariantUB ext vars2) -> do
      pure
        ( (Equality t1 (TypeVariant vars2), ann)
            : (Equality (TypeVar ext) (TypeVariant []), ann)
            : []
        , mempty
        )
    Equality (TypePolyVariantUB ext vars1) t2@TypePolyVariantLB {} -> do
      pure
        ( (Equality t2 (TypeVariant vars1), ann)
            : (Equality (TypeVar ext) (TypeVariant []), ann)
            : []
        , mempty
        )

    -- all other cases match like regular variants

    Equality t1@TypePolyVariantLB {} t2@TypeVariant {} -> do
      solveConstraint (Equality t2 t1, ann)
    Equality (TypePolyVariantUB _ vars) t2@TypeVariant {} -> do
      solveConstraint (Equality (TypeVariant vars) t2, ann)
    Equality t1@TypeVariant {} (TypePolyVariantUB _ vars) -> do
      solveConstraint (Equality t1 (TypeVariant vars), ann)

    --------------------------

    -- Map a type variable to the other type
    Equality (TypeVar tv) t2 ->
      pure
        ( mempty
        , M.singleton tv (ann, t2)
        )
    Equality t1 (TypeVar tv) ->
      solveConstraint (Equality (TypeVar tv) t1, ann)
    -- finally, if non of the above cases matched with this empty open variant,
    -- treat it as a type variable.
    Equality (TypePolyVariantLB [] tv) t2 ->
      solveConstraint (Equality (TypeVar tv) t2, ann)
    Equality t1 (TypePolyVariantLB [] tv) ->
      solveConstraint (Equality (TypeVar tv) t1, ann)
    -- When all else fails, throw an error.
    Equality t1 t2 ->
      throwErr [ann] $ TypeMismatch t1 t2
