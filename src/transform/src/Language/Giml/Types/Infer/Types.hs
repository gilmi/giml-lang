{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

-- | Utility types for type inference
module Language.Giml.Types.Infer.Types where

import Control.Monad.Except
import Data.Set qualified as S
import Data.Text qualified as T
-- for debugging purposes --

import Language.Giml.Pretty (braced', ppSourcePos, ppType, render)
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser qualified as Parser
import Language.Giml.Utils
import Prettyprinter (Doc, Pretty, align, pretty, (<+>))

---------------

-- * Types

-- | The annotation of the input
type InputAnn = Parser.Ann

-- | The annotation of the output: the input + the type
data Ann = Ann
  { annInput :: InputAnn
  , annType :: Type
  }
  deriving (Show, Eq, Ord, Data)

type TypeErrorA = ([InputAnn], TypeError)

-- | The type of type errors.
data TypeError
  = TypeMismatch Type Type
  | UnboundVar Var
  | InfiniteType TypeVar Type
  | ArityMismatch Type Type
  | UnboundTypeVarsInType (Datatype InputAnn)
  | DuplicateTypeVarsInSig (Datatype InputAnn)
  | DuplicateConstrs (Datatype InputAnn)
  | DuplicateConstrs2 [(Constr, (VariantSig InputAnn, VariantSig InputAnn))]
  | NoSuchVariant Constr
  | RecordDiff Type Type [Label] [Label]
  | VariantDiff Type Type [Constr] [Constr]
  | NotARecord Type
  | NotAVariant Type
  | DuplicateVarsInPattern Pattern
  deriving (Show, Eq, Ord)

-- | Represents the constraints on types we collect during the elaboration phase.
data Constraint
  = -- | The two type should be equal.
    Equality Type Type
  deriving (Show, Eq, Ord, Data)

-- | A constraint with the input annotation.
type ConstraintA = (Constraint, InputAnn)

-- | A @Set@ of constraints.
type Constraints = Set ConstraintA

-- | A mapping from type variable to types. Also contains the source position for error reporting.
--   This is the output of the constraint solving phase.
type Substitution =
  Map TypeVar (InputAnn, Type)

-- | Relevant information about a data constructor.
data VariantSig a = VariantSig
  { vsVars :: [TypeVar]
  , vsDatatype :: Type
  , vsPayloadTemplate :: Maybe Type
  , vsAnn :: a
  }
  deriving (Show, Eq, Ord, Data)

vsToTypeScheme :: VariantSig a -> Type
vsToTypeScheme VariantSig {..} =
  let
    addscheme
      | null vsVars = id
      | otherwise = TypeScheme vsVars
  in
    addscheme $
      maybe
        vsDatatype
        (\t -> typeFun [t] vsDatatype)
        vsPayloadTemplate

-- * Utils

-- | Throw an error with annotation.
throwErr :: (MonadError TypeErrorA m) => [InputAnn] -> TypeError -> m a
throwErr ann err = throwError (ann, err)

-- | Retrieve the type of an expression. Will explode when used on a non @EAnnotated@ node.
getType :: Expr Ann -> Type
getType = annType . getExprAnn

-- * Pretty printing

ppShow :: (Pretty ann) => (Functor f) => (Show (f Text)) => f ann -> Text
ppShow = pShow . fmap (render . pretty)

ppShowC :: Constraint -> Text
ppShowC = render . ppConstraint

ppShowCs :: (Show (f Text)) => (Functor f) => f Constraint -> Text
ppShowCs = pShow . fmap (render . ppConstraint)

ppShowCAss :: Set ConstraintA -> Text
ppShowCAss = pShow . S.map (render . ppConstraint . fst)

ppShowCAs :: (Show (f Text)) => (Functor f) => f ConstraintA -> Text
ppShowCAs = pShow . fmap (render . ppConstraint . fst)

printAnn :: Ann -> Text
printAnn = render . pretty

ppAnn :: Ann -> Doc ann
ppAnn (Ann src typ) = braced' [ppSourcePos src, ppType typ]

ppConstraint :: Constraint -> Doc ann
ppConstraint = \case
  Equality t1 t2 -> align $ ppType t1 <+> "~" <+> ppType t2

instance Pretty Parser.SourcePos where
  pretty = ppSourcePos

instance Pretty Ann where
  pretty = ppAnn

instance Pretty Type where
  pretty = ppType

instance Pretty Constraint where
  pretty = ppConstraint

ppTypeError :: TypeErrorA -> Text
ppTypeError (srcs, typeErr) =
  T.unlines
    [ "*** Error from: " <> T.intercalate ", and " (map (render . ppSourcePos) srcs)
    , pShow typeErr
    ]
