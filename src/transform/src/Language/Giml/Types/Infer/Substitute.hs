-- | Apply a substitution to a type
module Language.Giml.Types.Infer.Substitute where

import Control.Monad.Except
import Data.Generics.Uniplate.Data qualified as U
import Data.Map qualified as M
import Data.Set qualified as S
import Data.Text qualified as T
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer.Types
import Language.Giml.Utils

--------------------------

-- * Substitute

-- | Apply a substitution.

-- ** API

-- | Replaces all type variables for any data type that
--   has an instance of Data using uniplate magic.
--
--   Note: uniplate magic = slow.
substitute :: (Substitute m) => (Data f) => Substitution -> f -> m f
substitute sub
  | M.null sub = pure
  | otherwise = U.transformBiM (replaceTypeVar sub)

-- | Replaces all type variables for constraints.
substituteConstraints :: (Substitute m) => Substitution -> [ConstraintA] -> m [ConstraintA]
substituteConstraints sub
  | M.null sub = pure
  | otherwise =
      traverse $ \case
        (c, ann) ->
          let
            onConstraint constr t1 t2 =
              fmap (flip (,) ann) $
                constr
                  <$> overTypes (replaceTypeVar sub) t1
                  <*> overTypes (replaceTypeVar sub) t2
          in
            case c of
              Equality t1 t2 ->
                onConstraint Equality t1 t2

-- | Replaces all type variables for substitutions.
substituteSubs :: (Substitute m) => Substitution -> Substitution -> m Substitution
substituteSubs sub
  | M.null sub = pure
  | otherwise = traverse $ traverse $ overTypes (replaceTypeVar sub)

-- ** Types

-- | Monadic capabilities of Substitute
type Substitute m =
  ( MonadError TypeErrorA m
  )

-- ** Algorithm

-- | Find type variables that appear in the substitution and replace them.
replaceTypeVar :: (Substitute m) => Substitution -> Type -> m Type
replaceTypeVar sub = \case
  TypeVar v ->
    maybe
      (pure $ TypeVar v)
      (uncurry $ occursCheck v) -- Make sure we don't have an infinite type
      (M.lookup v sub)
  TypeRecExt r1 ext
    | Just (ann, r2) <- M.lookup ext sub -> do
        void $ occursCheck ext ann r2
        mergeRecords ann r1 r2

  -- if the hidden type variable name is change we change it here as well
  TypePolyVariantUB tv vars
    | Just (_, TypeVar tv') <- M.lookup tv sub -> do
        pure $ TypePolyVariantUB tv' vars
  TypePolyVariantUB tv vars1
    | Just (ann, vars2) <- M.lookup tv sub -> do
        -- @TODO occursCheck ?
        mergeVariants ann vars1 vars2

  -- this case represents a normal TypeVar
  TypePolyVariantLB [] tv
    | Just (_, t) <- M.lookup tv sub -> do
        pure t
  TypePolyVariantLB vars1 tv
    | Just (ann, vars2) <- M.lookup tv sub -> do
        -- @TODO occursCheck ?
        mergeVariants ann vars1 vars2
  t@TypeCon {} -> pure t
  t@TypeApp {} -> pure t
  t@TypeRec {} -> pure t
  t@TypeRecExt {} -> pure t
  t@TypeVariant {} -> pure t
  t@TypePolyVariantLB {} -> pure t
  t@TypePolyVariantUB {} -> pure t
  t@TypeScheme {} -> pure t

-- Merging two open variants
mergeVariants :: (Substitute m) => InputAnn -> [(Constr, Type)] -> Type -> m Type
mergeVariants ann vars1 = \case
  TypePolyVariantLB vars2 tv ->
    pure . flip TypePolyVariantLB tv . M.toList $ M.fromListWith (flip const) (vars1 <> vars2)
  TypePolyVariantUB tv vars2 ->
    pure . TypePolyVariantUB tv . M.toList $ M.fromListWith (flip const) (vars1 <> vars2)
  TypeVariant vars2 ->
    pure . TypeVariant . M.toList $ M.fromListWith (flip const) (vars1 <> vars2)
  t ->
    throwErr [ann] $ NotAVariant t

mergeRecords :: (Substitute m) => InputAnn -> [(Label, Type)] -> Type -> m Type
mergeRecords ann r1 = \case
  TypeRec r2 ->
    pure . TypeRec . M.toList $ M.fromListWith (flip const) (r1 <> r2)
  TypeVar ext' ->
    pure $ TypeRecExt r1 ext'
  TypeRecExt r2 ext ->
    pure . flip TypeRecExt ext . M.toList $ M.fromListWith (flip const) (r1 <> r2)
  t ->
    throwErr [ann] $ NotARecord t

-- | protect against infinite types
occursCheck :: (Substitute m) => TypeVar -> InputAnn -> Type -> m Type
occursCheck v ann = \case
  TypeVar v'
    | v == v' ->
        pure $ TypeVar v'
  -- types that contain the type variable we are trying to replace
  -- are forbidden
  t -> do
    let
      tvars = [() | TypeVar tv <- U.universe t, tv == v]
    unless (null tvars) $ throwErr [ann] $ InfiniteType v t
    pure t

-- * Generalize

generalizeTermDef :: TermDef Ann -> TermDef Ann
generalizeTermDef termdef =
  let
    t = annType . getTermAnn $ termdef
    (freevars, mapping) = generalizeType t
    closed = closeType freevars mapping t
    replace = U.transformBi $ \tv ->
      fromMaybe tv (M.lookup tv mapping)
  in
    case termdef of
      Variable (Ann x _) name _ expr ->
        Variable (Ann x closed) name (Just closed) (replace expr)
      Function (Ann x _) name _ vars body ->
        Function (Ann x closed) name (Just closed) vars (replace body)

generalizeAndClose :: Type -> Type
generalizeAndClose t = (uncurry closeType $ generalizeType t) t

generalizeType :: Type -> ([TypeVar], M.Map TypeVar TypeVar)
generalizeType t =
  let
    freevars = S.toList $ freeVars t
    mapping =
      M.fromList $
        zip freevars $
          map (TV . T.pack . pure) ['a' .. 'z'] <> [TV $ T.pack ('a' : show i) | i :: Int <- [0 ..]]
  in
    (freevars, mapping)

closeType :: [TypeVar] -> M.Map TypeVar TypeVar -> Type -> Type
closeType freevars mapping t =
  if null freevars
    then t
    else TypeScheme (M.elems mapping) $
      flip U.transformBi t $ \tv ->
        fromMaybe tv (M.lookup tv mapping)

freeVars :: Type -> S.Set TypeVar
freeVars t = S.fromList [tv | tv <- U.universeBi t]
