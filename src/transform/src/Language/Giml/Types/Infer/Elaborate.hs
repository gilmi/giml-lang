{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Elaborate types
--
-- During the elaboration phase we annotate the AST with the types
-- we know. When we don't know what the type of something is, we generate
-- a new type variable as a placeholder and mark that type with a @Constraint@
-- depending on its syntactic use.
--
-- - Input: Builtin data types, builtin type environment, and the AST
-- - Output: Annotated AST with types (to the best of our abilities) + constraints on those types
module Language.Giml.Types.Infer.Elaborate where

import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Data.Generics.Uniplate.Data qualified as U
import Data.Map qualified as M
import Data.Set qualified as S
import Language.Giml.Builtins
import Language.Giml.Logging
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser qualified as Parser
import Language.Giml.Types.Infer.Types
import Language.Giml.Utils

-- * Elaborate data types

-- ** Types

-- | The monadic capabilities for the elaboration phase.
type ElaborateData b m =
  ( MonadBase b m
  , MonadState VariantEnv m
  , MonadError TypeErrorA m
  )

-- | The state we keep during elaboration.
data ElabState = ElabState
  { esTypeVarCounter :: Int
  -- ^ Used to generate unique type variable names.
  , esConstraints :: Constraints
  -- ^ The constraints we collect.
  }

-- | The environment we use.
data ElabEnv b = ElabEnv
  { eeTypeEnv :: Env Type
  -- ^ Represents the variables in scope and their types.
  , eeBuiltins :: Env Type
  , eeVariantEnv :: VariantEnv
  -- ^ Mapping from a data constructor name to the data type
  --   and variant type signature.
  , logAction :: LogAction b LogMsg
  }
  deriving (Generic)

-- ** Algorithm

elaborateEnv
  :: (MonadBase b b)
  => [Datatype ()]
  -> [Datatype InputAnn]
  -> ExceptT TypeErrorA b ([Datatype Ann], VariantEnv)
elaborateEnv builtinDats typeDefs =
  flip runStateT mempty $ do
    _ <-
      traverse elaborateTypeDef $
        map (setTypeAnn $ Parser.dummyAnn "builtin") builtinDats
    -- We invent types for top level term definitions.
    typeDefs' <- traverse elaborateTypeDef typeDefs
    pure typeDefs'

-- | Add a datatype information into the environment
addVariantSigs :: (ElaborateData b m) => VariantEnv -> m ()
addVariantSigs venv = do
  env <- get
  let
    duplicates = M.toList $ M.intersectionWith (,) venv env

  -- check for duplicate variants
  case duplicates of
    [] -> pure ()
    (d, v) : rest -> do
      throwErr [vsAnn $ fst v] $ DuplicateConstrs2 ((d, v) : rest)
  put $ env `M.union` venv

-- | Add a data type to the VariantEnv and elaborate it with a dummy type.
elaborateTypeDef :: (ElaborateData b m) => Datatype InputAnn -> m (Datatype Ann)
elaborateTypeDef = \case
  dt@(Datatype ann typename kind args variants) -> do
    let
      datatype = typeApp (TypeCon typename) (map TypeVar args)
      boundvars = S.fromList args
      variantsvars = [t | TypeVar t <- U.universeBi variants]
      constrs = S.fromList $ map (\(Variant constr _) -> constr) variants

    -- check for duplicate data constructors, duplicate type variables
    -- and unbound type vars
    unless (length boundvars == length args) $
      throwErr [ann] $
        DuplicateTypeVarsInSig dt

    unless (length variants == length constrs) $
      throwErr [ann] $
        DuplicateConstrs dt

    unless (all (`S.member` boundvars) variantsvars) $
      throwErr [ann] $
        UnboundTypeVarsInType dt

    -- convert to @VariantSig@s
    let
      variantsigs =
        M.fromList $
          map
            ( \(Variant constr template) ->
                ( constr
                , VariantSig
                    { vsVars = args
                    , vsDatatype = datatype
                    , vsPayloadTemplate = template
                    , vsAnn = ann
                    }
                )
            )
            variants
    addVariantSigs variantsigs
    pure (Datatype (Ann ann tUnit) typename kind args variants)

-----------------------------------------------

-- * Elaborate

-- | Run elaboration algorithm
elaborate :: (MonadBase b b) => LogAction b LogMsg -> VariantEnv -> Env Type -> [TermDef InputAnn] -> ExceptT TypeErrorA b ([TermDef Ann], ElabState)
elaborate = runElaborate

------------

-- ** Types

-- | The monadic capabilities for the elaboration phase.
type Elaborate b m =
  ( MonadBase b m
  , MonadState ElabState m
  , MonadReader (ElabEnv b) m
  , MonadError TypeErrorA m
  )

-- | An environment from a name to something.
type Env a = Map Var a

-- | A mapping from a data constructor to the type that defines it and the type it holds.
type VariantEnv =
  Map Constr (VariantSig InputAnn)

-- | Return type for some elaboration functions.
data ElabInfo a = ElabInfo
  { eiResult :: a
  -- ^ The result of the current elaboration
  , eiType :: Type
  -- ^ The type of the result
  , eiNewEnv :: Env Type
  -- ^ A definition to add to the environment for the next statements, if needed.
  }

-------------

-- ** Utils

-- | Try to find the type of a variable in scope.
lookupVarMaybe :: (Elaborate b m) => Var -> m (Maybe Type)
lookupVarMaybe var = do
  env <- asks eeTypeEnv
  pure (M.lookup var env)

-- | Same as @lookupVarMaybe@ but throws an @UnboundVar@ error on failure.
lookupVar :: (Elaborate b m) => InputAnn -> Var -> m Type
lookupVar ann var = do
  env <- asks eeTypeEnv
  maybe
    (throwErr [ann] $ UnboundVar var)
    pure
    (M.lookup var env)

-- | Look for a builtin value/function. Throws an @UnboundVar@ error on failure.
lookupBuiltin :: (Elaborate b m) => InputAnn -> Var -> m Type
lookupBuiltin ann var = do
  env <- asks eeBuiltins
  maybe
    (throwErr [ann] $ UnboundVar var)
    pure
    (M.lookup var env)

-- | Adding new variable into the scope
insertToEnv :: Env Type -> ElabEnv b -> ElabEnv b
insertToEnv vars elabEnv =
  elabEnv
    { eeTypeEnv = M.union vars (eeTypeEnv elabEnv)
    }

-- | Remove variables from the scope
removeFromEnv :: [Var] -> ElabEnv b -> ElabEnv b
removeFromEnv vars elabEnv =
  elabEnv
    { eeTypeEnv = foldr M.delete (eeTypeEnv elabEnv) vars
    }

-- | Run an elaboration function with extra variables in scope.
withEnv :: (Elaborate b m) => [(Var, Type)] -> m a -> m a
withEnv = local . insertToEnv . M.fromList

-- | Run an elaboration function with extra variables in scope (Map version).
withEnv' :: (Elaborate b m) => Env Type -> m a -> m a
withEnv' = local . insertToEnv

-- | Run an elaboration function without some variables in scope.
withoutEnv :: (Elaborate b m) => [Var] -> m a -> m a
withoutEnv = local . removeFromEnv

-- | Lookup variant in env
lookupVariant :: (Elaborate b m) => InputAnn -> Constr -> m (VariantSig InputAnn)
lookupVariant ann constr = do
  maybe (throwErr [ann] $ NoSuchVariant constr) pure . M.lookup constr . eeVariantEnv =<< ask

-- | Generate a new type variable.
genTypeVar :: (MonadState ElabState m) => Text -> m TypeVar
genTypeVar prefix = do
  n <- esTypeVarCounter <$> get
  modify $ \s -> s {esTypeVarCounter = n + 1}
  pure $ TV $ prefix <> toText (show n)

-- | Add a new constraint.
constrain :: (Elaborate b m) => InputAnn -> Constraint -> m ()
constrain ann constraint =
  modify $ \s ->
    s
      { esConstraints =
          S.insert (constraint, ann) (esConstraints s)
      }

------------------

-- ** Algorithm

-- | Run the algorithm.
runElaborate
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> VariantEnv
  -> Env Type
  -> [TermDef InputAnn]
  -> ExceptT TypeErrorA b ([TermDef Ann], ElabState)
runElaborate logact variantEnv builtinsTypes =
  ( flip runStateT (ElabState 0 mempty)
      . flip runReaderT (ElabEnv mempty builtinsTypes variantEnv logact)
      . setStage TypeInference
      . elaborateFile
  )

-- | Elaborate a source file
elaborateFile :: (Elaborate b m) => [TermDef InputAnn] -> m [TermDef Ann]
elaborateFile termDefs = do
  let
    names = map getTermName termDefs
  vars <- traverse (\name -> (,) name . TypeVar <$> genTypeVar "top") names
  withEnv vars $ traverse elaborateDef termDefs

-- | Elaborate a @TermDef@
elaborateDef :: (Elaborate b m) => TermDef InputAnn -> m (TermDef Ann)
elaborateDef def = do
  -- we need to change the type of def in the current environment for recursive functions
  let
    name = getTermName def
    ann = getTermAnn def
  t <- lookupVar ann name
  -- When evaluating a definition, the type should not be let polymorphic.
  elab <- withEnv [(name, t)] $ elaborateTermDef def
  constrain ann $ Equality t (eiType elab)
  pure $ setTermAnn (Ann ann $ eiType elab) $ eiResult elab

-- | Elaborate a term definition helper function
elaborateTermDef :: (Elaborate b m) => TermDef InputAnn -> m (ElabInfo (TermDef Ann))
elaborateTermDef = \case
  Variable ann name mtype expr -> do
    expr' <- elaborateExpr ann expr
    when (name == "main") $
      constrain ann $
        Equality (getType expr') (tIO tUnit)
    maybe (pure ()) (constrain ann . Equality (getType expr')) mtype
    pure $
      ElabInfo
        { eiType = getType expr'
        , eiResult = Variable (Ann ann $ getType expr') name Nothing expr'
        , eiNewEnv = mempty
        }
  Function ann name mtype args body -> do
    tfun <- genTypeVar "tfun"
    targsEnv <- traverse (\arg -> (,) arg <$> genTypeVar "t") args
    body' <-
      withEnv
        ( (name, TypeVar tfun)
            : mapMaybe
              (\(var, t) -> flip (,) (TypeVar t) <$> var)
              targsEnv
        )
        (elaborateExpr ann body)

    let
      tret = getType body'
      tfunexpanded = typeFun (map (TypeVar . snd) targsEnv) tret

    constrain ann $
      Equality
        (TypeVar tfun)
        tfunexpanded

    when (name == "main") $
      constrain ann $
        Equality (TypeVar tfun) (tIO tUnit)

    maybe (pure ()) (constrain ann . Equality (TypeVar tfun)) mtype

    pure $
      ElabInfo
        { eiType = tfunexpanded
        , eiResult = Function (Ann ann tfunexpanded) name Nothing args body'
        , eiNewEnv = mempty
        }

-- | Elaborate a list of statements.
--   Returns the type of the final statement as well.
elaborateBlock :: (Elaborate b m) => Block InputAnn -> m (Type, Block Ann)
elaborateBlock block = do
  -- we want to fold over the list of statements, and for each new definition,
  -- add it to the environment for the elaboration of the next expressions
  block' <-
    foldM
      ( \blockinfo stmt -> do
          stmt' <- withEnv' (eiNewEnv blockinfo) $ elaborateStmt stmt
          pure $
            ElabInfo
              { eiResult = eiResult stmt' : eiResult blockinfo
              , eiType = eiType stmt'
              , eiNewEnv = M.union (eiNewEnv stmt') (eiNewEnv blockinfo)
              }
      )
      (ElabInfo [] (tIO tUnit) mempty)
      block
  pure (eiType block', reverse (eiResult block'))

-- | Elaborate a single statement.
elaborateStmt :: (Elaborate b m) => Statement InputAnn -> m (ElabInfo (Statement Ann))
elaborateStmt = \case
  SExpr ann expr -> do
    expr' <- elaborateExpr ann expr
    t <- TypeVar <$> genTypeVar "t"
    constrain ann $ Equality (getType expr') (tIO t)
    pure $
      ElabInfo
        { eiResult = SExpr (Ann ann (getType expr')) expr'
        , eiType = getType expr'
        , eiNewEnv = mempty
        }
  SDef ann def -> do
    ElabInfo def' t _ <- elaborateTermDef def
    pure $
      ElabInfo
        { eiResult = SDef (Ann ann t) def'
        , eiType = t
        , eiNewEnv = M.singleton (getTermName def') t
        }
  SBind ann name expr -> do
    expr' <- elaborateExpr ann expr
    t <- TypeVar <$> genTypeVar "t"
    let
      t' = getType expr'
    constrain ann $ Equality t' (tIO t)
    pure $
      ElabInfo
        { eiResult = SBind (Ann ann t) name expr'
        , eiType = t'
        , eiNewEnv = M.singleton name t
        }

-- | Elaborate an expression.
--   Traverses the expression Top-Down.
elaborateExpr :: (Elaborate b m) => InputAnn -> Expr InputAnn -> m (Expr Ann)
elaborateExpr ann = \case
  -- Replace the current annotation an evaluate the inner expression
  EAnnotated ann' e ->
    elaborateExpr ann' e
  -- The types of literals are known.
  ELit lit ->
    pure $ EAnnotated (Ann ann $ getLitType lit) (ELit lit)
  -- For variables, we look it up in the environment
  EVar var -> do
    t <- maybe (lookupBuiltin ann var) pure =<< lookupVarMaybe var
    typ <- do
      tv <- genTypeVar "t"
      constrain ann $ Equality (TypeVar tv) t
      pure $ TypeVar tv
    pure $
      EAnnotated (Ann ann typ) $
        EVar var

  -- For operators, we look it up in the environment
  EOp opDef@OpDef {opFunRef} -> do
    t <- maybe (lookupBuiltin ann opFunRef) pure =<< lookupVarMaybe opFunRef
    typ <- do
      tv <- genTypeVar "t"
      constrain ann $ Equality (TypeVar tv) t
      pure $ TypeVar tv
    pure $
      EAnnotated (Ann ann typ) $
        EOp opDef

  -- Generate type variables for function arguments, and annotate
  -- the body with the arguments in the environment.
  -- The result type should be a function from the arguments types to the type of the body
  EFun args body -> do
    targsEnv <- traverse (\arg -> (,) arg <$> genTypeVar "t") args
    body' <-
      withEnv
        ( mapMaybe
            (\(var, t) -> flip (,) (TypeVar t) <$> var)
            targsEnv
        )
        (elaborateExpr ann body)

    let
      tret = getType body'

    let
      tfun = typeFun (map (TypeVar . snd) targsEnv) tret

    pure $
      EAnnotated (Ann ann tfun) $
        EFun args body'

  -- Generate a return type and constrain the type of the function as
  -- a function from the arguments types to the generated return type
  EFunCall f args -> do
    f' <- elaborateExpr ann f
    args' <- traverse (elaborateExpr ann) args
    tret <- TypeVar <$> genTypeVar "t"
    constrain ann $
      Equality
        (getType f')
        (typeFun (map getType args') tret)
    pure $
      EAnnotated (Ann ann tret) $
        EFunCall f' args'

  -- If we have a type for an ffi call supplied by the user, we'll use it for type inference.
  -- otherwise we'll take any amount and any type of arguments and return IO {}.
  EFfi f mtype args -> do
    args' <- traverse (elaborateExpr ann) args
    case mtype of
      Nothing -> do
        pure $
          EAnnotated (Ann ann (tIO tUnit)) $
            EFfi f Nothing args'
      Just t -> do
        let
          (targs, tret) = toTypeFun t

        unless (length args' == length targs) $ do
          tmp <- TypeVar <$> genTypeVar "t"
          throwErr [ann] $ ArityMismatch t (typeFun (map getType args') tmp)

        zipWithM_ (fmap (constrain ann) . Equality) targs (map getType args')

        pure $
          EAnnotated (Ann ann tret) $
            EFfi f (Just t) args'
  EBlock block -> do
    (t, block') <- elaborateBlock block
    pure $
      EAnnotated (Ann ann t) $
        EBlock block'
  ELet termdef expr -> do
    ElabInfo {eiResult, eiType} <- elaborateTermDef termdef
    expr' <- withEnv [(getTermName termdef, eiType)] $ elaborateExpr ann expr
    pure $
      EAnnotated (Ann ann (getType expr')) $
        ELet eiResult expr'

  -- lookup variant in the environment
  EVariant constr -> do
    vsType <- vsToTypeScheme <$> lookupVariant ann constr
    t <- TypeVar <$> genTypeVar "t"
    constrain ann $ Equality t vsType

    pure $
      EAnnotated (Ann ann t) $
        EVariant constr

  -- An open variant has a specific type according to the constructor and value,
  -- but may be plugged in any place that can handle it.
  -- Must take exactly one value as input.
  EOpenVariant constr -> do
    tv <- genTypeVar "t"
    t <- TypeVar <$> genTypeVar "t"
    pure $
      EAnnotated (Ann ann (typeFun [t] $ TypePolyVariantLB [(constr, t)] tv)) $
        EOpenVariant constr

  -- For if expressions, the condition should be of type Bool,
  -- and the two branches should match.
  EIf cond trueBranch falseBranch -> do
    cond' <- elaborateExpr ann cond
    trueBranch' <- elaborateExpr ann trueBranch
    falseBranch' <- elaborateExpr ann falseBranch
    constrain ann $ Equality tBool (getType cond')
    constrain ann $ Equality (getType trueBranch') (getType falseBranch')
    pure $
      EAnnotated (Ann ann (getType trueBranch')) $
        EIf cond' trueBranch' falseBranch'
  ECase expr patterns -> do
    expr' <- elaborateExpr ann expr
    patT <- TypeVar <$> genTypeVar "t"
    patterns' <- elaboratePatterns ann (getType expr') patT patterns
    pure $
      EAnnotated (Ann ann patT) $
        ECase expr' patterns'
  ERecord record -> do
    record' <- traverse (elaborateExpr ann) record
    let
      recordT = TypeRec $ M.toList $ fmap getType record'

    pure $
      EAnnotated (Ann ann recordT) $
        ERecord record'

  -- the "expr" should be a record with at least the "label" label
  ERecordAccess expr label -> do
    labelT <- TypeVar <$> genTypeVar "t"
    ext <- genTypeVar "t"
    expr' <- elaborateExpr ann expr
    constrain ann $
      Equality
        (getType expr')
        (TypeRecExt [(label, labelT)] ext)
    pure $
      EAnnotated (Ann ann labelT) $
        ERecordAccess expr' label
  ERecordExtension record expr -> do
    expr' <- elaborateExpr ann expr
    record' <- traverse (elaborateExpr ann) record
    et <- genTypeVar "t"
    constrain ann $ Equality (TypeRecExt [] et) (getType expr')
    let
      recordTypes = M.toList $ fmap getType record'
      t = TypeRecExt recordTypes et
    pure $
      EAnnotated (Ann ann t) $
        ERecordExtension record' expr'

-- | Elaborate patterns in case expressions
elaboratePatterns
  :: (Elaborate b m)
  => InputAnn
  -> Type
  -> Type
  -> [(Pattern, Expr InputAnn)]
  -> m [(Pattern, Expr Ann)]
elaboratePatterns ann exprT bodyT pats = do
  exprTV <- genTypeVar "t"
  constrain ann $ Equality exprT (TypeVar exprTV)
  for pats $ \(pat, body) -> do
    env <- elaboratePattern ann exprTV pat
    body' <- withEnv env $ elaborateExpr ann body
    let
      t = getType body'
    constrain ann $ Equality bodyT t
    pure (pat, body')

-- | Elaborate a single pattern match
elaboratePattern
  :: (Elaborate b m)
  => InputAnn
  -> TypeVar
  -> Pattern
  -> m [(Var, Type)]
elaboratePattern ann exprTV outerPat = do
  case outerPat of
    PWildcard -> do
      tv <- genTypeVar "t"
      constrain ann $
        Equality
          (TypeVar exprTV)
          (TypePolyVariantLB [] tv)
      pure mempty

    -- variable capture does different things depending if it matches with variants
    -- or other types.
    -- For variants, it matches any variant, but for the specified variants it must
    -- match their type.
    -- Otherwise it will match the expression type.
    PVar v -> do
      tv <- genTypeVar "t"
      constrain ann $
        Equality
          (TypeVar exprTV)
          (TypePolyVariantLB [] tv)
      pure [(v, TypeVar exprTV)]
    PLit lit -> do
      constrain ann $ Equality (getLitType lit) (TypeVar exprTV)
      pure mempty

    -- open variants constrain the type of expr to be able to handle their variant
    POpenVariant (Variant constr innerPat) -> do
      innerPatT <- genTypeVar "t"
      constrain ann $
        Equality
          (TypeVar exprTV)
          (TypePolyVariantUB exprTV [(constr, TypeVar innerPatT)])
      elaboratePattern ann innerPatT innerPat
    PVariant (Variant constr minnerPat) -> do
      vs <- lookupVariant ann constr
      case minnerPat of
        Nothing -> do
          constrain ann $
            Equality (TypeVar exprTV) (vsToTypeScheme vs)
          pure mempty
        Just innerPat -> do
          tv <- genTypeVar "t"
          constrain ann $
            Equality
              (typeFun [TypeVar tv] (TypeVar exprTV))
              (vsToTypeScheme vs)
          elaboratePattern ann tv innerPat
    PRecord record -> do
      record' <- for record $ \pat -> do
        t <- genTypeVar "t"
        (,) (TypeVar t) <$> elaboratePattern ann t pat

      ext <- genTypeVar "t"
      constrain ann $
        Equality
          (TypeVar exprTV)
          (TypeRecExt (M.toList $ fmap fst record') ext)

      -- check if we have duplicate names in the environment
      let
        envs = map (M.fromList . snd) $ M.elems record'
        env = M.unions $ map (M.fromList . snd) $ M.elems record'

      unless (sum (map length envs) == length env) $ do
        throwErr [ann] $ DuplicateVarsInPattern outerPat
      pure $ M.toList env

-- | The type of a literal
getLitType :: Lit -> Type
getLitType = \case
  LBool {} -> tBool
  LInt {} -> tInt
  LString {} -> tString
  LFloat {} -> tFloat

type Solve b m =
  ( MonadBase b m
  , MonadError TypeErrorA m
  , MonadState ElabState m -- We highjack the ElabState type for the genTypeVar function.
  , MonadReader (ElabEnv b) m
  )

-- | Create an instance of a type. (The type serves as a template for specialized types)
--
-- Let polymorphism gives us the ability to use a generic function in more contexts.
--
-- For example, @id@ is a function that can work for @x@ of any type. But our algorithm
-- collects constraints globally, including that:
--
-- @
-- id = \x -> x
--
-- one = id 1          -- constrain that the type of id __is equal to__ the type [Int] -> tN
--
-- hello = id "hello"  -- constrain that the type of id __is equal to__ the type [String] -> tM
-- @
--
-- We need to invent a new constraint that will define the relationship between the type of id
-- and the arguments passing to it as an __Instance of__ relationship.
--
-- @InstanceOf t1 t2@ relationship means that @t1@ is an @instantiation@ of @t2@
-- (or a special case if you will).
-- What we'll do is copy the type of @t2@, generate new type variables in place of all type variables
-- inside of it, and then say that this new type @t3@ has an equality relationship with @t1@.
--
-- It's important to solve the equality constraints for each function before solving the InstanceOf
-- constraints, so that when we instantiate we already have the final type of the function.
--
-- We will highjack the @Ord@ instance deriving (constructors defined later are bigger)
-- and the fact that @Set@ is ordered to accomplish that.
instantiate :: (Solve b m) => Text -> [TypeVar] -> Type -> m Type
instantiate prefix vars typ = do
  env <-
    M.fromList <$> traverse (\v -> (,) v <$> genTypeVar prefix) vars
  pure $ flip U.transform typ $ \case
    TypeVar tv
      | Just tv' <- M.lookup tv env ->
          TypeVar tv'
    TypeRecExt r tv
      | Just tv' <- M.lookup tv env ->
          TypeRecExt r tv'
    TypePolyVariantLB variants tv
      | Just tv' <- M.lookup tv env ->
          TypePolyVariantLB variants tv'
    TypePolyVariantUB tv variants
      | Just tv' <- M.lookup tv env ->
          TypePolyVariantUB tv' variants
    t@TypeVar {} -> t
    t@TypeCon {} -> t
    t@TypeApp {} -> t
    t@TypeRec {} -> t
    t@TypeRecExt {} -> t
    t@TypeVariant {} -> t
    t@TypePolyVariantLB {} -> t
    t@TypePolyVariantUB {} -> t
    t@TypeScheme {} -> t
