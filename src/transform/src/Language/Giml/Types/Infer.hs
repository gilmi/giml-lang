{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Type inference
--
-- Our type inference algorithm works in stages:
--
-- First, we __elaborate__ the AST and collect __constraints__.
-- Then, we solve these constraints and get back a __substitution__.
-- We go over the AST again, replacing the type variables we added in the elaboration stage
-- with concrete types.
--
-- The constraints of each group of definitions are solved separately and in order.
module Language.Giml.Types.Infer (
  module Language.Giml.Types.Infer.Types,
  ElabState (..),
  generalizeAndClose,
  infer,
) where

import Control.Monad.Except
import Data.Map qualified as M
import Data.Set qualified as S
import Language.Giml.Builtins
import Language.Giml.Logging
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer.Elaborate
import Language.Giml.Types.Infer.Solve
import Language.Giml.Types.Infer.Substitute
import Language.Giml.Types.Infer.Types
import Language.Giml.Utils

---------------

-- * Run

-- | Infer the types for all expressions in a source file
infer :: (MonadBase b b) => LogAction b LogMsg -> File InputAnn -> ExceptT TypeErrorA b (File Ann)
infer logact (File datatypes termDefGroups) = do
  (elaboratedDatatypes, variantEnv) <- elaborateEnv builtinDatatypes datatypes
  (elaboratedTermDefGroups, _) <-
    foldM
      ( \(termdefgroups, tyEnv) termdefs -> do
          termdefGroup <- inferTermDefGroup logact variantEnv tyEnv termdefs
          pure (termdefGroup : termdefgroups, toTypeEnv termdefGroup `M.union` tyEnv)
      )
      ([], fmap bType builtins)
      termDefGroups
  pure $ File elaboratedDatatypes (reverse elaboratedTermDefGroups)

-- | Infer the types for all expressions in a source file
inferTermDefGroup
  :: (MonadBase b b)
  => LogAction b LogMsg
  -> VariantEnv
  -> Env Type
  -> [TermDef InputAnn]
  -> ExceptT TypeErrorA b [TermDef Ann]
inferTermDefGroup logact variantEnv typeEnv termdefGroup = do
  withLogAction logact $
    setStage TypeInference $
      logGeneral "Type inference stage"

  (elaborated, s) <- elaborate logact variantEnv typeEnv termdefGroup

  withLogAction logact $
    setStage TypeInference $
      logDetailed ("elaborated: " <> pShow elaborated)
  withLogAction logact $
    setStage TypeInference $
      logDetailed
        ( "constraints for termdef group "
            <> pShow (map getTermName termdefGroup)
            <> ": \n"
            <> ppShowCAss (esConstraints s)
        )

  let
    seed = esTypeVarCounter s
  sub <-
    ( ( \(sub, _) -> do
          withLogAction logact $
            setStage TypeInference $
              logDetailed ("solved: " <> ppShow sub)
          pure sub
      )
        <=< solve logact seed
          . S.fromList
        <=< substituteConstraints mempty . S.toList
      )
      (esConstraints s)

  subtituted <- substitute sub elaborated

  withLogAction logact $
    setStage TypeInference $
      logDetailed ("substituted: " <> pShow subtituted)

  let
    generalized = map generalizeTermDef subtituted

  withLogAction logact $
    setStage TypeInference $
      logDetailed ("generalized: " <> pShow generalized)

  pure generalized

toTypeEnv :: [TermDef Ann] -> Env Type
toTypeEnv =
  M.fromList . map \case
    Variable (Ann _ t) name _ _ ->
      (name, t)
    Function (Ann _ t) name _ _ _ ->
      (name, t)
