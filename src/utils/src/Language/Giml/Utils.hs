{-# LANGUAGE OverloadedStrings #-}

module Language.Giml.Utils (
  module Language.Giml.Utils,
  module Export,
) where

import Control.DeepSeq as Export
import Control.Monad as Export
import Control.Monad.Reader as Export
import Data.Bifunctor as Export
import Data.Data as Export (Data)
import Data.Foldable as Export
import Data.Functor as Export
import Data.Int as Export
import Data.Map as Export (Map)
import Data.Maybe as Export
import Data.Set as Export (Set)
import Data.String as Export (fromString)
import Data.Text as Export (Text)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Traversable as Export
import Data.Typeable as Export
import Data.Void as Export
import Data.Word as Export
import Debug.Trace
import GHC.Generics as Export (Generic)
import Text.Pretty.Simple qualified as PS

ltrace :: (Show a) => String -> a -> a
ltrace lbl x = trace (lbl <> ": " <> toString (pShow x)) x
{-# WARNING ltrace "ltrace left in code" #-}

ltraceM :: (Applicative m) => (Show a) => String -> a -> m ()
ltraceM lbl x = traceM (lbl <> ": " <> toString (pShow x))
{-# WARNING ltraceM "ltraceM left in code" #-}

pShow :: (Show a) => a -> Text
pShow = TL.toStrict . PS.pShow

toString :: Text -> String
toString = T.unpack

toText :: String -> Text
toText = T.pack

data Matches a b
  = OnlyLeft a
  | OnlyRight a
  | BothSides b

unzipMatches :: [Matches a b] -> ([a], [a], [b])
unzipMatches = \case
  [] -> ([], [], [])
  current : rest ->
    let
      (lefts, rights, both) = unzipMatches rest
    in
      case current of
        OnlyLeft x ->
          (x : lefts, rights, both)
        OnlyRight x ->
          (lefts, x : rights, both)
        BothSides x ->
          (lefts, rights, x : both)
