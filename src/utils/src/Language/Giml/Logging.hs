{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

-- | Logging infra for the compiler
module Language.Giml.Logging (
  module Language.Giml.Logging,
  Colog.LogAction,
  MonadBase,
  liftBase,
) where

import Colog.Core qualified as Colog
import Control.Monad.Base
import Control.Monad.Except
import Control.Monad.Identity
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Language.Giml.Utils
import Records.Generic.HasField
import System.IO

-- * Logging API

runWithoutLogger :: ExceptT e (ReaderT (CompileInfo Identity) Identity) a -> Either e a
runWithoutLogger =
  runIdentity . flip runReaderT mkCompileInfoPure . runExceptT

runWithoutLoggerEnv :: ExceptT e Identity a -> Either e a
runWithoutLoggerEnv =
  runIdentity . runExceptT

runIOLogger :: Verbosity -> FilePath -> ExceptT e (ReaderT (CompileInfo IO) IO) a -> IO (Either e a)
runIOLogger v path = flip runReaderT (mkCompileInfoIO v path) . runExceptT

runIOLoggerWithoutLogging :: ExceptT e (ReaderT (CompileInfo IO) IO) a -> IO (Either e a)
runIOLoggerWithoutLogging = flip runReaderT mkCompileInfoIONoLogging . runExceptT

runIOLoggerWithoutEnv :: ExceptT e IO a -> IO (Either e a)
runIOLoggerWithoutEnv = runExceptT

runLog :: Colog.LogAction m1 LogMsg -> ReaderT (CompileInfo m1) m2 a -> m2 a
runLog = flip runReaderT . CompileInfo

withLogAction :: (MonadBase b m) => Colog.LogAction b LogMsg -> ReaderT (CompileInfo b) m a -> m a
withLogAction logact m = runReaderT m (CompileInfo logact)

warn :: (HasLog' LogMsg env b m) => Text -> m ()
warn msg =
  logMsg $
    emptyLogMsg
      { _lmVerbosity = Warning
      , _lmMessage = msg
      }

logGeneral :: (HasLog' LogMsg env b m) => Text -> m ()
logGeneral msg =
  logMsg $
    emptyLogMsg
      { _lmVerbosity = General
      , _lmMessage = msg
      }

logConcise :: (HasLog' LogMsg env b m) => Text -> m ()
logConcise msg =
  logMsg $
    emptyLogMsg
      { _lmVerbosity = Concise
      , _lmMessage = msg
      }

logDetailed :: (HasLog' LogMsg env b m) => Text -> m ()
logDetailed msg =
  logMsg $
    emptyLogMsg
      { _lmVerbosity = Detailed
      , _lmMessage = msg
      }

setStage :: (HasLog' LogMsg env b m) => Stage -> m a -> m a
setStage stage = overLogAction (\msg -> msg {_lmStage = stage})

setRewrite :: (HasLog' LogMsg env b m) => Text -> m a -> m a
setRewrite rewrite = overLogAction (\msg -> msg {_lmRewrite = rewrite})

-------

logMsg :: (HasLog' msg env b m) => msg -> m ()
logMsg msg = do
  act <- getLogAction
  liftBase $ Colog.unLogAction act msg

overLogAction :: (HasLog' msg env b m) => (msg -> msg) -> m a -> m a
overLogAction f m = do
  logact <- getLogAction
  let
    logact' = Colog.cmap f logact
  setLogAction logact' m

-- ** LogActions

mkCompileInfoPure :: CompileInfo Identity
mkCompileInfoPure =
  CompileInfo noLogging

mkCompileInfoIONoLogging :: CompileInfo IO
mkCompileInfoIONoLogging =
  CompileInfo noLoggingIO

mkCompileInfoIO :: Verbosity -> FilePath -> CompileInfo IO
mkCompileInfoIO v path =
  CompileInfo $ Colog.cmap (\msg -> msg {_lmFile = path}) (logStdErr v)

logStdErr :: Verbosity -> Colog.LogAction IO LogMsg
logStdErr v = Colog.LogAction $ \LogMsg {..} ->
  when (v >= _lmVerbosity) $
    T.hPutStrLn stderr $
      T.unwords
        [ "[ " <> pShow _lmVerbosity <> " ]"
        , pShow _lmFile
        , pShow _lmStage <> (if T.null _lmRewrite then "" else " _ " <> _lmRewrite)
        , ": " <> _lmMessage
        ]

noLoggingIO :: Colog.LogAction IO LogMsg
noLoggingIO = Colog.LogAction $ \_ ->
  pure ()

noLogging :: Colog.LogAction Identity LogMsg
noLogging = Colog.LogAction $ \_ ->
  pure ()

-- ** Types

-- | The type of a message we wish to log.
--
-- - Stage (pre infer, post infer, etc.)
-- - Specific rewrite/process (parsing, grouping definitions, elaboration, constraint solving)
-- - Verbosity level (only constraints generated, each constraint, etc.)
-- - The message in the log
data LogMsg = LogMsg
  { _lmFile :: FilePath
  , _lmStage :: Stage
  , _lmRewrite :: Text
  , _lmVerbosity :: Verbosity
  , _lmMessage :: Text
  }
  deriving (Show)

-- | Which stage did the message come from
data Stage
  = Parsing
  | PreInfer
  | TypeInference
  | PostInfer
  | Compilation Text
  deriving (Show)

-- | Verbose level from least detailed to most detailed
data Verbosity
  = Warning
  | General
  | Concise
  | Detailed
  deriving (Show, Read, Eq, Ord)

emptyLogMsg :: LogMsg
emptyLogMsg =
  LogMsg
    { _lmFile = ""
    , _lmStage = Parsing
    , _lmRewrite = ""
    , _lmVerbosity = General
    , _lmMessage = "Empty message"
    }

type CompilePhase e env b m =
  ( MonadError e m
  , HasLog' LogMsg env b m
  )

type Compile e m =
  ExceptT e (ReaderT (CompileInfo m) m)

data CompileInfo m = CompileInfo
  { logAction :: Colog.LogAction m LogMsg
  }
  deriving (Generic)

-- We want to be able to:
-- - Log anywhere in the compiler, should work with different monad stacks
-- for that we need to be able to lift the base monad which holds the logging capabilities
-- into the current monad

-- What I'm trying to say here is:
instance-- if @b@ is the base monad of @m@, and

  ( MonadBase b m
  , -- @m@ is has some Reader monad capabilities with @env@ as the environment
    MonadReader env m
  , -- and @env@ has the field @logAction@ with this type @Colog.LogAction b msg@
    -- which means "a log action of type msg -> b ()"
    HasField "logAction" (Colog.LogAction b msg) env
  )
  => -- then the monad @m@ has logging capabilities as well
  HasLog msg b m
  where
  getLogAction :: m (Colog.LogAction b msg)
  getLogAction = asks $ getField @"logAction"
  setLogAction :: Colog.LogAction b msg -> m a -> m a
  setLogAction = local . setField @"logAction"

class HasLog msg b m | m -> b where
  getLogAction :: (MonadBase b m) => m (Colog.LogAction b msg)
  setLogAction :: (MonadBase b m) => Colog.LogAction b msg -> m a -> m a

type HasLog' msg env b m =
  ( MonadBase b m
  , MonadReader env m
  , HasField "logAction" (Colog.LogAction b msg) env
  , HasLog msg b m
  )
