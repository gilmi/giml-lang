{-# LANGUAGE DerivingVia #-}

-- | Common data types
module Language.Giml.Common where

import Data.Data (Data)
import Data.Map qualified as M
import Data.String
import Data.Text qualified as T
import GHC.Generics
import Prettyprinter

-- | A type variable name
newtype TypeVar = TV {unTypeVar :: T.Text}
  deriving (Show, Eq, Ord, Data, Generic)
  deriving (IsString, Pretty) via T.Text

-- | A type constructor name
newtype TypeCon = TC {unTypeCon :: T.Text}
  deriving (Show, Eq, Ord, Data, Generic)
  deriving (IsString, Pretty) via T.Text

-- | A data constructor
newtype Constr = C {unConstr :: T.Text}
  deriving (Show, Eq, Ord, Data, Generic)
  deriving (IsString, Pretty) via T.Text

-- | A record label
newtype Label = L {unLabel :: T.Text}
  deriving (Show, Eq, Ord, Data, Generic)
  deriving (IsString, Pretty) via T.Text

-- | A general record data type
type Record a =
  M.Map Label a

-- | A general variant data type
data Variant a
  = Variant Constr a
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)
