{-# LANGUAGE OverloadedStrings #-}

-- | Prettyprint Giml
--
-- Convert Giml types, ast, and errors to text
module Language.Giml.Pretty where

import Language.Giml.Syntax.Parser (SourcePos (..), unPos)
import Language.Giml.Syntax.Types
import Language.Giml.Utils
import Prettyprinter
import Prettyprinter.Render.Text (renderStrict)

-- * Print

render :: Doc a -> Text
render = renderStrict . layoutPretty defaultLayoutOptions

printSourcePos :: SourcePos -> Text
printSourcePos = render . ppSourcePos

printType :: Type -> Text
printType = render . ppType

-- * Pretty Ann

ppSourcePos :: SourcePos -> Doc ann
ppSourcePos src =
  encloseSep
    ""
    ""
    ":"
    [ -- pretty $ sourceName src
      pretty $ unPos $ sourceLine src
    , pretty $ unPos $ sourceColumn src
    ]

-- * Pretty Types

ppType :: Type -> Doc ann
ppType = ppType' Nah

ppType' :: AddParens -> Type -> Doc ann
ppType' addParens = \case
  TypeVar t -> pretty t
  TypeCon t -> pretty t
  TypeApp (TypeApp (TypeCon "->") t1) t2 ->
    (if Nah /= addParens then parens else id) $ fun' [ppType' IfFunction t1, ppType' Nah t2]
  TypeApp t1 t2 ->
    (if IfDatatype == addParens then parens else id) $ ppType' Nah t1 <+> ppType' IfDatatype t2
  TypeRec record -> record' $ ppMapping record
  TypeRecExt record tv -> recordAlt (ppMapping record) (pretty tv)
  TypeVariant vars -> variant' $ ppMapping vars
  TypePolyVariantLB vars tv -> variantLB (ppMapping vars) (pretty tv)
  TypePolyVariantUB tv vars -> variantUB (ppMapping vars) (pretty tv)
  TypeScheme vars t ->
    (if Nah /= addParens then parens else id) $
      encloseSep' "forall " "." " " (map pretty vars)
        <+> ppType' Nah t

data AddParens
  = Nah
  | IfFunction
  | IfDatatype
  deriving (Eq)

ppMapping :: (Pretty l) => [(l, Type)] -> [Doc ann]
ppMapping record =
  map
    (\(k, v) -> pretty k <+> ":" <+> ppType v)
    record

-- * Helpers

fun' :: [Doc ann] -> Doc ann
fun' = encloseSep' "" "" " -> "

record' :: [Doc ann] -> Doc ann
record' = encloseSep' "{" "}" ", "

recordAlt :: [Doc ann] -> Doc ann -> Doc ann
recordAlt = encloseSepAlt "{" "}" ", " (flatAlt "| " " | ")

variant' :: [Doc ann] -> Doc ann
variant' = encloseSep' "[" "]" " | "

-- we don't actually want to display the hidden variable but we'll do it for debugging purposes.
variantLB :: [Doc ann] -> Doc ann -> Doc ann
variantLB variants t
  | null variants = t
  | otherwise = encloseSep' ("[" <> t <> ">") "]" " | " variants

-- we don't actually want to display the hidden variable but we'll do it for debugging purposes.
variantUB :: [Doc ann] -> Doc ann -> Doc ann
variantUB variants t = encloseSep' ("[" <> t <> "<") "]" " | " variants

tupled' :: [Doc ann] -> Doc ann
tupled' = encloseSep' "(" ")" ", "

braced' :: [Doc ann] -> Doc ann
braced' = encloseSep' "<" ">" ", "

encloseSep' :: Doc ann -> Doc ann -> Doc ann -> [Doc ann] -> Doc ann
encloseSep' open close seperator =
  align
    . group
    . encloseSep
      (flatAlt (open <> " ") open)
      (flatAlt (hardline <> close) close)
      seperator
    . map align

encloseSepAlt :: Doc ann -> Doc ann -> Doc ann -> Doc ann -> [Doc ann] -> Doc ann -> Doc ann
encloseSepAlt open close seperator lastsep elems lastelem =
  align . group $
    myEncloseSep
      (flatAlt (open <> " ") open)
      (flatAlt (hardline <> close) close)
      seperator
      lastsep
      (map align elems)
      lastelem

myEncloseSep
  :: Doc ann
  -- ^ left delimiter
  -> Doc ann
  -- ^ right delimiter
  -> Doc ann
  -- ^ separator
  -> Doc ann
  -- ^ last separator
  -> [Doc ann]
  -- ^ input documents
  -> Doc ann
  -- ^ last element
  -> Doc ann
myEncloseSep open close seperator lastsep ds lastelem =
  case ds of
    [] -> open <> lastsep <> lastelem <> close
    [d] -> open <> d <> lastsep <> lastelem <> close
    _ -> cat (zipWith (<>) (open : replicate (length ds - 1) seperator <> [lastsep]) (ds <> [lastelem])) <> close
