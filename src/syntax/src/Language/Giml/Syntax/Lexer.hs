{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings #-}

module Language.Giml.Syntax.Lexer where

import Data.Text qualified as T
import Language.Giml.Utils
import Text.Megaparsec ((<?>))
import Text.Megaparsec qualified as P
import Text.Megaparsec.Char qualified as P
import Text.Megaparsec.Char.Lexer qualified as L
import Text.Megaparsec.Pos
import Prelude hiding (lex)

-----------------------------------------------------------

-- * Error

newtype LexerError
  = LexerError (P.ParseErrorBundle Text ())
  deriving stock (Show, Eq, Data, Typeable, Generic)
  deriving newtype (NFData)

-----------------------------------------------------------

data Loc = Loc
  { locStart :: SourcePos
  , locEnd :: SourcePos
  , locLength :: Int
  }
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

-- * Token

data Token = Token
  { tokToken :: RawToken
  , tokLoc :: Loc
  }
  deriving (Eq, Ord, Data, Typeable, Generic, NFData)

instance Show Token where
  show t =
    unwords
      [ show (tokToken t)
      , ";"
      , concat
          [ show (sourceLine $ locStart $ tokLoc t)
          , ":"
          , show (unPos $ sourceColumn $ locStart $ tokLoc t)
          ]
      , concat
          [ show (unPos $ sourceLine $ locEnd $ tokLoc t)
          , ":"
          , show (unPos $ sourceColumn $ locEnd $ tokLoc t)
          ]
      , "; Len "
      , show (locLength $ tokLoc t)
      ]

data RawToken
  = -- Literals
    Identifier !Text
  | UpperIdentifier !Text
  | HashIdentifier !Text
  | Op !Text
  | Integer !Int32
  | String !Text
  | Character !Char
  | Boolean !Bool
  | LineComment Text
  | -- Keywords
    Let
  | In
  | Case
  | Of
  | Do
  | Ffi
  | -- Punctuation
    Equals
  | Colon
  | Dot
  | Comma
  | Lambda
  | Bar
  | Underscore
  | -- Arrows
    Arrow
  | LeftArrow
  | -- Other
    EOF
  | Indentation !Word64
  | -- Parens
    LParen
  | RParen
  | LBracket
  | RBracket
  | LBrace
  | RBrace
  --  -- Error
  --  | Error !LexerError
  deriving (Show, Eq, Ord, Data, Typeable, Generic, NFData)

type Parser = P.Parsec Void Text

type ErrBundle = P.ParseErrorBundle Text Void

keywords :: [Text]
keywords =
  [ "let"
  , "in"
  , "case"
  , "of"
  , "do"
  , "ffi"
  ]

-----------------------------------------------------------

-- * Lexer

runLexer :: FilePath -> Text -> Either ErrBundle [Token]
runLexer = P.runParser parseTokens

parseTokens :: Parser [Token]
parseTokens = do
  startPos <- P.getSourcePos
  P.choice
    [ parseSimpleTokens
    , (:)
        <$> (numberToken startPos id <?> "number")
        <*> parseSimpleTokens
    , (:)
        <$> (identifierToken <?> "identifier")
        <*> parseSimpleTokens
    , (:)
        <$> (upperIdentifierToken <?> "constructor")
        <*> parseSimpleTokens
    , (:)
        <$> (hashIdentifierToken <?> "variant")
        <*> parseSimpleTokens
    , (:)
        <$> (stringToken <?> "string")
        <*> parseSimpleTokens
    , (:)
        <$> (charToken <?> "character")
        <*> parseSimpleTokens
    ]

parseSimpleTokens :: Parser [Token]
parseSimpleTokens = do
  posStart <- P.getSourcePos
  P.choice
    [ do
        tok <- singleCharToken posStart '.' Dot
        (:) tok <$> parseTokens
    , do
        tok <- simpleToken posStart
        maybe id (:) tok <$> parseTokens
    , pure <$> eofToken posStart
    ]

simpleToken :: P.SourcePos -> Parser (Maybe Token)
simpleToken posStart = do
  P.choice
    [ pure <$> parenTokens posStart <?> "parenthesis"
    , pure <$> underscoreToken posStart <?> "underscore"
    , opTokens posStart <?> "operator"
    , spaceConsumer <?> "space"
    ]

eofToken :: P.SourcePos -> Parser Token
eofToken posStart =
  P.eof $> Token EOF (Loc posStart posStart 0)

parenTokens :: P.SourcePos -> Parser Token
parenTokens posStart =
  P.choice
    [ singleCharToken posStart '(' LParen
    , singleCharToken posStart ')' RParen
    , singleCharToken posStart '[' LBracket
    , singleCharToken posStart ']' RBracket
    , singleCharToken posStart '{' LBrace
    , singleCharToken posStart '}' RBrace
    ]

opTokens :: P.SourcePos -> Parser (Maybe Token)
opTokens posStart = do
  txt <- P.some $ P.oneOf opChars
  let
    mkToken tok len = do
      posEnd <- P.getSourcePos
      pure $ pure $ Token tok $ Loc posStart posEnd len
  case txt of
    "-" -> do
      pure <$> numberToken posStart negate
    "--" -> do
      _comment <- P.takeWhileP (Just "character") (/= '\n')
      -- mkToken (LineComment comment) (T.length comment + 2)
      pure Nothing
    "=" -> mkToken Equals 1
    ":" -> mkToken Colon 1
    "." -> mkToken Dot 1
    "," -> mkToken Comma 1
    "|" -> mkToken Bar 1
    "_" -> mkToken Underscore 1
    "\\" -> mkToken Lambda 1
    "->" -> mkToken Arrow 2
    "<-" -> mkToken LeftArrow 2
    _ -> mkToken (Op $ T.pack txt) (length txt)

underscoreToken :: P.SourcePos -> Parser Token
underscoreToken posStart = do
  _ <- P.char '_'
  posEnd <- P.getSourcePos
  pure $ Token Underscore (Loc posStart posEnd 1)

spaceConsumer :: Parser (Maybe Token)
spaceConsumer =
  P.choice
    [ do
        _ <- P.some (P.char ' ')
        indentationTokens Nothing <?> "indentation"
    , rawIndentation <?> "indentation"
    ]

rawIndentation :: Parser (Maybe Token)
rawIndentation = do
  _ <- P.newline
  posStart <- P.getSourcePos
  tabs <- P.many (P.char '\t')
  posEnd <- P.getSourcePos
  let
    indentation =
      Token
        (Indentation (fromIntegral $ length tabs))
        ( Loc
            posStart
            posEnd
            (length tabs)
        )
  indentationTokens (Just indentation)

indentationTokens :: Maybe Token -> Parser (Maybe Token)
indentationTokens indents =
  P.choice
    [ rawIndentation
    , commentToken indents
    , pure indents
    ]

commentToken :: Maybe Token -> Parser (Maybe Token)
commentToken indents = do
  void $ P.string "--"
  _comment <- P.takeWhileP (Just "character") (/= '\n')
  indentationTokens indents

identifierToken :: Parser Token
identifierToken = do
  posStart <- P.getSourcePos
  txt <-
    (:)
      <$> P.oneOf identBeginChars
      <*> P.many (P.oneOf identRestChars)
  posEnd <- P.getSourcePos
  let
    mkToken tok = pure . Token tok . Loc posStart posEnd
  case txt of
    "let" -> mkToken Let 3
    "in" -> mkToken In 2
    "case" -> mkToken Case 4
    "of" -> mkToken Of 2
    "do" -> mkToken Do 2
    "ffi" -> mkToken Ffi 3
    _ -> mkToken (Identifier $ T.pack txt) (length txt)

upperIdentifierToken :: Parser Token
upperIdentifierToken = do
  posStart <- P.getSourcePos
  txt <-
    (:)
      <$> P.oneOf upperIdentBeginChars
      <*> P.many (P.oneOf identRestChars)
  posEnd <- P.getSourcePos
  let
    mkToken tok = pure . Token tok . Loc posStart posEnd
  case txt of
    "True" -> mkToken (Boolean True) 4
    "False" -> mkToken (Boolean False) 5
    _ -> mkToken (UpperIdentifier $ T.pack txt) (length txt)

hashIdentifierToken :: Parser Token
hashIdentifierToken = do
  posStart <- P.getSourcePos
  _ <- P.char '#'
  txt <-
    (:)
      <$> P.oneOf upperIdentBeginChars
      <*> P.many (P.oneOf identRestChars)
  posEnd <- P.getSourcePos
  let
    mkToken tok = pure . Token tok . Loc posStart posEnd
  case txt of
    -- todo: remove
    "True" -> mkToken (Boolean True) 4
    "False" -> mkToken (Boolean False) 5
    _ -> mkToken (HashIdentifier $ T.pack txt) (length txt)

-----

singleCharToken :: P.SourcePos -> Char -> RawToken -> Parser Token
singleCharToken posStart char tok =
  P.char char *> do
    posEnd <- P.getSourcePos
    pure $ Token tok $ Loc posStart posEnd 1

-----

opChars :: [Char]
opChars = "!@$%^&*+<>=|-:.,\\"

identBeginChars :: [Char]
identBeginChars = ['a' .. 'z']

upperIdentBeginChars :: [Char]
upperIdentBeginChars = ['A' .. 'Z']

identRestChars :: [Char]
identRestChars =
  concat
    [ ['a' .. 'z']
    , ['A' .. 'Z']
    , ['0' .. '9']
    , ['_']
    ]

-----

numberToken :: SourcePos -> (Int32 -> Int32) -> Parser Token
numberToken posStart sign = do
  tok <- numberRawToken sign
  posEnd <- P.getSourcePos
  pure $ Token tok $ Loc posStart posEnd (subtractCol posEnd posStart)

numberRawToken :: (Int32 -> Int32) -> Parser RawToken
numberRawToken sign = do
  let
    tok = fmap (Integer . sign)
  P.choice
    [ P.char '0'
        *> ( P.choice
              [ tok $ P.char 'b' *> L.binary
              , tok $ P.char 'x' *> L.hexadecimal
              , tok $ P.choice [L.decimal, pure 0]
              ]
           )
    , tok L.decimal
    ]

charToken :: Parser Token
charToken = do
  posStart <- P.getSourcePos
  char <- P.between (P.char '\'') (P.char '\'') L.charLiteral
  posEnd <- P.getSourcePos
  pure $
    Token
      (Character char)
      ( Loc
          posStart
          posEnd
          (subtractCol posEnd posStart)
      )

stringToken :: Parser Token
stringToken = do
  posStart <- P.getSourcePos
  str <- P.char '\"' *> P.manyTill L.charLiteral (P.char '\"')
  posEnd <- P.getSourcePos
  pure $
    Token
      (String $ T.pack str)
      ( Loc
          posStart
          posEnd
          (subtractCol posEnd posStart)
      )

subtractCol :: SourcePos -> SourcePos -> Int
subtractCol end start =
  unPos (sourceColumn end) - unPos (sourceColumn start)
