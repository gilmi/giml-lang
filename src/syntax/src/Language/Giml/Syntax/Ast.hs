{-# LANGUAGE OverloadedStrings #-}

-- | Giml AST definition
module Language.Giml.Syntax.Ast (
  -- * Language definition
  module Language.Giml.Syntax.Ast,

  -- * Common data types
  module Language.Giml.Common,

  -- * Giml types
  module Language.Giml.Syntax.Types,
) where

import Data.Map qualified as M
import Data.Text qualified as T
import Language.Giml.Common
import Language.Giml.Syntax.Types
import Language.Giml.Utils

-- * Language definition

-- | The type of a source file as seen by the parser
data ParsedFile a
  = ParsedFile [Definition a]
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | The type of a source file after ordering and grouping
data File a
  = File [Datatype a] [[TermDef a]]
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | A top level definition
data Definition a
  = -- | A type definition
    TypeDef (Datatype a)
  | -- | A term definition
    TermDef (TermDef a)
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | A term definition
data TermDef a
  = -- | A variable
    Variable a Var (Maybe Type) (Expr a)
  | -- | A function
    --  | Operator a OpDef -- ^ An operator definition
    Function a Var (Maybe Type) [Maybe Var] (Expr a)
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | A Block is a list of statements
type Block a = [Statement a]

-- | A data type definition
data Datatype a
  = Datatype a TypeCon (Maybe Kind) [TypeVar] [Variant (Maybe Type)]
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | A Statement
data Statement a
  = -- | An expression
    SExpr a (Expr a)
  | -- | A term definition
    SDef a (TermDef a)
  | -- | A bind statement
    SBind a Var (Expr a)
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | An expression
data Expr a
  = -- | Annotates the child expression with some annotation
    EAnnotated a (Expr a)
  | -- | A Literal
    ELit Lit
  | -- | A variable name
    EVar Var
  | -- | An anonymous function
    EFun [Maybe Var] (Expr a)
  | -- | An operator
    EOp OpDef
  | -- | A function call
    EFunCall (Expr a) [Expr a]
  | -- | A do block
    EBlock (Block a)
  | -- | A let expression
    ELet (TermDef a) (Expr a)
  | -- | A variant (or data constructor)
    EVariant Constr
  | -- | An open variant
    EOpenVariant Constr
  | -- | A record
    ERecord (Record (Expr a))
  | -- | Record access to a specific label
    ERecordAccess (Expr a) Label
  | -- | Extends a record
    ERecordExtension (Record (Expr a)) (Expr a)
  | -- | A 2-way if expression
    EIf (Expr a) (Expr a) (Expr a)
  | -- | A case expression (pattern matching)
    ECase (Expr a) [(Pattern, Expr a)]
  | -- | A foreign function interface call
    EFfi T.Text (Maybe Type) [Expr a]
  deriving (Show, Eq, Ord, Data, Functor, Foldable, Traversable)

-- | A literal
data Lit
  = -- | Booleans
    LBool Bool
  | -- | Integers
    LInt Int32
  | -- | Floating point numbers
    LFloat Float
  | -- | Strings
    LString T.Text
  deriving (Show, Eq, Ord, Data)

-- | A pattern
data Pattern
  = -- | A catch all pattern
    PWildcard
  | -- | A variable capture pattern
    PVar Var
  | -- | A literal pattern
    PLit Lit
  | -- | A variant pattern
    PVariant (Variant (Maybe Pattern))
  | -- | An open variant pattern
    POpenVariant (Variant Pattern)
  | -- | A record pattern
    PRecord (Record Pattern)
  deriving (Show, Eq, Ord, Data)

-- | A variable name
type Var = T.Text

-- | Operator definition
data OpDef = OpDef
  { opName :: Var
  , opFunRef :: Var
  , opFixity :: OpFixity
  , opPrecedence :: Word8
  }
  deriving (Show, Eq, Ord, Data)

-- | Define the associativity of the operator
data OpFixity
  = InfixLeft
  | InfixRight
  | Infix
  deriving (Show, Eq, Ord, Data)

-- * Utils

-- | Create a record expression (or extension) from a a list of (label, expression)
--   and a expression to extend (if needed).
--
--   Note that this function is left-biased - duplicate fields will preserve the first field.
mkERec :: [(Label, Expr a)] -> Maybe (Expr a) -> Expr a
mkERec fields =
  maybe
    (ERecord $ M.fromListWith (flip const) fields)
    (ERecordExtension $ M.fromListWith (flip const) fields)

-- | A lambda without wildcards
efun :: [Var] -> Expr a -> Expr a
efun args body = EFun (Just <$> args) body

-- | Extract a term definition and annotation.
getTermDef :: Definition ann -> Maybe (TermDef ann)
getTermDef = \case
  TermDef def -> Just def
  TypeDef {} -> Nothing

-- | Get the name of a term definition.
getTermName :: TermDef a -> Var
getTermName = \case
  Variable _ name _ _ -> name
  Function _ name _ _ _ -> name

--  Operator _ op -> opName op

-- | Get definition name
getDefName :: Definition ann -> Text
getDefName = \case
  TermDef termdef -> getTermName termdef
  TypeDef (Datatype _ (TC name) _ _ _) -> name

-- | Extract a type definition and annotation.
getTypeDef :: Definition ann -> Maybe (Datatype ann)
getTypeDef = \case
  TermDef {} -> Nothing
  TypeDef def -> Just def

-- | Get the name of a term definition.
getTermAnn :: TermDef ann -> ann
getTermAnn = \case
  Variable ann _ _ _ -> ann
  Function ann _ _ _ _ -> ann

--  Operator ann _ -> ann

-- | Set the annotation of a term definition.
setTermAnn :: a -> TermDef a -> TermDef a
setTermAnn ann = \case
  Variable _ name typ expr -> Variable ann name typ expr
  Function _ name typ args body -> Function ann name typ args body

--  Operator _ op -> Operator ann op

-- | Get the name of a datatype definition.
getTypeName :: Datatype ann -> TypeCon
getTypeName = \case
  Datatype _ name _ _ _ -> name

-- | Get the annotation of a datatype definition.
getTypeAnn :: Datatype ann -> ann
getTypeAnn = \case
  Datatype ann _ _ _ _ -> ann

-- | Set the annotation of a datatype definition.
setTypeAnn :: b -> Datatype a -> Datatype b
setTypeAnn ann = \case
  Datatype _ a b c d -> Datatype ann a b c d

-- | Retrieve the annotation of an expression. Will explode when used on a non @EAnnotated@ node.
getExprAnn :: (Show a) => Expr a -> a
getExprAnn = \case
  EAnnotated typ _ -> typ
  e -> error $ toString $ "Expr is not annotated: " <> pShow e
