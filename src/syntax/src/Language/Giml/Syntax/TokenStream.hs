{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Language.Giml.Syntax.TokenStream where

import Data.List qualified as DL
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NE
import Data.Proxy
import Data.Text qualified as T
import Language.Giml.Syntax.Lexer
import Language.Giml.Utils
import Text.Megaparsec qualified as P
import Text.Megaparsec.Pos
import Text.Megaparsec.Stream qualified as MS
import Prelude hiding (lex)

-----------------------------------------------------------

data TokenStream = TokenStream
  { streamInput :: Text -- for showing offending lines
  , unStream :: [Token]
  }
  deriving (Show)

instance MS.Stream TokenStream where
  type Token TokenStream = Token
  type Tokens TokenStream = [Token]
  tokenToChunk Proxy x = [x]
  tokensToChunk Proxy xs = xs
  chunkToTokens Proxy = id
  chunkLength Proxy = length
  chunkEmpty Proxy = null
  take1_ (TokenStream _ []) = Nothing
  take1_ (TokenStream str (t : ts)) =
    Just
      ( t
      , TokenStream (T.drop (MS.tokensLength pxy (t :| [])) str) ts
      )
  takeN_ n (TokenStream str s)
    | n <= 0 = Just ([], TokenStream str s)
    | null s = Nothing
    | otherwise =
        let
          (x, s') = splitAt n s
        in
          case NE.nonEmpty x of
            Nothing -> Just (x, TokenStream str s')
            Just nex -> Just (x, TokenStream (T.drop (MS.tokensLength pxy nex) str) s')
  takeWhile_ f (TokenStream str s) =
    let
      (x, s') = DL.span f s
    in
      case NE.nonEmpty x of
        Nothing -> (x, TokenStream str s')
        Just nex -> (x, TokenStream (T.drop (MS.tokensLength pxy nex) str) s')

instance MS.VisualStream TokenStream where
  showTokens Proxy =
    DL.intercalate " "
      . NE.toList
      . fmap (toString . pShow . tokToken)
  tokensLength Proxy xs = sum (locLength . tokLoc <$> xs)

instance MS.TraversableStream TokenStream where
  reachOffset o P.PosState {..} =
    ( Just (prefix ++ restOfLine)
    , P.PosState
        { pstateInput =
            TokenStream
              { streamInput = T.pack postStr
              , unStream = post
              }
        , pstateOffset = max pstateOffset o
        , pstateSourcePos = newSourcePos
        , pstateTabWidth = pstateTabWidth
        , pstateLinePrefix = prefix
        }
    )
    where
      prefix =
        if sameLine
          then pstateLinePrefix ++ preLine
          else preLine
      sameLine = sourceLine newSourcePos == sourceLine pstateSourcePos
      newSourcePos =
        case post of
          [] -> pstateSourcePos
          (x : _) -> locStart $ tokLoc x
      (pre, post) = splitAt (o - pstateOffset) (unStream pstateInput)
      (preStr, postStr) = splitAt tokensConsumed (T.unpack $ streamInput pstateInput)
      preLine = reverse . takeWhile (/= '\n') . reverse $ preStr
      tokensConsumed =
        case NE.nonEmpty pre of
          Nothing -> 0
          Just nePre -> MS.tokensLength pxy nePre
      restOfLine = takeWhile (/= '\n') postStr

pxy :: Proxy TokenStream
pxy = Proxy
