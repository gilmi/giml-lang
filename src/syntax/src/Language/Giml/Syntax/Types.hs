{-# LANGUAGE OverloadedStrings #-}

-- | Type definitions
module Language.Giml.Syntax.Types where

import Data.Data (Data)
import Data.Foldable (foldl')
import Language.Giml.Common

-- | A data type representing kinds.
data Kind
  = Type
  | KindFun Kind Kind
  deriving (Show, Eq, Ord, Data)

-- | A data type representing types.
data Type
  = -- | type variables, like @a@
    TypeVar TypeVar
  | -- | type constructors, like @List@ and @Int@
    TypeCon TypeCon
  | -- | type application, like @List a@
    TypeApp Type Type
  | -- | the type of a record, like @{ x : Int, y : String }@
    TypeRec [(Label, Type)]
  | -- | a record with an extension, such as @{ x : Int, y : String | r }@,
    --   means "this record has at least @{ x : Int, y : String }@, but can have more fields"
    TypeRecExt [(Label, Type)] TypeVar
  | -- | the type of a closed variant, such as @[ Some : Int | Nil : {} ]@,
    --   the value can be one of the constructors.
    TypeVariant [(Constr, Type)]
  | -- | the type of a lower bounded polymorphic variant, such as @[a> Some : Int | Nil : {} ]@,
    --   the value can be at least one of the constructors or potentially more.
    TypePolyVariantLB [(Constr, Type)] TypeVar
  | -- | the type of a upper bounded polymorphic variant, such as @[a< Some : Int | Nil : {} ]@,
    --   the value can be at most one of the constructors or potentially less.
    --
    -- The type variable here is special and is used to track the information of
    -- which variants we already know of during the type inference process.
    TypePolyVariantUB TypeVar [(Constr, Type)]
  | -- | A generalized type which closes over all of its type variables,
    --   such as @forall a b. a -> b -> a@
    TypeScheme [TypeVar] Type
  deriving (Show, Eq, Ord, Data)

typeFun :: [Type] -> Type -> Type
typeFun argsT retT =
  foldr (\i o -> TypeApp (TypeApp (TypeCon "->") i) o) retT argsT

typeApp :: Type -> [Type] -> Type
typeApp = foldl' TypeApp

toTypeFun :: Type -> ([Type], Type)
toTypeFun = \case
  TypeApp (TypeApp (TypeCon "->") i) o ->
    let
      (f, r) = toTypeFun o
    in
      (i : f, r)
  other ->
    ([], other)

overTypes :: (Monad m) => (Type -> m Type) -> Type -> m Type
overTypes f = \case
  TypeRec record -> do
    r <- traverse (traverse $ overTypes f) record
    f (TypeRec r)
  TypeRecExt record ext -> do
    r <- traverse (traverse $ overTypes f) record
    f (TypeRecExt r ext)
  TypeVariant vars -> do
    vars' <- traverse (traverse $ overTypes f) vars
    f (TypeVariant vars')
  TypePolyVariantUB tv vars -> do
    vars' <- traverse (traverse $ overTypes f) vars
    f (TypePolyVariantUB tv vars')
  TypePolyVariantLB vars tv -> do
    vars' <- traverse (traverse $ overTypes f) vars
    f (TypePolyVariantLB vars' tv)
  TypeScheme vars typ -> do
    inner <- TypeScheme vars <$> overTypes f typ
    f inner
  TypeApp t1 t2 -> do
    inner <- TypeApp <$> overTypes f t1 <*> overTypes f t2
    f inner
  TypeVar var -> f (TypeVar var)
  TypeCon con -> f (TypeCon con)
