{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

-- | Parser of Giml types
module Language.Giml.Syntax.Parse.Type where

import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Token
import Text.Megaparsec qualified as P

parseTypeDef :: Parser (Datatype Loc)
parseTypeDef = do
  Annotated name loc <- typeCon
  vars <- P.many typeVar
  indentation *> parseEquals
  variants <- P.many $ indented do
    parseBar
    parseVariant
  pure $ Datatype loc name Nothing (map getThing vars) (map getThing variants)

parseVariant :: Parser (Annotated Loc (Variant (Maybe Type)))
parseVariant = do
  con <- dataCon
  typ <- P.optional (indented parseType)
  pure
    Annotated
      { getThing = Variant (getThing con) (getThing <$> typ)
      , getAnn = getAnn con
      }

parseType' :: Parser (Annotated Loc Type)
parseType' =
  P.choice
    [ fmap TypeVar <$> typeVar
    , fmap TypeCon <$> typeCon
    , do
        Annotated (record, ext) loc <- parseRecord parseType (Just typeVar)
        pure $
          maybe
            (Annotated (TypeRec $ fmap getThing <$> record) loc)
            (flip Annotated loc . TypeRecExt (fmap getThing <$> record) . getThing)
            ext
    , parens parseType
    ]

parseType :: Parser (Annotated Loc Type)
parseType =
  foldr1 arrow . map (foldl1 app)
    <$> (P.some (P.try $ indented parseType') `P.sepBy1` P.try (indented parseArrow))
  where
    app a b =
      Annotated
        (TypeApp (getThing a) (getThing b))
        (chainLoc 1 (getAnn a) (getAnn b))

    arrow a b =
      Annotated
        (TypeApp (TypeApp (TypeCon "->") (getThing a)) (getThing b))
        (chainLoc 4 (getAnn a) (getAnn b))

parseRecord' :: Parser (Annotated Loc [(Label, Type)])
parseRecord' = do
  start <- P.getSourcePos
  let
    pfield = do
      (Annotated label _) <-
        indentation *> parseIdent
      indentation *> parseColon
      Annotated field _ <- indented parseType
      pure (L label, field)
  mapping <- curly do
    pfield `P.sepBy` P.try (indented parseComma)
  end <- P.getSourcePos
  pure $ Annotated mapping (Loc start end 0)

typeCon :: Parser (Annotated Loc TypeCon)
typeCon =
  token "Type constructor" tc
  where
    tc t =
      case t of
        UpperIdentifier i -> Just (TC i)
        _ -> Nothing

typeVar :: Parser (Annotated Loc TypeVar)
typeVar =
  token "Type variable" tv
  where
    tv t =
      case t of
        Identifier i -> Just (TV i)
        _ -> Nothing
