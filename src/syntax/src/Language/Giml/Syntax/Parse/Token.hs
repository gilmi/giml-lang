{-# LANGUAGE OverloadedStrings #-}

-- | Parsing Giml tokens
module Language.Giml.Syntax.Parse.Token where

import Data.Text qualified as T
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Utils
import Text.Megaparsec qualified as P

parens :: (Show a) => Parser a -> Parser a
parens parser = do
  _ <- token "open parenthesis" topen
  result <- indented parser
  _ <- indentation *> parenClose
  pure result
  where
    topen t =
      case t of
        LParen -> Just ()
        _ -> Nothing

parenClose :: Parser ()
parenClose = do
  void $ token "close paren" parser
  where
    parser t =
      case t of
        RParen -> Just ()
        _ -> Nothing

curly :: (Show a) => Parser a -> Parser a
curly parser = do
  _ <- token "open curly bracket" topen
  result <- parser
  _ <- indentation *> curlyClose
  pure result
  where
    topen t =
      case t of
        LBrace -> Just ()
        _ -> Nothing

curlyClose :: Parser ()
curlyClose = do
  void $ token "close curly bracket" tclose
  where
    tclose t =
      case t of
        RBrace -> Just ()
        _ -> Nothing

dataCon :: Parser (Annotated Loc Constr)
dataCon =
  token "Data constructor" c
  where
    c t =
      case t of
        UpperIdentifier i -> Just (C i)
        _ -> Nothing

hashedCon :: Parser (Annotated Loc Constr)
hashedCon =
  token "Polymorphic variant" c
  where
    c t =
      case t of
        HashIdentifier i -> Just (C i)
        _ -> Nothing

parseIdent :: Parser (Annotated Loc Var)
parseIdent =
  token "identifier" tident
  where
    tident t =
      case t of
        Identifier i -> Just i
        _ -> Nothing

funArg :: Parser (Maybe Var)
funArg =
  P.choice
    [ Nothing <$ parseUnderscore
    , Just . getThing <$> parseIdent
    ]

parseUnderscore :: Parser ()
parseUnderscore =
  void (token "_" tunder)
  where
    tunder t =
      case t of
        Underscore -> Just ()
        _ -> Nothing

parseArrow :: Parser ()
parseArrow = do
  void (token "arrow" tarrow)
  where
    tarrow t =
      case t of
        Arrow -> Just ()
        _ -> Nothing

parseLeftArrow :: Parser ()
parseLeftArrow = do
  void (token "left arrow" parser)
  where
    parser t =
      case t of
        LeftArrow -> Just ()
        _ -> Nothing

parseBar :: Parser ()
parseBar = do
  void (token "|" tbar)
  where
    tbar t =
      case t of
        Bar -> Just ()
        _ -> Nothing

parseEquals :: Parser ()
parseEquals = do
  void (token "=" parser)
  where
    parser t =
      case t of
        Equals -> Just ()
        _ -> Nothing

parseColon :: Parser ()
parseColon = do
  void (token ":" parser)
  where
    parser t =
      case t of
        Colon -> Just ()
        _ -> Nothing

parseComma :: Parser ()
parseComma = do
  void (token "," parser)
  where
    parser t =
      case t of
        Comma -> Just ()
        _ -> Nothing

parseDot :: Parser ()
parseDot = do
  void (token "." parser)
  where
    parser t =
      case t of
        Dot -> Just ()
        _ -> Nothing

parseDo :: Parser ()
parseDo = do
  void (token "do" tdo)
  where
    tdo t =
      case t of
        Do -> Just ()
        _ -> Nothing

parseFfi :: Parser ()
parseFfi = do
  void (token "ffi" tffi)
  where
    tffi t =
      case t of
        Ffi -> Just ()
        _ -> Nothing

parseLet :: Parser ()
parseLet = do
  void (token "let" tlet)
  where
    tlet t =
      case t of
        Let -> Just ()
        _ -> Nothing

parseIn :: Parser ()
parseIn = do
  void (token "in" tin)
  where
    tin t =
      case t of
        In -> Just ()
        _ -> Nothing

parseRecord
  :: (Show a, Show b)
  => Parser a
  -> Maybe (Parser b)
  -> Parser (Annotated Loc ([(Label, a)], Maybe b))
parseRecord pa mpb = do
  let
    fancyIndent1 = indentation *> P.notFollowedBy (curlyClose P.<|> parseBar)
    fancyIndent2 = indentation *> P.notFollowedBy curlyClose
  start <- P.getSourcePos
  (mapping, b) <- curly do
    let
      pfield = do
        (Annotated label _) <-
          indentation *> parseIdent
        indentation *> parseColon
        field <- indented pa
        pure (L label, field)
    mapping <-
      indented pfield
        `P.sepBy` (P.try fancyIndent1 *> parseComma)
    b <-
      maybe
        (pure Nothing)
        (\pb -> P.optional (P.try fancyIndent2 *> parseBar *> pb))
        mpb
    pure (mapping, b)
  end <- P.getSourcePos
  let
    loc = Loc start end 0
  pure $ Annotated (mapping, b) loc

parseLit :: Parser (Annotated Loc Lit)
parseLit =
  P.choice
    [ fmap LInt <$> token "integer" tint
    , -- , fmap Float <$> token "floating point number" tfloat
      fmap LString <$> parseStr
    , fmap LBool <$> token "boolean" tbool
    ]
  where
    tint t =
      case t of
        Integer i -> Just i
        _ -> Nothing
    -- tfloat t =
    --   case t of
    --     Floating f -> Just f
    --     _ -> Nothing
    tbool t =
      case t of
        Boolean b -> Just b
        _ -> Nothing

parseStr :: Parser (Annotated Loc Text)
parseStr = token "string" tstr
  where
    tstr t =
      case t of
        String s -> Just s
        _ -> Nothing

parseEOF :: Parser ()
parseEOF =
  P.token (teof . tokToken) mempty P.<?> "end of file"
  where
    teof t =
      case t of
        EOF -> Just ()
        _ -> Nothing

commented :: [Text] -> (Text -> Parser p) -> Parser p
commented builder p = do
  indentation
  P.choice
    [ do
        comment <- parseComment
        commented (comment : builder) p
    , p (T.unlines (reverse builder))
    ]

parseComment :: Parser Text
parseComment = getThing <$> token "comment" tcomment
  where
    tcomment t =
      case t of
        LineComment s -> Just s
        _ -> Nothing

indented :: (Show a) => Parser a -> Parser a
indented p = do
  local (+ 1) maybeIndentation >>= \case
    Just () -> local (+ 1) p
    Nothing -> p

indentation :: Parser ()
indentation = void maybeIndentation

maybeIndentation :: Parser (Maybe ())
maybeIndentation = do
  n <- ask
  void $ P.notFollowedBy (P.token (tindentBad n . tokToken) mempty P.<?> "indentation " <> show n)
  P.optional (P.token (tindent n . tokToken) mempty P.<?> "indentation " <> show n)
  where
    tindentBad n t =
      case t of
        Indentation m | n /= m -> Just ()
        _ -> Nothing
    tindent n t =
      case t of
        Indentation m | n == m -> Just ()
        _ -> Nothing

indentation' :: Parser Word64
indentation' =
  P.token (tindent . tokToken) mempty P.<?> "indentation'"
  where
    tindent t =
      case t of
        Indentation m -> Just m
        _ -> Nothing

rawToken :: RawToken -> Parser ()
rawToken raw =
  P.token (rawt . tokToken) mempty P.<?> "end of file"
  where
    rawt t
      | raw == t = Just ()
      | otherwise = Nothing

token :: String -> (RawToken -> Maybe a) -> Parser (Annotated Loc a)
token desc tok = P.token (withLoc tok) mempty P.<?> desc

withLoc :: (RawToken -> Maybe a) -> Token -> Maybe (Annotated Loc a)
withLoc f tok = flip Annotated (tokLoc tok) <$> f (tokToken tok)
