{-# LANGUAGE OverloadedStrings #-}

-- | Parser API
module Language.Giml.Syntax.Parse.API where

import Control.Monad.Except
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Language.Giml.Logging
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Definitions
import Language.Giml.Syntax.Parse.Expr
import Language.Giml.Syntax.Parse.Token
import Language.Giml.Syntax.Parse.Type
import Language.Giml.Syntax.TokenStream
import Language.Giml.Utils
import Text.Megaparsec qualified as P
import Text.Read (readMaybe)

-- * API

-- | Lexer and parser stream
-- runParser
--  :: Parser e
--  -> FilePath
--  -> Text
--  -> Either (Either ErrBundle ParseErr) e
runParser :: (CompilePhase Text env b m) => Parser a -> FilePath -> Text -> m a
runParser p file txt = do
  stream <-
    TokenStream txt
      <$> either (throwError . ppErrors) pure (runLexer file txt)
  -- ltraceM "stream" stream
  either
    (throwError . ppErrors)
    pure
    (runReader (P.runParserT (p <* parseEOF) file stream) 0)

parse :: Parser e -> T.Text -> Either T.Text e
parse p = runWithoutLogger . runParser p "input"

----------------------------------------------------

-- * Debugging stuff

-- | It's like @read@, but actually useful.
readError
  :: forall m a
   . (Read a, Typeable a, MonadError ParseErr m, MonadFail m)
  => String
  -> m a
readError str =
  case readMaybe str of
    Just v -> pure v
    Nothing ->
      fail $
        toString $
          T.unwords
            [ "Could not parse string"
            , pShow str
            , "to type"
            , pShow (typeOf (undefined :: a)) <> "."
            ]

-- | REPL testing utility function
pp :: Text -> IO ()
pp txt =
  case parse parseExpr txt of
    Left err -> do
      T.putStrLn err
    Right prog -> do
      T.putStrLn (pShow $ fmap (const ()) prog)

-- | REPL testing utility function
ppA :: Text -> IO ()
ppA txt =
  case parse parseExpr txt of
    Left err -> do
      T.putStrLn err
    Right prog -> do
      T.putStrLn (pShow prog)

-- | REPL testing utility function
ppd :: Text -> IO ()
ppd txt =
  case parse parseDefs txt of
    Left err -> do
      T.putStrLn err
    Right prog -> do
      T.putStrLn (pShow $ fmap (const ()) prog)

-- | REPL testing utility function
ppt :: Text -> IO ()
ppt txt = do
  T.putStrLn txt
  case parse parseType txt of
    Left err -> do
      T.putStrLn err
    Right prog -> do
      T.putStrLn (pShow prog)

ppErrors
  :: (P.TraversableStream a, P.VisualStream a)
  => P.ParseErrorBundle a Void
  -> Text
ppErrors = fromString . P.errorBundlePretty
