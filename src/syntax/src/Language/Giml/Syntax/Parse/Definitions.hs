{-# LANGUAGE OverloadedStrings #-}

-- | Parser of Giml programs
module Language.Giml.Syntax.Parse.Definitions where

import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Expr
import Language.Giml.Syntax.Parse.Token
import Language.Giml.Syntax.Parse.Type
import Language.Giml.Utils
import Text.Megaparsec qualified as P

---------------------------------------------------------

-- * Parser implementation

parseFile :: Parser (ParsedFile P.SourcePos)
parseFile = fmap locStart <$> parseDefs

parseDefs :: Parser (ParsedFile Loc)
parseDefs = ParsedFile <$> P.many (commented [] parseDef <* indentation)

parseDef :: Text -> Parser (Definition Loc)
parseDef _ = do
  P.choice
    [ TypeDef <$> parseTypeDef
    , TermDef <$> parseTermDef
    ]
