{-# LANGUAGE OverloadedStrings #-}

-- | Utilities for Giml parsers
module Language.Giml.Syntax.Parse.Common where

import Data.List.NonEmpty qualified as NonEmpty
import Data.Set qualified as Set
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.TokenStream
import Language.Giml.Utils
import Text.Megaparsec qualified as P

type Parser = P.ParsecT Void TokenStream (Reader Word64)

type ParseErr = P.ParseErrorBundle TokenStream Void

type Ann = P.SourcePos

data Annotated ann a = Annotated
  { getThing :: a
  , getAnn :: ann
  }
  deriving (Functor, Show)

addLoc :: (t -> Expr ann) -> Annotated ann t -> Annotated ann (Expr ann)
addLoc f (Annotated e loc) = Annotated (EAnnotated loc $ f e) loc

addLoc' :: (t -> Expr ann) -> Annotated ann t -> (Expr ann)
addLoc' f (Annotated e loc) = EAnnotated loc $ f e

chainLoc :: Int -> Loc -> Loc -> Loc
chainLoc len aLoc bLoc =
  Loc
    { locStart = locStart aLoc
    , locEnd = locEnd bLoc
    , locLength = locLength aLoc + len + 2 + locLength bLoc
    }

failure :: [Token] -> [Token] -> Parser a
failure item =
  P.failure
    (Just $ P.Tokens $ NonEmpty.fromList item)
    . Set.fromList
    . map (P.Tokens . NonEmpty.singleton)
