{-# LANGUAGE OverloadedStrings #-}

-- | Parser of Giml expressions
module Language.Giml.Syntax.Parse.Expr where

import Control.Monad.Combinators.Expr qualified as P
import Data.Map qualified as M
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Operators
import Language.Giml.Syntax.Parse.Token
import Language.Giml.Syntax.Parse.Type
import Language.Giml.Utils
import Text.Megaparsec qualified as P

-- * Parser implementation

-- | An expression.
parseExpr :: Parser (Expr Loc)
parseExpr = do
  P.makeExprParser (commented [] parseApp) operators

-- | Function application.
parseApp :: Text -> Parser (Expr Loc)
parseApp _ =
  apply
    <$> (parseExpr1 P.<?> "expression")
    <*> P.many (indented parseExpr1)

parseExpr1 :: Parser (Expr Loc)
parseExpr1 = do
  start <- P.getSourcePos
  expr <- parseExpr2
  mlbl <- P.optional (parseDot *> (parseIdent `P.sepBy` parseDot))
  end <- P.getSourcePos
  let
    loc = Loc start end 0
  pure $
    maybe
      expr
      (EAnnotated loc . foldl' ERecordAccess expr)
      (map (L . getThing) <$> mlbl)

-- | A basic expression without function application.
parseExpr2 :: Parser (Expr Loc)
parseExpr2 = do
  P.choice
    [ addLoc' ELit <$> parseLit P.<?> "literal"
    , addLoc' EVar <$> parseIdent P.<?> "identifier"
    , --    , addLoc EOp <$> parseOp P.<?> "operator"
      parseFun P.<?> "function"
    , parseCaseExpr P.<?> "case expression"
    , do
        Annotated (record, ext) loc <- parseRecord parseExpr (Just parseExpr) P.<?> "record"
        let
          record' = M.fromList . reverse $ record
        pure $
          maybe
            (EAnnotated loc (ERecord record'))
            (EAnnotated loc . ERecordExtension record')
            ext
    , addLoc' EVariant <$> dataCon P.<?> "variant"
    , addLoc' EOpenVariant <$> hashedCon P.<?> "open variant"
    , addLoc' EBlock <$> parseStatements P.<?> "do"
    , parseLetInExpr P.<?> "let expression"
    , parseFfiExpr P.<?> "FFI"
    , parens parseExpr
    ]

-- | Anonymous functions.
parseFun :: Parser (Expr Loc)
parseFun = do
  (Annotated _ lamloc) <- token "lambda" tlambda
  args <- P.some (indented funArg)
  parseArrow
  body <- indented parseExpr
  let
    bodyLoc = getExprAnn body
    loc =
      Loc
        (locStart lamloc)
        (locEnd bodyLoc)
        (subtractCol (locEnd bodyLoc) (locStart lamloc))
  pure $ EAnnotated loc $ EFun args body
  where
    tlambda t =
      case t of
        Lambda -> Just ()
        _ -> Nothing

-- | Case expressions.
parseCaseExpr :: Parser (Expr Loc)
parseCaseExpr = do
  (Annotated _ caseloc) <- token "case" tcase
  expr <- indented parseExpr
  _ <- token "of" tof
  patterns <- P.many $ indented parsePatternMatch
  endPos <- P.getSourcePos
  let
    loc =
      Loc
        (locStart caseloc)
        endPos
        (subtractCol endPos (locStart caseloc))
  pure $ EAnnotated loc $ ECase expr patterns
  where
    tcase t =
      case t of
        Case -> Just ()
        _ -> Nothing
    tof t =
      case t of
        Of -> Just ()
        _ -> Nothing

-- | A single pattern match.
parsePatternMatch :: Parser (Pattern, Expr Loc)
parsePatternMatch = do
  parseBar
  pat <- indented parsePattern
  parseArrow
  expr <- indented parseExpr
  pure (pat, expr)

-- | A pattern.
parsePattern :: Parser Pattern
parsePattern =
  P.choice
    [ PWildcard <$ parseUnderscore
    , PVar . getThing <$> parseIdent
    , PLit . getThing <$> parseLit
    , PVariant <$> parseVariantPat
    , POpenVariant <$> parseOpenVariantPat
    , PRecord . M.fromList . reverse . fst . getThing
        <$> parseRecord parsePattern (Nothing :: Maybe (Parser ()))
    ]

-- | A variant pattern.
parseVariantPat :: Parser (Variant (Maybe Pattern))
parseVariantPat = do
  (Annotated con _) <- dataCon
  typ <- P.optional (indented parsePattern)
  pure (Variant con typ)

-- | A polymorphic variant pattern.
parseOpenVariantPat :: Parser (Variant Pattern)
parseOpenVariantPat = do
  (Annotated con _) <- hashedCon
  typ <- indented parsePattern
  pure (Variant con typ)

-- | Do notation.
parseStatements :: Parser (Annotated Loc (Block Loc))
parseStatements = do
  start <- P.getSourcePos
  parseDo
  stmts <-
    P.some $ indented do
      P.choice
        [ do
            -- <var> <- <expr>
            var <- getThing <$> P.try (parseIdent <* parseLeftArrow)
            mk2 SBind var (indented parseExpr)
        , do
            -- let <var> = <expr>
            start' <- P.getSourcePos
            parseLet
            termdef <- indented parseTermDef
            end' <- P.getSourcePos
            let
              loc = Loc start' end' 0
            pure (SDef loc termdef)
        , SExpr `mk` parseExpr -- <expr>
        ]
  end <- P.getSourcePos
  let
    loc = Loc start end 0
  pure $ Annotated stmts loc
  where
    mk f x' = (\x -> f (getExprAnn x) x) <$> x'
    mk2 f a x' = (\x -> f (getExprAnn x) a x) <$> x'

-- | FFI.
parseFfiExpr :: Parser (Expr Loc)
parseFfiExpr = do
  start <- P.getSourcePos
  parseFfi
  parens do
    (name, typ) <- do
      name <- parseStr
      typ <- P.optional $ indented do
        parseColon
        parseType
      pure (name, typ)
    exprs <- P.many (P.try (indentation *> P.notFollowedBy parenClose) *> parseComma *> parseExpr)
    end <- P.getSourcePos
    let
      loc = Loc start end 0
    pure $ EAnnotated loc (EFfi (getThing name) (getThing <$> typ) exprs)

-- | let ... in expression.
parseLetInExpr :: Parser (Expr Loc)
parseLetInExpr = do
  start <- P.getSourcePos
  parseLet
  termdef <- indented parseTermDef
  indentation *> parseIn
  body <- indented parseExpr
  end <- P.getSourcePos
  let
    loc = Loc start end 0
  pure $ EAnnotated loc (ELet termdef body)

-- | Term definition.
parseTermDef :: Parser (TermDef Loc)
parseTermDef = do
  Annotated name loc <- parseIdent
  mtyp <- do
    P.optional (indented parseColon) >>= \case
      Nothing -> pure Nothing
      Just {} -> do
        typ <- parseType
        indentation
        Annotated name' loc' <- parseIdent
        when (name /= name') do
          failure
            [Token (Identifier name') loc']
            [Token (Identifier name) loc]
        pure (Just (getThing typ))
  vars <- P.many funArg
  parseEquals
  indented do
    indentation
    expr <- parseExpr
    pure case vars of
      [] -> Variable loc name mtyp expr
      args -> Function loc name mtyp args expr
