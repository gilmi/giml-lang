{-# LANGUAGE OverloadedStrings #-}

-- | Parsers for Giml operators
module Language.Giml.Syntax.Parse.Operators where

import Control.Monad.Combinators.Expr qualified as P
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Lexer hiding (Parser)
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Token
import Language.Giml.Utils
import Text.Megaparsec (try)

-- * Operators

operators :: [[P.Operator Parser (Expr Loc)]]
operators =
  [
    [ mkOpR "<." "composeLeft" 9
    , mkOpL ".>" "composeRight" 9
    ]
  ,
    [ mkOpL "*" "multiply" 7
    , mkOpL "/" "divide" 7
    ]
  ,
    [ mkOpL "+" "add" 6
    , mkOpL "-" "subtract" 6
    ]
  ,
    [ mkOpL "==" "equals" 4
    , mkOpL "!=" "notEquals" 4
    ]
  ,
    [ mkOpR "<|" "composeLeft" 0
    , mkOpL "|>" "composeRight" 0
    ]
  ]

mkOpL :: Var -> Var -> Word8 -> P.Operator Parser (Expr Loc)
mkOpL op name prec =
  P.InfixL $ do
    ann <- getAnn <$> try (indentation *> parseOp op)
    pure $ \a b ->
      apply (EAnnotated ann $ EOp (OpDef op name InfixLeft prec)) [a, b]

mkOpR :: Var -> Var -> Word8 -> P.Operator Parser (Expr Loc)
mkOpR op name prec =
  P.InfixR $ do
    ann <- getAnn <$> try (indentation *> parseOp op)
    pure $ \a b ->
      apply (EAnnotated ann $ EOp (OpDef op name InfixLeft prec)) [a, b]

parseOp :: Var -> Parser (Annotated Loc Var)
parseOp v =
  token "operator" top
  where
    top t =
      case t of
        Op o
          | v == o -> Just o
        _ -> Nothing

apply :: Expr Loc -> [Expr Loc] -> Expr Loc
apply e1 es
  | null es = e1
  | otherwise =
      let
        locStart = getExprAnn e1
        Loc {locEnd = end} = getExprAnn (last es)
      in
        EAnnotated
          (locStart {locEnd = end})
          (EFunCall e1 es)
