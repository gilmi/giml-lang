{-# LANGUAGE OverloadedStrings #-}

-- | Parser of Giml programs
module Language.Giml.Syntax.Parser (
  module Language.Giml.Syntax.Parse.Common,
  module Language.Giml.Syntax.Parse.Token,
  module Language.Giml.Syntax.Parse.Expr,
  module Language.Giml.Syntax.Parse.Type,
  module Language.Giml.Syntax.Parse.API,
  module Language.Giml.Syntax.Parse.Definitions,
  module Language.Giml.Syntax.Parser,
  P.SourcePos (..),
  P.Pos,
  P.mkPos,
  P.unPos,
) where

import Language.Giml.Syntax.Parse.API
import Language.Giml.Syntax.Parse.Common
import Language.Giml.Syntax.Parse.Definitions
import Language.Giml.Syntax.Parse.Expr
import Language.Giml.Syntax.Parse.Token
import Language.Giml.Syntax.Parse.Type
import Text.Megaparsec qualified as P

dummyAnn :: FilePath -> P.SourcePos
dummyAnn fname = P.SourcePos fname (P.mkPos 1) (P.mkPos 1)
