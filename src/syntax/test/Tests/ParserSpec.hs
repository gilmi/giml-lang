{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Tests.ParserSpec where

import Control.Monad.Except
import Data.Data (Data)
import Data.Generics.Uniplate.Data
import Data.Map qualified as M
import Data.Text qualified as T
import Language.Giml.Logging
import Language.Giml.Syntax.Ast
import Language.Giml.Syntax.Parser
import Test.Hspec
import Text.RawString.QQ

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "parser" $ do
    lits
    records
    expressions
    programs

lits :: Spec
lits = do
  describe "literals" $ do
    numbers
    strings

numbers :: Spec
numbers = do
  describe "numbers" $ do
    it "0" $
      shouldBe
        (testParserNoLoc parseLit "0")
        (pure $ LInt 0)

    it "11717" $
      shouldBe
        (testParserNoLoc parseLit "11717")
        (pure $ LInt 11717)

    it "-11" $
      shouldBe
        (testParserNoLoc parseLit "-11")
        (pure $ LInt (-11))

    it "0x15" $
      shouldBe
        (testParserNoLoc parseLit "0x15")
        (pure $ LInt 21)

    it "-0xff" $
      shouldBe
        (testParserNoLoc parseLit "-0xff")
        (pure $ LInt (-255))

    it "-0b101" $
      shouldBe
        (testParserNoLoc parseLit "-0b101")
        (pure $ LInt (-5))

    it "0b1010" $
      shouldBe
        (testParserNoLoc parseLit "0b1010")
        (pure $ LInt 10)

strings :: Spec
strings = do
  describe "strings" $ do
    it "empty string" $
      shouldBe
        (testParserNoLoc parseLit [r|""|])
        (pure $ LString "")

    it "hello world" $
      shouldBe
        (testParserNoLoc parseLit [r|"hello world"|])
        (pure $ LString "hello world")

    it "quotes" $
      shouldBe
        (testParserNoLoc parseLit [r|"\"hello world\""|])
        (pure $ LString "\"hello world\"")

    it "newlines" $
      shouldBe
        (testParserNoLoc parseLit [r|"hello\nworld"|])
        (pure $ LString "hello\nworld")

records :: Spec
records = do
  describe "records" $ do
    it "{}" $
      shouldBe
        (testParserNoAnn parseExpr "{}")
        (pure $ ERecord mempty)

    it "{} with spaces" $
      shouldBe
        (testParserNoAnn parseExpr "{    \n }")
        (pure $ ERecord mempty)

    it "x, y" $
      shouldBe
        ( testParserNoAnn
            parseExpr
            [r|{ x : "hello", y : -1 }|]
        )
        ( pure $
            ERecord
              ( M.fromList
                  [ ("x", ELit $ LString "hello")
                  , ("y", ELit $ LInt (-1))
                  ]
              )
        )

    it "x, y with newlines" $
      shouldBe
        ( testParserNoAnn
            parseExpr
            [r|{ x : "hello"
, y : -1
}|]
        )
        ( pure $
            ERecord
              ( M.fromList
                  [ ("x", ELit $ LString "hello")
                  , ("y", ELit $ LInt (-1))
                  ]
              )
        )

    it "duplicate labels" $
      shouldBe
        ( testParserNoAnn
            parseExpr
            [r|{ a : "hello", a : 2, a : -1 } |]
        )
        ( pure
            ( ERecord $
                M.fromList
                  [ ("a", ELit $ LString "hello")
                  ]
            )
        )

expressions :: Spec
expressions = do
  describe "expressions" $ do
    vars
    recordsExpr
    functions
    cases
    letexprs

vars :: Spec
vars = do
  describe "variables" $ do
    it "single letter" $
      shouldBe
        (testParserNoAnn parseExpr "v")
        (pure $ EVar "v")

    it "multiple mixed letters" $
      shouldBe
        (testParserNoAnn parseExpr "vAcBkFx")
        (pure $ EVar "vAcBkFx")

    it "with numbers" $
      shouldBe
        (testParserNoAnn parseExpr "v123")
        (pure $ EVar "v123")

    it "with symbols" $
      shouldBe
        (testParserNoAnn parseExpr "v_123")
        (pure $ EVar "v_123")

recordsExpr :: Spec
recordsExpr = do
  describe "records" $ do
    it "one item" $
      shouldBe
        (testParserNoAnn parseExpr "{ a : f x }")
        ( pure $
            mkERec
              [("a", EFunCall (EVar "f") [EVar "x"])]
              Nothing
        )

    it "two items" $
      shouldBe
        (testParserNoAnn parseExpr [r|{ a : f x, b : "hello" }|])
        ( pure $
            mkERec
              [ ("a", EFunCall (EVar "f") [EVar "x"])
              , ("b", ELit $ LString "hello")
              ]
              Nothing
        )

    it "extension" $
      shouldBe
        (testParserNoAnn parseExpr [r|{ a : f x, b : "hello" | r }|])
        ( pure $
            mkERec
              [ ("a", EFunCall (EVar "f") [EVar "x"])
              , ("b", ELit $ LString "hello")
              ]
              (Just $ EVar "r")
        )

    it "access" $
      shouldBe
        (testParserNoAnn parseExpr [r|record.x.y.z|])
        ( pure $
            ERecordAccess
              ( ERecordAccess
                  (ERecordAccess (EVar "record") "x")
                  "y"
              )
              "z"
        )

functions :: Spec
functions = do
  describe "functions" $ do
    it "f x" $
      shouldBe
        (testParserNoAnn parseExpr "f x")
        ( pure $ EFunCall (EVar "f") [EVar "x"]
        )

    it "f x  y z" $
      shouldBe
        (testParserNoAnn parseExpr "f x  y z")
        ( pure $ EFunCall (EVar "f") [EVar "x", EVar "y", EVar "z"]
        )

    it "(\\x y -> add x  y) 1 2" $
      shouldBe
        (testParserNoAnn parseExpr "(\\x y -> add x  y) 1 2")
        ( pure $
            EFunCall
              ( EFun [Just "x", Just "y"] $ EFunCall (EVar "add") [EVar "x", EVar "y"]
              )
              [ELit $ LInt 1, ELit $ LInt 2]
        )

    it "wildcard funarg" $
      shouldBe
        (testParserNoAnn parseExpr [r|(\x _ -> x) 1 2|])
        ( pure $
            EFunCall
              ( EFun [Just "x", Nothing] (EVar "x")
              )
              [ELit $ LInt 1, ELit $ LInt 2]
        )

cases :: Spec
cases = do
  describe "cases of" $ do
    it "simple" $
      shouldBe
        (testParserNoAnn parseExpr "case x of | 1 -> 1")
        ( pure $
            ECase
              (EVar "x")
              [(PLit $ LInt 1, ELit $ LInt 1)]
        )

    it "mutlicase" $
      shouldBe
        ( testParserNoAnn
            parseExpr
            [r|case x of
	| Constr -> {}
	| Constr {} -> {}
	| Constr { a : 1 } -> 1
	| v -> v
	| _ -> x == y|]
        )
        ( pure $
            ECase
              (EVar "x")
              [
                ( PVariant (Variant "Constr" $ Nothing)
                , ERecord mempty
                )
              ,
                ( PVariant (Variant "Constr" $ Just $ PRecord mempty)
                , ERecord mempty
                )
              ,
                ( PVariant
                    ( Variant "Constr" $
                        Just $
                          PRecord $
                            M.fromList [("a", PLit $ LInt 1)]
                    )
                , ELit (LInt 1)
                )
              , (PVar "v", EVar "v")
              ,
                ( PWildcard
                , EFunCall
                    ( EOp
                        ( OpDef
                            { opName = "=="
                            , opFunRef = "equals"
                            , opFixity = InfixLeft
                            , opPrecedence = 4
                            }
                        )
                    )
                    [EVar "x", EVar "y"]
                )
              ]
        )

letexprs :: Spec
letexprs = do
  describe "let in" $ do
    it "variable" $
      shouldBe
        (testParserNoAnn parseExpr "let x = 1 in x")
        ( pure $
            ELet
              (Variable () "x" Nothing (ELit $ LInt 1))
              (EVar "x")
        )

    it "function" $
      shouldBe
        (testParserNoAnn parseExpr "let eq x y = x == y in eq 1 2")
        ( pure $
            ELet
              ( Function
                  ()
                  "eq"
                  Nothing
                  [Just "x", Just "y"]
                  ( EFunCall
                      (EOp (OpDef "==" "equals" InfixLeft 4))
                      [EVar "x", EVar "y"]
                  )
              )
              (EFunCall (EVar "eq") [ELit $ LInt 1, ELit $ LInt 2])
        )

    it "function with type and indentation" $
      shouldBe
        ( testParserNoAnn
            parseExpr
            [r|let
	eq : Int -> Int -> Int
	eq x y =
		x == y
in
	eq 1 2|]
        )
        ( pure $
            ELet
              ( Function
                  ()
                  "eq"
                  ( Just
                      ( TypeApp
                          (TypeApp (TypeCon "->") (TypeCon "Int"))
                          (TypeApp (TypeApp (TypeCon "->") (TypeCon "Int")) (TypeCon "Int"))
                      )
                  )
                  [Just "x", Just "y"]
                  ( EFunCall
                      (EOp (OpDef "==" "equals" InfixLeft 4))
                      [EVar "x", EVar "y"]
                  )
              )
              (EFunCall (EVar "eq") [ELit $ LInt 1, ELit $ LInt 2])
        )

programs :: Spec
programs = do
  describe "programs" $ do
    it "length" $
      shouldBe
        ( testParserNoAnn
            parseFile
            [r|
List a =
	| Nil
	| Cons { head : a, tail : List a }

length : List a -> Int
length xs =
	case xs of
		| Nil -> 0
		| Cons { head : _, tail : rest } ->
			add 1 (length rest)

main = do
	ffi("console.log" : Int -> IO {}, length (Cons { head : 1, tail : Cons { head : 2, tail : Nil } }))
|]
        )
        ( pure $
            ParsedFile
              [ TypeDef
                  ( Datatype
                      ()
                      "List"
                      Nothing
                      ["a"]
                      [ Variant "Nil" Nothing
                      , Variant "Cons" $
                          Just $
                            TypeRec
                              [ ("head", TypeVar "a")
                              , ("tail", TypeApp (TypeCon "List") (TypeVar "a"))
                              ]
                      ]
                  )
              , TermDef
                  ( Function
                      ()
                      "length"
                      ( Just
                          ( TypeApp
                              (TypeApp (TypeCon (TC {unTypeCon = "->"})) (TypeApp (TypeCon (TC {unTypeCon = "List"})) (TypeVar (TV {unTypeVar = "a"}))))
                              (TypeCon (TC {unTypeCon = "Int"}))
                          )
                      )
                      [Just "xs"]
                      ( ECase
                          (EVar "xs")
                          [
                            ( PVariant (Variant "Nil" Nothing)
                            , ELit (LInt 0)
                            )
                          ,
                            ( PVariant $
                                Variant "Cons" $
                                  Just
                                    ( PRecord $
                                        M.fromList
                                          [ ("head", PWildcard)
                                          , ("tail", PVar "rest")
                                          ]
                                    )
                            , EFunCall
                                (EVar "add")
                                [ ELit (LInt 1)
                                , EFunCall (EVar "length") [EVar "rest"]
                                ]
                            )
                          ]
                      )
                  )
              , TermDef $
                  Variable () "main" Nothing $
                    EBlock
                      [ SExpr
                          ()
                          ( EFfi
                              "console.log"
                              ( Just (TypeApp (TypeApp (TypeCon (TC {unTypeCon = "->"})) (TypeCon (TC {unTypeCon = "Int"}))) (TypeApp (TypeCon (TC {unTypeCon = "IO"})) (TypeRec [])))
                              )
                              [ EFunCall
                                  (EVar "length")
                                  [ EFunCall
                                      (EVariant "Cons")
                                      [ ERecord $
                                          M.fromList
                                            [ ("head", ELit (LInt 1))
                                            ,
                                              ( "tail"
                                              , EFunCall
                                                  (EVariant "Cons")
                                                  [ ERecord $
                                                      M.fromList
                                                        [ ("head", ELit (LInt 2))
                                                        , ("tail", EVariant "Nil")
                                                        ]
                                                  ]
                                              )
                                            ]
                                      ]
                                  ]
                              ]
                          )
                      ]
              ]
        )

----------------------

testParserNoLoc :: Parser (Annotated x a) -> T.Text -> Either T.Text a
testParserNoLoc p = fmap getThing . testParser p

testParser :: Parser a -> T.Text -> Either T.Text a
testParser p src =
  runExcept . runLog noLogging $ runParser p "test" src

testParserNoAnn :: (Functor f) => (Data (f ())) => Parser (f a) -> T.Text -> Either T.Text (f ())
testParserNoAnn p src =
  ann . fmap (const ()) <$> testParser p src

ann :: (Data (f a)) => f a -> f a
ann = transformBi \case
  EAnnotated _ e -> e
  (e :: Expr ()) -> e

{-
import Data.Text qualified as T

example :: T.Text
example = T.unlines
  [ "length : List a -> Int"
  , "length xs ="
  , "\tcase xs of"
  , "\t\t| Nil -> 0"
  , "\t\t| Cons { head : _, tail : rest } ->"
  , "\t\t\tlength rest + 1"
  ]
-}
