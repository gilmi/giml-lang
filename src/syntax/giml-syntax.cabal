cabal-version: 3.4
name: giml-syntax
version: 0.1.0.0
synopsis: Syntax for Giml - A purely functional programming language with emphasis on structural typing
description: Giml is a strict, statically typed, purely functional programming language
             with first-class functions, extensible records, ADTs, pattern matching, and more.

             Giml is developed live on stream.

             Visit the website for more information: https://giml-lang.org
homepage: https://giml-lang.org
license: Apache-2.0
license-file: LICENSE
author: Gil Mizrahi
maintainer: gil@gilmi.net
copyright: 2023 Gil Mizrahi
category: Language
build-type: Simple

library
  hs-source-dirs: src
  exposed-modules:
      Language.Giml.Common
      Language.Giml.Pretty

      Language.Giml.Syntax.Ast
      Language.Giml.Syntax.Lexer
        Language.Giml.Syntax.Parse.Common
        Language.Giml.Syntax.Parse.Definitions
        Language.Giml.Syntax.Parse.Token
        Language.Giml.Syntax.Parse.Expr
        Language.Giml.Syntax.Parse.Type
        Language.Giml.Syntax.Parse.API
        Language.Giml.Syntax.Parse.Operators
      Language.Giml.Syntax.TokenStream
      Language.Giml.Syntax.Types
      Language.Giml.Syntax.Parser
  build-depends:
      base >= 4.7 && < 5
    , containers
    , deepseq
    , text
    , pretty-simple
    , mtl
    , prettyprinter
    , megaparsec
    , parser-combinators
    , uniplate
    , optparse-generic
    , co-log-core
    , generic-records
    , transformers-base
    , giml-utils

  default-extensions:
    LambdaCase
    MonoLocalBinds
    BlockArguments
  default-language: GHC2021
  ghc-options: -O -Wall

test-suite giml-syntax-test
  type: exitcode-stdio-1.0
  hs-source-dirs: test
  main-is: Spec.hs
  other-modules:
    Tests.ParserSpec
  build-depends:
      base
    , giml-utils
    , giml-syntax
    , text
    , containers
    , mtl
    , hspec
    , QuickCheck
    , process
    , raw-strings-qq
    , uniplate
  default-language: GHC2021
  default-extensions:
    LambdaCase
    MonoLocalBinds
    BlockArguments
  ghc-options: -O -threaded -rtsopts -with-rtsopts=-N
  build-tool-depends: hspec-discover:hspec-discover == 2.*

source-repository head
  type: git
  location: https://gitlab.com/gilmi/giml-lang
