{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}

-- | Command-line interface for gimlc
module Language.Giml.Compiler.Run where

import Control.Monad.Except
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Language.Giml qualified as Giml
import Language.Giml.Compiler.Compile
import Language.Giml.Compiler.REPL
import Language.Giml.Utils
import Options.Generic
import System.Exit
import System.IO

-- | gimlc cli interface type
data Command w
  = Compile
      { input :: w ::: FilePath <?> "input file"
      , output :: w ::: Maybe FilePath <?> "output file"
      , warn :: w ::: Bool <?> "emit warnings"
      }
  | Parse
      { input :: w ::: FilePath <?> "input file"
      , output :: w ::: Maybe FilePath <?> "output file"
      }
  | Infer
      { input :: w ::: FilePath <?> "input file"
      , output :: w ::: Maybe FilePath <?> "output file"
      , warn :: w ::: Bool <?> "emit warnings"
      }
  | Interactive
      { load :: w ::: Maybe FilePath <?> "input file"
      }
  deriving (Generic)

instance ParseRecord (Command Wrapped)

deriving instance Show (Command Unwrapped)

-- | Reads command-line arguments and acts accordingly
run :: IO ()
run = do
  arguments <- unwrapRecord "gimlc"
  case arguments of
    Compile inputFile outputFile w -> do
      process
        (compile $ logact inputFile (if w then Just Giml.Warning else Nothing))
        inputFile
        outputFile
    Parse inputFile outputFile -> do
      process
        ( \i o ->
            pure
              . fmap pShow
              . Giml.runWithoutLogger
              $ Giml.parse i o
        )
        inputFile
        outputFile
    Infer inputFile outputFile w -> do
      process
        ( \i o -> do
            runExceptT . fmap (pShow . fmap Giml.printAnn) $
              Giml.parseInferPipeline (logact i $ if w then Just Giml.Warning else Nothing) i o
        )
        inputFile
        outputFile
    Interactive mInputFile -> do
      runRepl mInputFile

logact :: FilePath -> Maybe Giml.Verbosity -> Giml.LogAction IO Giml.LogMsg
logact i =
  maybe
    Giml.noLoggingIO
    (Giml.logAction . flip Giml.mkCompileInfoIO i)

-- | Takes a processing function, an input file and optionally an output file
process
  :: (FilePath -> T.Text -> IO (Either T.Text T.Text))
  -> FilePath
  -> Maybe FilePath
  -> IO ()
process func inputFile outputFile = do
  file <- T.readFile inputFile
  result <- func inputFile file
  case result of
    Left err -> do
      T.hPutStrLn stderr err
      exitFailure
    Right prog -> do
      maybe T.putStrLn T.writeFile outputFile prog
