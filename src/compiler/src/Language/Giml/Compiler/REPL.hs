{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}

-- | REPL for Giml
module Language.Giml.Compiler.REPL where

import Control.Monad.Catch
import Control.Monad.Except
import Control.Monad.Reader
import Data.Generics.Uniplate.Data qualified as U
import Data.IORef
import Data.List
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Language.Giml qualified as Giml
import Language.Giml.Compiler.Compile
import Language.Giml.Syntax.Parser qualified as Parser
import Language.Giml.Types.Infer qualified as Infer
import Language.Giml.Utils
import System.Console.Haskeline qualified as H
import System.Directory (createDirectoryIfMissing, getHomeDirectory)
import System.IO
import System.Process (readProcessWithExitCode)

-- * Run

-- | Run a REPL, optionally load a source file
runRepl :: Maybe FilePath -> IO ()
runRepl mfile = do
  putStrLn "not implemented."

{-
  dir <- gimlHomeDir
  createDirectoryIfMissing False dir
  emptyEnv <- newEmptyEnv
  flip runReaderT emptyEnv . H.runInputT (settings dir) $ do
    H.outputStrLn "Gimli - interactive Giml mode"
    lift $ maybe (pure ()) loadFile mfile
    replWithHandler
  putStrLn "Goodbye!"

-- | Runs the repl while handling interrupts (Ctrl-C)
replWithHandler :: H.InputT Repl ()
replWithHandler =
    H.handleInterrupt
      ( do
        lift $ putCurrentText Nothing
        H.outputStrLn "Cancelled."
        replWithHandler
      )
      (H.withInterrupt repl)

-- | Read & process loop
repl :: H.InputT Repl ()
repl = do
  hasText <- isJust <$> lift getCurrentText
  let
    prompt
      | hasText = "λ| "
      | otherwise = "λ> "

  minput <- H.getInputLine prompt

  case minput of
      Nothing -> pure ()

      Just (':' : (splitCmd -> (cmd, rest)))
        | not hasText ->
          case lookup cmd replCommands of
            Just action ->
              action rest
            Nothing -> do
              H.outputStrLn $ "No such command: " <> cmd
              repl

      Just input -> do
        lift $ processInput input
        repl

-- * Utils

splitCmd :: String -> (String, String)
splitCmd str =
  (takeWhile (/=' ') str, trim $ dropWhile (/=' ') str)

trim :: String -> String
trim str =
  dropWhileEnd (== ' ') $ dropWhile (==' ') str

-- * Types

type Repl = ReaderT Env IO

type Env = IORef ReplState

data ReplState
  = ReplState
    { rsCurrentText :: Maybe Text
    , rsFile :: Giml.ParsedFile Giml.InputAnn
    , rsLogMode :: Maybe Giml.Verbosity
    }
  deriving Show

newEmptyEnv :: IO Env
newEmptyEnv = newIORef $ ReplState mempty (Giml.ParsedFile []) Nothing -- Just Giml.Detailed

-- * Repl command and processing

-- | Load a source file into environment
loadFile :: MonadReader Env m => MonadIO m => FilePath -> m ()
loadFile file = do
  loggerInfo <- getLoggerInfo file
  content <- liftIO $ T.readFile file
  parsed <-
    ( liftIO
    . runExceptT
    . withExceptT Giml.ParseError
    . Giml.runLog loggerInfo
    . Giml.parse file
    ) content
  case parsed of
    Left err -> do
      liftIO $ T.putStrLn $ Giml.errorToText err
      liftIO $ putStrLn $ "Failed loading file."
    Right parsed' -> do
      inferred <- liftIO $ runExceptT $ Giml.inferPipeline' loggerInfo parsed'
      case inferred of
        Left err -> do
          liftIO $ T.putStrLn $ Giml.errorToText err
          liftIO $ putStrLn $ "Failed loading file."
        Right _ -> do
          putFile parsed'
          liftIO $ putStrLn $ "File loaded successfully."

-- | Repl commands (start with @:@)
replCommands :: [(String, String -> H.InputT Repl ())]
replCommands =
  [ ("t"
    , \e -> do
      lift $ printTypeOfExpr e
      repl
    )
  , ("type"
    , \e -> do
      lift $ printTypeOfExpr e
      repl
    )
  , ( "load"
    , \e -> do
      catch
        (lift $ loadFile e)
        (\(SomeException ex) -> liftIO $ hPutStrLn stderr (show ex))
      repl
    )
  , ( "log"
    , \e -> do
      let
        mv = case T.toLower (T.pack e) of
          "none" -> Just $ Nothing
          "warning" -> Just $ Just Giml.Warning
          "concise" -> Just $ Just Giml.Concise
          "detailed" -> Just $ Just Giml.Detailed
          "general" -> Just $ Just Giml.General
          _ -> Nothing
      case mv of
        Nothing ->
          liftIO $ hPutStrLn stderr "Choose one of: none, warning, general, concise, detailed"
        Just v ->
          lift $ liftIO . flip modifyIORef (\s -> s { rsLogMode = v }) =<< ask
      repl
    )
  , ( "q"
    , \_ -> pure ()
    )
  ]

-- | Print the type of an expression command
printTypeOfExpr :: MonadReader Env m => MonadIO m => String -> m ()
printTypeOfExpr line = do
  loggerInfo <- getLoggerInfo "repl"
  case parseReplThing $ T.pack line of
    Right (Parser.EarlyEOF _) ->
      liftIO $ putStrLn "I can only handle single line expression."
    Right (Parser.ReplDefinition _) -> do
      liftIO $ putStrLn "{}"
    Right (Parser.ReplExpr expr) -> do
      Giml.ParsedFile defs <- getFile
      result' <- liftIO $ runExceptT $ Giml.inferPipeline' loggerInfo (mkProgram expr defs)
      case result' of
        Left err ->
          liftIO $ T.putStrLn $ Giml.errorToText err
        Right res' -> do
          case extractType res' of
            Just typ ->
              liftIO $ T.putStrLn $ Giml.printType typ
            Nothing ->
              liftIO $ putStrLn "Internal error."
    Left err -> do
      liftIO $ T.putStrLn err

-- | Process user input (read expressions, evaluate and print)
processInput :: MonadReader Env m => MonadIO m => String -> m ()
processInput line = do
  loggerInfo <- getLoggerInfo "repl"
  mcurr <- getCurrentText
  let
    curr = case mcurr of
      Nothing -> T.pack line
      Just txt -> txt <> "\n" <> T.pack line
    result = parseReplThing curr
  case result of
    Right (Parser.EarlyEOF txt) ->
      putCurrentText $ Just txt
    Right (Parser.ReplDefinition def) -> do
      putCurrentText Nothing
      Giml.ParsedFile defs <- getFile
      inferred <- liftIO $ runExceptT $
        Giml.inferPipeline' loggerInfo (Giml.ParsedFile (overridedef def defs))
      case inferred of
        Left err ->
          liftIO $ T.putStrLn $ Giml.errorToText err
        Right{} ->
          putFile $ Giml.ParsedFile (overridedef def defs)
    Right (Parser.ReplExpr expr) -> do
      putCurrentText Nothing
      Giml.ParsedFile defs <- getFile
      result' <- liftIO $ runExceptT $
        Giml.inferPipeline' loggerInfo (mkProgram expr defs)
      case result' of
        Left err ->
          liftIO $ T.putStrLn $ Giml.errorToText err
        Right res' -> do
          (_exitcode, out, err) <- liftIO $
            readProcessWithExitCode "nodejs" [] (T.unpack $ translate' $ joinIOForMain res')
          liftIO $ putStr out
          liftIO $ hPutStr stderr err
    Left err -> do
      putCurrentText Nothing
      liftIO $ T.putStrLn err

-- | Parse user input
parseReplThing :: Text -> Either Text (Parser.ReplResult Text)
parseReplThing line =
  first Parser.ppErrors $ Parser.runReplParser "repl" line

-- * Handle Repl State

-- | If a user is in the middle of writing multiline input,
--   This will return Just <text-until-now>.
getCurrentText :: MonadReader Env m => MonadIO m => m (Maybe Text)
getCurrentText =
  fmap rsCurrentText $ liftIO . readIORef =<< ask

-- | Set the input the user might be in the middle of writing (multiline input)
putCurrentText :: MonadReader Env m => MonadIO m => Maybe Text -> m ()
putCurrentText mtxt = do
  ref <- ask
  s <- liftIO $ readIORef ref
  liftIO $ writeIORef ref $ s { rsCurrentText = mtxt }

-- | Get the currently defined definitions in the environment
getLoggerInfo
  :: MonadReader Env m
  => MonadIO m
  => FilePath -> m (Giml.LogAction IO Giml.LogMsg)
getLoggerInfo file = do
  verbosity <- fmap rsLogMode $ liftIO . readIORef =<< ask
  pure $ Giml.logAction $ maybe Giml.mkCompileInfoIONoLogging (flip Giml.mkCompileInfoIO file) verbosity

-- | Get the currently defined definitions in the environment
getFile :: MonadReader Env m => MonadIO m => m (Giml.ParsedFile Giml.InputAnn)
getFile =
  fmap rsFile $ liftIO . readIORef =<< ask

-- | Put the currently defined definitions in the environment
putFile :: MonadReader Env m => MonadIO m => (Giml.ParsedFile Giml.InputAnn) -> m ()
putFile file = do
  ref <- ask
  s <- liftIO $ readIORef ref
  liftIO $ writeIORef ref $ s { rsFile = file }

-- * Parsed File manipulation

-- | Add a definition or override an existing definition
overridedef
  :: Giml.Definition Giml.InputAnn
  -> [Giml.Definition Giml.InputAnn]
  -> [Giml.Definition Giml.InputAnn]
overridedef def defs =
  def : filter (\d -> Giml.getDefName d /= Giml.getDefName def) defs

-- | @join@ the expression that needs to be printed if it's an IO expression
joinIOForMain :: Giml.File Giml.Ann -> Giml.File Giml.Ann
joinIOForMain (Giml.File typs defs) =
  Giml.File typs $
    map
      ( map $ \case
        Giml.Variable ann1 "main" (Giml.EAnnotated ann2 (Giml.EFfi f t [e])) ->
          Giml.Variable ann1 "main"
            ( Giml.EAnnotated ann2 $ Giml.EFfi f t
              [ case Giml.getType e of
                  Giml.TypeApp (Giml.TypeCon "IO") _ ->
                    Giml.EFunCall e []
                  _ -> e
              ]
            )
        x -> x
      )
    defs

-- | create a program from an expression and definitions
mkProgram
  :: Giml.Expr Giml.InputAnn
  -> [Giml.Definition Giml.InputAnn]
  -> Giml.ParsedFile Giml.InputAnn
mkProgram expr defs =
  Giml.ParsedFile $
    ( Giml.TermDef
      ( Giml.Variable
        (Giml.getExprAnn expr)
        "main"
        ( Giml.EAnnotated (Giml.getExprAnn expr) $
          Giml.EFfi "console.log" Nothing [renameMainVars expr]
        )
      )
    : renameMainVars (renameMainDef defs)
    )

-- | Extract the type of the expression in the REPL.
--   This should generally always work.
extractType :: Giml.File Giml.Ann -> Maybe Giml.Type
extractType (Giml.File _ defs) =
  ( fmap Infer.generalizeAndClose
  . listToMaybe
  . map
    ( \case
      Giml.Variable _ "main" (Giml.EAnnotated _ (Giml.EFfi _ _ [e])) -> Giml.getType e
      _ -> undefined
    )
  . filter
    ( \case
      Giml.Variable _ "main" _ -> True
      _ -> False
    )
  . concat
  ) defs

-- | We want to preserve the main of a loaded file in case the user want to call it.
--   This function will change "main" to "_main" in all __expressions__.
renameMainVars :: Data a => a -> a
renameMainVars = U.transformBi $ \case
  Giml.EVar "main" -> Giml.EVar "_main"
  e -> (e :: Giml.Expr Giml.InputAnn)

-- | We want to preserve the main of a loaded file in case the user want to call it
--   This function will change the name of the __definition__ "main" to "_main".
renameMainDef :: [Giml.Definition Giml.InputAnn] -> [Giml.Definition Giml.InputAnn]
renameMainDef = map $ \case
  Giml.TermDef (Giml.Variable ann "main" v) ->
    Giml.TermDef (Giml.Variable ann "_main" v)
  x -> x

-- * Settings

settings :: MonadReader Env m => MonadIO m => FilePath -> H.Settings m
settings dir = do
  H.Settings
    { H.complete =
      H.completeWordWithPrev Nothing " \t\n"
        ( \(trim -> reverse -> left) word ->
          case left of
            ":load" ->
              H.listFiles word

            ":log" ->
              pure (map H.simpleCompletion ["none", "warning", "general", "concise", "detailed"])

            []
              | (':' : cmd) <- word ->
                ( pure
                . map (H.simpleCompletion . (':' :))
                . filter (cmd `isPrefixOf`)
                . map fst
                ) replCommands

            _ -> do
              Giml.ParsedFile defs <- getFile
              pure
                . map H.simpleCompletion
                . filter (word `isPrefixOf`)
                . map toString
                . concatMap
                  ( \case
                    Giml.TermDef (Giml.Variable _ name _) -> [name]
                    Giml.TermDef (Giml.Function _ name _ _) -> [name]
                    Giml.TypeDef (Giml.Datatype _ _ _ variants) ->
                      map (\(Giml.Variant (Giml.C v) _) -> v) variants
                  )
                $ defs
        )

    , H.historyFile = Just $ dir <> ".history"
    , H.autoAddHistory = True
    }

gimlHomeDir :: MonadIO m => m FilePath
gimlHomeDir = (<> "/.giml/") <$> liftIO getHomeDirectory

-}
