-- | Compilation runner
module Language.Giml.Compiler.Compile where

import Control.Monad.Except
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Language.Backend.JS qualified as JS
import Language.Giml qualified as Giml
import Language.Giml.Compiler.Translate

-- | Compile a Giml source file to JS
compile
  :: (Giml.MonadBase b b)
  => Giml.LogAction b Giml.LogMsg
  -- ^ logging
  -> FilePath
  -- ^ Name of the source file
  -> T.Text
  -- ^ Content of the source file
  -> b (Either T.Text T.Text)
compile logact file src = runExceptT $ do
  ast <- Giml.parseInferPipeline logact file src
  pure $
    ( JS.pp JS.ppFile
        . translate translateFile Giml.builtins
    )
      ast

translate' :: Giml.File Giml.Ann -> T.Text
translate' = JS.pp JS.ppFile . translate translateFile Giml.builtins

translate'IO :: Giml.File Giml.Ann -> IO ()
translate'IO = T.putStrLn . translate'
