{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}

-- | Translate Giml to JavaScript
module Language.Giml.Compiler.Translate where

import Control.Monad.State
import Data.Map qualified as M
import Data.Text qualified as T
import Language.Backend.JS qualified as JS
import Language.Giml.Builtins
import Language.Giml.Syntax.Ast
import Language.Giml.Types.Infer (Ann (..))
import Language.Giml.Utils

-- Types and utilities --

type TranState = Int

type Translate m =
  ( MonadState TranState m
  , MonadReader Builtins m
  )

genVar :: (Translate m) => T.Text -> m Var
genVar prefix = do
  n <- get
  modify (+ 1)
  pure ("_" <> prefix <> "_" <> T.pack (show n))

translate :: (a -> StateT TranState (Reader Builtins) b) -> Builtins -> a -> b
translate tran built =
  ( flip runReader built
      . flip evalStateT 0
      . tran
  )

-- Translation --

translateFile :: (Translate m) => File Ann -> m JS.File
translateFile (File _typedefs termdefs) = do
  let
    -- we don't need to compile data type definitions
    defs = concat termdefs

  fmap JS.File $
    (<>)
      <$> traverse (fmap JS.SDef . translateDef) defs
      <*> pure
        [ JS.SExpr $ JS.EFunCall (JS.EVar "main") []
        | hasMain defs
        ]

hasMain :: [TermDef Ann] -> Bool
hasMain =
  any
    ( \case
        Variable _ "main" _ _ ->
          True
        _ ->
          False
    )

translateDef :: (Translate m) => TermDef Ann -> m JS.Definition
translateDef = \case
  Variable _ var _ expr ->
    JS.Variable var <$> translateExpr expr
  Function _ var _ args body ->
    JS.Function var (map (fromMaybe "_") args) . pure . JS.SRet <$> translateExpr body

translateBlock :: (Translate m) => Block Ann -> m JS.Block
translateBlock stmts =
  case reverse stmts of
    [] -> pure []
    SExpr _ expr : rest ->
      fmap reverse $
        (:)
          <$> (JS.SRet <$> translateExpr (EFunCall expr []))
          <*> traverse translateStmt rest
    _ ->
      traverse translateStmt stmts

translateStmt :: (Translate m) => Statement Ann -> m JS.Statement
translateStmt = \case
  SExpr _ expr ->
    JS.SExpr <$> translateExpr (EFunCall expr [])
  SDef _ def ->
    JS.SDef <$> translateDef def
  SBind ann name expr -> do
    JS.SDef <$> translateDef (Variable ann name Nothing (EFunCall expr []))

translateExpr :: (Translate m) => Expr Ann -> m JS.Expr
translateExpr = \case
  ELit lit ->
    pure $ JS.ELit (translateLit lit)
  EVar var -> do
    mbuiltin <- asks (M.lookup var)
    case mbuiltin of
      Nothing ->
        pure $ JS.EVar var
      Just Builtin {bImpl = impl} ->
        case impl of
          Func fun ->
            pure $ JS.ERaw fun
          BinOp op ->
            pure $
              JS.EFun
                ["x"]
                [ JS.SRet $
                    JS.EFun
                      ["y"]
                      [JS.SRet (JS.EBinOp op (JS.EVar "x") (JS.EVar "y"))]
                ]
  EOp OpDef {opFunRef} ->
    translateExpr (EVar opFunRef)
  EFun args body ->
    JS.EFun (map (fromMaybe "_") args) . pure . JS.SRet <$> translateExpr body
  EFunCall fun args ->
    JS.EFunCall
      <$> translateExpr fun
      <*> traverse translateExpr args
  EOpenVariant (C tag) ->
    pure . JS.EFun ["_data"] . pure $
      JS.SRet $
        JS.ERecord $
          M.fromList
            [ ("_constr", JS.ELit $ JS.LString tag)
            , ("_field", JS.EVar "_data")
            ]
  EAnnotated (Ann _ t) (EVariant (C tag))
    | tag == "True" ->
        pure $ JS.ELit (JS.LBool True)
    | tag == "False" ->
        pure $ JS.ELit (JS.LBool False)
    | TypeApp (TypeApp (TypeCon "->") _) _ <- t ->
        pure . JS.EFun ["_data"] . pure $
          JS.SRet $
            JS.ERecord $
              M.fromList
                [ ("_constr", JS.ELit $ JS.LString tag)
                , ("_field", JS.EVar "_data")
                ]
    | otherwise ->
        pure $
          JS.ERecord $
            M.fromList
              [ ("_constr", JS.ELit $ JS.LString tag)
              , ("_field", JS.ERecord mempty)
              ]
  ERecord record ->
    JS.ERecord . M.mapKeys unLabel <$> traverse translateExpr record
  ERecordAccess expr (L label) ->
    JS.ERecordAccess
      <$> translateExpr expr
      <*> pure label
  ERecordExtension record expr -> do
    expr' <- translateExpr expr
    record' <- traverse translateExpr record
    let
      fun =
        JS.EFun ["_record"] $
          concat
            [ [JS.SRecordClone "_record_copy" (JS.EVar "_record")]
            , map
                (\(L lbl, val) -> JS.SRecordAssign "_record_copy" lbl val)
                (M.toList record')
            , [JS.SRet (JS.EVar "_record_copy")]
            ]
    pure $ JS.EFunCall fun [expr']
  EIf cond trueBranch falseBranch -> do
    cond' <- translateExpr cond
    trueBranch' <- translateExpr trueBranch
    falseBranch' <- translateExpr falseBranch
    pure $
      JS.EFunCall
        ( JS.EFun
            []
            [ JS.SIf cond' [JS.SRet trueBranch']
            , JS.SIf (JS.ELit $ JS.LBool True) [JS.SRet falseBranch']
            ]
        )
        []
  ECase expr patterns -> do
    expr' <- translateExpr expr
    var <- genVar "case"
    patterns' <- translatePatterns (JS.EVar var) patterns
    pure $
      JS.EFunCall
        (JS.EFun [var] patterns')
        [expr']
  EFfi fun typ args ->
    case typ of
      Nothing ->
        JS.EFun [] . pure . JS.SRet . JS.EFunCall (JS.EVar fun) <$> traverse translateExpr args
      Just (toTypeFun -> (_, tret))
        | TypeApp (TypeCon "IO") _ <- tret ->
            JS.EFun [] . pure . JS.SRet . JS.EFunCall (JS.EVar fun) <$> traverse translateExpr args
        | otherwise ->
            JS.EFunCall (JS.EVar fun) <$> traverse translateExpr args
  EBlock block ->
    JS.EFun [] <$> translateBlock block
  ELet termdef expr -> do
    def <- translateDef termdef
    expr' <- translateExpr expr
    pure $
      flip JS.EFunCall [] $
        JS.EFun
          []
          [ JS.SDef def
          , JS.SRet expr'
          ]
  EAnnotated _ e ->
    translateExpr e
  e@EVariant {} ->
    error $
      unlines
        [ "COMPILER BUG: Unexpected naked EVariant not wrapped in EAnnotated - `" <> show e <> "`."
        , "We need type annotation in order to know if the variant should take a payload as an argument."
        ]

translatePatterns :: (Translate m) => JS.Expr -> [(Pattern, Expr Ann)] -> m JS.Block
translatePatterns outer = traverse $ \(pat, body) -> do
  result' <- translateExpr body
  PatResult conds matches <- translatePattern outer pat
  let
    (matchersV, matchersE) = unzip matches
  pure $
    JS.SIf
      (JS.EAnd conds)
      [ JS.SRet $
          JS.EFunCall
            (JS.EFun matchersV [JS.SRet result'])
            matchersE
      ]

{-

We want to translate ~pat -> result~ to something that looks like this:

if (<conditions>) {
    return (function(<matchersV>) { return result })(<matchersE>);
}

-}

data PatResult = PatResult
  { conditions :: [JS.Expr]
  , matchers :: [(Var, JS.Expr)]
  }

instance Semigroup PatResult where
  (<>) (PatResult c1 m1) (PatResult c2 m2) =
    PatResult (c1 <> c2) (m1 <> m2)

instance Monoid PatResult where
  mempty = PatResult [] []

translatePattern :: (Translate m) => JS.Expr -> Pattern -> m PatResult
translatePattern expr = \case
  PWildcard ->
    pure $
      PatResult
        { conditions = [JS.ELit $ JS.LBool True]
        , matchers = []
        }
  PVar v ->
    pure $
      PatResult
        { conditions = [JS.ELit $ JS.LBool True]
        , matchers = [(v, expr)]
        }
  PLit lit ->
    pure $
      PatResult
        { conditions = [JS.EEquals (JS.ELit $ translateLit lit) expr]
        , matchers = []
        }
  PVariant (Variant "True" Nothing) ->
    pure $
      PatResult
        { conditions = [expr]
        , matchers = []
        }
  PVariant (Variant "False" Nothing) ->
    pure $
      PatResult
        { conditions = [JS.ENot expr]
        , matchers = []
        }
  PVariant (Variant (C tag) mpat) -> do
    pat' <- traverse (translatePattern (JS.ERecordAccess expr "_field")) mpat
    pure $
      PatResult
        { conditions =
            ( JS.EEquals
                (JS.ELit $ JS.LString tag)
                (JS.ERecordAccess expr "_constr")
            )
              : maybe [] conditions pat'
        , matchers = maybe [] matchers pat'
        }
  POpenVariant (Variant (C tag) pat) -> do
    pat' <- translatePattern (JS.ERecordAccess expr "_field") pat
    pure $
      PatResult
        { conditions =
            ( JS.EEquals
                (JS.ELit $ JS.LString tag)
                (JS.ERecordAccess expr "_constr")
            )
              : conditions pat'
        , matchers = matchers pat'
        }
  PRecord (M.toList -> fields) -> do
    fmap mconcat $ for fields $ \(L field, pat) ->
      translatePattern (JS.ERecordAccess expr field) pat

translateLit :: Lit -> JS.Lit
translateLit = \case
  LInt i -> JS.LInt i
  LFloat f -> JS.LFloat f
  LString s -> JS.LString s
  LBool b -> JS.LBool b
