{-# LANGUAGE OverloadedStrings #-}

module Tests.TranslateSpec where

import Data.Map qualified as M
import Data.Text qualified as T
import Language.Giml
import Language.Giml.Compiler.Compile
import System.Process (readProcess)
import Test.Hspec

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = do
  hspec spec

spec :: Spec
spec = parallel $ do
  describe "translation" $ do
    simples
    records
    patmatch
    builtin

-- expressions --

simples :: Spec
simples = do
  describe "Simple" $ do
    it "lit int" $ do
      check
        ( boilerplate $
            ELit $
              LInt 7
        )
        "7"

    it "variant" $ do
      check
        ( boilerplate $
            EFunCall
              (typed (typeFun [tInt] (TypeApp (TypeCon "List") (TypeVar "t1"))) $ EVariant "Nil")
              [ERecord mempty]
        )
        "{ _constr: 'Nil', _field: {} }"

    it "function application" $ do
      check
        ( boilerplate $
            EFunCall
              (EFun [Just "a", Nothing] (EVar "a"))
              [ELit $ LInt 7, ELit $ LString "hello"]
        )
        "7"

records :: Spec
records = do
  describe "Records" $ do
    it "record access" $ do
      check
        ( boilerplate $
            ERecordAccess
              (ERecord $ M.fromList [("x", ELit $ LInt 9)])
              "x"
        )
        "9"

    it "record extension" $ do
      check
        ( boilerplate $
            ERecordAccess
              ( ERecordExtension
                  ( M.fromList
                      [ ("x", ELit $ LInt 7)
                      ]
                  )
                  (ERecord mempty)
              )
              "x"
        )
        "7"

    it "record extension preserves info" $ do
      check
        ( boilerplate $
            ERecordAccess
              ( ERecordExtension
                  ( M.fromList
                      [ ("x", ELit $ LInt 7)
                      ]
                  )
                  ( ERecord $
                      M.fromList
                        [ ("y", ELit $ LString "hello")
                        ]
                  )
              )
              "y"
        )
        "hello"

    it "record extension overrides fields" $ do
      check
        ( boilerplate $
            ERecordAccess
              ( ERecordExtension
                  ( M.fromList
                      [ ("x", ELit $ LInt 7)
                      ]
                  )
                  ( ERecord $
                      M.fromList
                        [ ("x", ELit $ LString "hello")
                        ]
                  )
              )
              "x"
        )
        "7"

patmatch :: Spec
patmatch = do
  describe "Pattern matching" $ do
    it "wildcard" $ do
      check
        ( boilerplate $
            ECase
              (ELit $ LInt 0)
              [ (PWildcard, ELit $ LInt 1)
              ]
        )
        "1"

    it "case int" $ do
      check
        ( boilerplate $
            ECase
              (ELit $ LInt 0)
              [ (PLit (LInt 1), ELit $ LInt 1)
              , (PLit (LInt 0), ELit $ LInt 0)
              ]
        )
        "0"

    it "case var" $ do
      check
        ( boilerplate $
            ECase
              (ELit $ LInt 17)
              [ (PLit (LInt 1), ELit $ LInt 1)
              , (PLit (LInt 0), ELit $ LInt 0)
              , (PVar "v", EVar "v")
              ]
        )
        "17"

    it "case_variant_label" $ do
      check
        ( boilerplate $
            ECase
              ( EFunCall
                  (typed (typeFun [tInt] (TypeApp (TypeCon "List") tInt)) $ EVariant "Nil")
                  [ ERecord $
                      M.fromList
                        [ ("head", ELit $ LInt 0)
                        , ("tail", ERecord mempty)
                        ]
                  ]
              )
              [ (PVariant $ Variant "Nil" (Just $ PVar "obj"), ERecordAccess (EVar "obj") "head")
              ]
        )
        "0"

    it "case variant record" $ do
      check
        ( boilerplate $
            ECase
              ( EFunCall
                  (typed (typeFun [tInt] (TypeApp (TypeCon "List") tInt)) $ EVariant "Nil")
                  [ ERecord $
                      M.fromList
                        [ ("head", ELit $ LInt 0)
                        , ("tail", ERecord mempty)
                        ]
                  ]
              )
              [
                ( PVariant $
                    Variant "Nil" $
                      Just
                        ( PRecord $
                            M.fromList
                              [ ("head", PVar "head")
                              , ("tail", PRecord mempty)
                              ]
                        )
                , EVar "head"
                )
              ]
        )
        "0"

    it "nested case" $ do
      check
        ( boilerplate $
            ECase
              (ELit $ LInt 0)
              [ (PLit (LInt 1), ELit $ LInt 1)
              ,
                ( PLit (LInt 0)
                , ECase
                    (ELit $ LInt 99)
                    [ (PLit (LInt 1), ELit $ LInt 1)
                    , (PVar "n", EVar "n")
                    ]
                )
              ]
        )
        "99"

builtin :: Spec
builtin = do
  describe "Builtins" $ do
    ints
    bools
    strings

ints :: Spec
ints = do
  describe "ints" $ do
    it "addition" $ do
      check
        ( boilerplate $
            EFunCall (EFunCall (EVar "add") [ELit $ LInt 1]) [ELit $ LInt 1]
        )
        "2"

    it "negate" $ do
      check
        ( boilerplate $
            EFunCall (EVar "negate") [ELit $ LInt 2]
        )
        "-2"

strings :: Spec
strings = do
  describe "strings" $ do
    it "concat" $ do
      check
        ( boilerplate $
            EFunCall
              ( EFunCall
                  (EVar "concat")
                  [ EFunCall (EFunCall (EVar "concat") [ELit $ LString "hello"]) [ELit $ LString " "]
                  ]
              )
              [ ELit $ LString "world"
              ]
        )
        "hello world"

bools :: Spec
bools = do
  describe "bools" $ do
    it "not" $ do
      check
        ( boilerplate $
            EFunCall (EVar "not") [typed tBool false]
        )
        "true"

    describe "and" $ do
      it "true true" $ do
        check
          ( boilerplate $
              EFunCall (EFunCall (EVar "and") [typed tBool true]) [typed tBool true]
          )
          "true"

      it "true false" $ do
        check
          ( boilerplate $
              EFunCall (EFunCall (EVar "and") [typed tBool true]) [typed tBool false]
          )
          "false"

    describe "or" $ do
      it "false false" $ do
        check
          ( boilerplate $
              EFunCall (EFunCall (EVar "or") [typed tBool false]) [typed tBool false]
          )
          "false"

      it "true false" $ do
        check
          ( boilerplate $
              EFunCall (EFunCall (EVar "or") [typed tBool true]) [typed tBool false]
          )
          "true"

--------------------------------------------

check :: File Ann -> String -> IO ()
check file expected = do
  result <- readProcess "node" [] (T.unpack $ translate' file)
  shouldBe result (expected <> "\n")

boilerplate :: Expr Ann -> File Ann
boilerplate e =
  File [] $
    pure
      [ Variable (Ann dummy tUnit) "main" (Just tUnit) $
          EBlock
            [ SExpr (Ann dummy tUnit) $ EFfi "console.log" Nothing [e]
            ]
      ]

typed :: Type -> Expr () -> Expr Ann
typed t e = EAnnotated (Ann dummy t) (fmap (const $ Ann dummy t) e)

dummy :: SourcePos
dummy = dummyAnn "test"
