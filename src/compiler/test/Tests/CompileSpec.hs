{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Tests.CompileSpec where

import Control.Monad.Identity
import Data.Text qualified as T
import Language.Giml as Giml
import Language.Giml.Compiler.Compile
import System.Process (readProcess)
import Test.Hspec
import Text.RawString.QQ

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = do
  hspec spec

spec :: Spec
spec = parallel $ do
  describe "compilation" $ do
    programs
    io
    ffi
    openVariants

programs :: Spec
programs = do
  describe "programs" $ do
    it "length" $
      check $
        Check
          { program =
              [r|
List a =
	| Nil
	| Cons { head : a, tail : List a }

length xs =
	case xs of
		| Nil -> 0
		| Cons { head : _, tail : rest } ->
			add 1 (length rest)

main = do
	ffi("console.log", length (Cons { head : 1, tail : Cons { head : 2, tail : Nil } }))
|]
          , expected = "2"
          }

    it "const" $
      check $
        Check
          { program =
              [r|
const a b = a

main = do
	ffi("console.log", const 1 2)
|]
          , expected = "1"
          }

    it "bools" $
      check $
        Check
          { program =
              [r|
hello n =
	case int_equals n 0 of
		 | True -> "Hi!"
		 | False -> "Hello!"

main = do
	ffi("console.log", hello 1)
|]
          , expected = "Hello!"
          }

    it "cube" $
      check $
        Check
          { program =
              [r|
cube n =
	let squared = mul n n
	in
		let cubed = mul n squared
		in cubed

main = do
	ffi("console.log", cube 2)
|]
          , expected = "8"
          }

    it "some" $
      check $
        Check
          { program =
              [r|
apply f x = f x

getSome x =
	case x of
		| Some y -> y

main = ffi("console.log", getSome (apply Some 1))
|]
          , expected = "1"
          }

    it "currying" $
      check $
        Check
          { program =
              [r|
increment = add 1

apply f x = f x

id x = x
const x y = x

main = do
	ffi("console.log", apply (add 1) (increment 1))
	ffi("console.log", (const 1) 1)
|]
          , expected = "3\n1"
          }

    it "let map" $
      check $
        Check
          { program =
              [r|
main =
	let
		map f xs =
			case xs of
				| Nil -> Nil
				| Cons { head : x, tail : rest } ->
					Cons { head : f x, tail : map f rest }

	in
		ffi( "console.log", head (map (add 1) list) )

List a =
	| Nil
	| Cons { head : a, tail : List a }

list =
	Cons { head : 1, tail : Cons { head : 2, tail : Nil } }

head list =
	case list of
		| Cons { head : head } ->
			head
|]
          , expected = "2"
          }

    it "foldable" $
      check $
        Check
          { program =
              [r|
List a =
	 | Nil
	 | Cons { head : a, tail : List a }

Tree a =
	 | Empty
	 | Leaf a
	 | Node { lhs : Tree a, value : a, rhs : Tree a }

lists =
	{ foldr : \f zero list ->
		case list of
			| Nil -> zero
			| Cons l ->
				f l.head (lists.foldr f zero l.tail)
	}

trees =
	{ foldr : \f zero tree ->
		case tree of
			| Empty -> zero
			| Leaf x -> f x zero
			| Node { lhs : lhs, value : v, rhs : rhs } ->
				trees.foldr f (f v (trees.foldr f zero rhs)) lhs
	}

foldMap monoid class f =
	class.foldr (\x y -> monoid.append (f x) y) monoid.empty

addition =
	{ empty : 0
	, append : add
	}

multipication =
	{ empty : 1
	, append : mul
	}

product class = foldMap multipication class id
sum class = foldMap addition class id

mylist = Cons
	{ head : 1
	, tail : Cons { head : 2, tail : Cons { head : 5, tail : Nil } }
	}

mytree = Node
	{ lhs : Node { lhs : Leaf 1, value : 2, rhs : Leaf 3 }
	, value : 4
	, rhs : Leaf 5
	}

id x = x

main = do
	ffi("console.log", sum lists mylist)
	ffi("console.log", product lists mylist)
	ffi("console.log", sum trees mytree)
	ffi("console.log", product trees mytree)
|]
          , expected = "8\n10\n15\n120"
          }

    it "out of order" $
      check $
        Check
          { program =
              [r|
x = add y 1
y = 1
main = ffi("console.log", x)
|]
          , expected = "2"
          }

io :: Spec
io = do
  describe "IO" $ do
    it "pure" $
      check $
        Check
          { program =
              [r|
x = do
	y <- pure 1
	pure y

main = do
	r <- x
	ffi("console.log", add 1 r)
|]
          , expected = "2"
          }
    ioRefs

ioRefs :: Spec
ioRefs = do
  describe "IO" $ do
    it "newIORef" $
      check $
        Check
          { program =
              [r|
mkZero = newIORef 0

main = do
	ref0 <- mkZero
	ref1 <- mkZero
	modifyIORef ref1 (add 1)
	zero <- readIORef ref0
	one <- readIORef ref1
	ffi("console.log" : Int -> IO {}, zero)
	ffi("console.log" : Int -> IO {}, one)
|]
          , expected = "0\n1"
          }

    it "writeIORef" $
      check $
        Check
          { program =
              [r|
main = do
	ref <- newIORef 0
	zero <- readIORef ref
	writeIORef ref 1
	one <- readIORef ref
	ffi("console.log" : Int -> IO {}, zero)
	ffi("console.log" : Int -> IO {}, one)
|]
          , expected = "0\n1"
          }

ffi :: Spec
ffi = do
  describe "FFI" $ do
    it "parseInt" $
      check $
        Check
          { program =
              [r|
main = ffi("console.log" : Int -> IO {}, ffi("parseInt" : String -> Int, "17"))
|]
          , expected = "17"
          }

openVariants :: Spec
openVariants = do
  describe "open variants" $ do
    it "poly -> concrete with pattern" $
      check $
        Check
          { program =
              [r|
withDefault x =
	case x of
		| #Some 1 -> 1
		| #Some y -> y
		| #Nil {} -> 0

main = do
	ffi("console.log" : Int -> IO {}, withDefault (#Some 2))
	ffi("console.log" : Int -> IO {}, withDefault (#Some 1))
	ffi("console.log" : Int -> IO {}, withDefault (#Nil {}))
|]
          , expected = "2\n1\n0"
          }

    it "poly -> generic concrete" $
      check $
        Check
          { program =
              [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

main = do
	ffi("console.log" : Int -> IO {}, withDefault 0 (#Some 1))
	ffi("console.log" : Int -> IO {}, withDefault 0 (#Nil {}))
|]
          , expected = "1\n0"
          }

    it "poly -> poly" $
      check $
        Check
          { program =
              [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

incrementMaybe x =
	case x of
		| #Some n -> #Some (add n 1)
		| #Nil {} -> #Nil {}

main = do
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Some 1)))
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Nil {})))
|]
          , expected = "2\n0"
          }

    it "poly -> ub poly" $
      check $
        Check
          { program =
              [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

incrementMaybe x =
	case x of
		| #Some n -> #Some (add n 1)
		| a -> a

main = do
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Some 1)))
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Nil {})))
|]
          , expected = "2\n0"
          }
    it "poly (with wildcard) -> ub poly" $
      check $
        Check
          { program =
              [r|
withDefault default x =
	case x of
		| #Some y -> y
		| #Nil {} -> default

incrementMaybe x =
	case x of
		| #Some n -> #Some (add n 1)
		| _ -> x

main = do
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Some 1)))
	ffi("console.log" : Int -> IO {}, withDefault 0 (incrementMaybe (#Nil {})))
|]
          , expected = "2\n0"
          }

data Check = Check
  { program :: T.Text
  , expected :: String
  }

check :: Check -> IO ()
check (Check prog expect) = do
  prog' <- either (error . T.unpack) pure $ runIdentity $ compile Giml.noLogging "test" prog
  result <- readProcess "node" [] (T.unpack prog')
  shouldBe result (expect <> "\n")
